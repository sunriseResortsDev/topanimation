<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
   
    public function __construct(){
	    parent::__construct();
	    $this->load->model('clients/Home_model');
	    $this->load->model('admin/General_model');
	    $this->load->helper('mailer_helper');
	    $this->data['language'] = 'english';
	    if (isset($_COOKIE['language'])) {
		    $this->data['language']      = $_COOKIE['language'];
		}
	    $this->data['activities']    = $this->General_model->get_activities();
	  }
		 
	public function index(){
		$data['sliders']       = $this->Home_model->get_sliders();
		$data['navbar']        = 'clients/includes/navbar';
		$data['view']          = 'clients/home/index.php';
		$this->load->view('clients/includes/layout',$data);
	}

	public function index_about(){
		$data['about']         = $this->Home_model->get_sit_metaData('about_us','single');
	    $data['who_we']        = $this->Home_model->get_sit_metaData('who_we_are','single');
	    $data['what_do']       = $this->Home_model->get_sit_metaData('what_we_do','single');
	    $data['achievements']  = $this->Home_model->get_sit_metaData('achievements','single');
		$this->load->view('clients/home/index_about',$data);
	}	
    
	public function index_gallery(){
		$data['gallerys']       = $this->Home_model->get_sit_metaData('gallery','multi');
		$data['points']         = $this->Home_model->get_gallery_unique();
		$this->load->view('clients/home/index_gallery',$data);
	}	

	public function index_services(){
		$data['services']       = $this->Home_model->get_sit_metaData('services','multi');
		$this->load->view('clients/home/index_services',$data);
	}	

	public function index_team(){
		$data['teams']           = $this->Home_model->get_sit_metaData('team','multi');
		$this->load->view('clients/home/index_team',$data);
	}	

    public function index_offers(){
		$data['allOffers']      = get_special();
		$this->load->view('clients/home/index_offers',$data);
	}	





	public function gallery(){
		$data['gallerys']       = $this->Home_model->get_sit_metaData('gallery','multi');
		$data['points']         = $this->Home_model->get_gallery_unique();
		$data['navbar']         = 'clients/includes/navbar';
		$data['view']           = 'clients/home/gallery.php';
		$this->load->view('clients/includes/layout',$data);
	}

	public function portfolio(){
		$data['portfolio']       = $this->Home_model->get_sit_metaData('portfolio','multi');
		$data['navbar']         = 'clients/includes/navbar';
		$data['view']           = 'clients/home/portfolio.php';
		$this->load->view('clients/includes/layout',$data);
	}

	public function team(){
		$data['teams']           = $this->Home_model->get_sit_metaData('team','multi');
		$data['navbar']         = 'clients/includes/navbar';
		$data['view']           = 'clients/home/team.php';
		$this->load->view('clients/includes/layout',$data);
	}

	public function contacts(){
		$data['contacts']            = $this->Home_model->get_sit_metaData('contact_us','multi');
		$data['contact_about']       = $this->Home_model->get_sit_metaData('contact_us','multi');
		$data['navbar']         = 'clients/includes/navbar';
		$data['view']           = 'clients/home/contact_us.php';
		$this->load->view('clients/includes/layout',$data);
	}

	public function contact_us(){
	    $formdata=[];
        parse_str($this->input->post('formdata'),$formdata);     
        $formdata['status'] ='24';
        unset($formdata['s']);
        unset($formdata['subscriber_email']);
	    $returned_id = $this->Home_model->add_client_mail($formdata);
	    $result = mail_management($formdata);
	    echo json_encode(true);
	    exit();  
	            
	}

	public function about(){
		$data['about']       = $this->Home_model->get_sit_metaData('about','single');
		$arrayFilter             = json_decode($data['about']['points'],true);
		for ($i = 1; $i < sizeof($arrayFilter); $i++) {
		    for ($j=$i+1; $j < sizeof($arrayFilter); $j++) {
		        if ($arrayFilter[$i]['rank'] > $arrayFilter[$j]['rank']) {
		            $c = $arrayFilter[$i];
		            $arrayFilter[$i] = $arrayFilter[$j];
		            $arrayFilter[$j] = $c;
		        }
		    }
		}
		$data['points']         = $arrayFilter;
		$data['navbar']         = 'clients/includes/navbar';
		$data['view']           = 'clients/home/about.php';
		$this->load->view('clients/includes/layout',$data);
	}
    

    public function services(){
		$data['services']       = $this->Home_model->get_sit_metaData('services','multi');
		$this->load->view('clients/home/services',$data);
	}

	public function products(){
		$data['categories']       = $this->Home_model->get_categories();
		$this->load->view('clients/home/products',$data);
	}

	public function product($serial){
		$data['serial']           = $serial;
		$data['categories']       = $this->Home_model->get_categories();
		$data['category']         = $this->Home_model->get_category($serial);
		$data['cat_items']        = $this->Home_model->get_category_items($data['category']['id']);
		$data['navbar']           = 'clients/includes/outer_navbar.php';
		$data['view']             = 'clients/home/product.php';
		$this->load->view('clients/includes/layout',$data);
	}

	public function blog(){
		$data['blog']       = $this->Home_model->get_sit_metaData('blog','multi');
		$this->load->view('clients/home/blog',$data);
	}

	public function language_converter($lang){
        setcookie('language', $lang, 2592000000, "/"); 
        redirect($this->agent->referrer());
	}


	

}
?>

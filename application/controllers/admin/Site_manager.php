<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Site_manager extends MY_Controller {
   
   		public function __construct(){
			parent::__construct();
        $this->load->model('admin/Modules_model');
	      $this->load->model('admin/General_model');
	      $this->load->model('admin/Site_manager_model');
	      $this->load->helper('mailer_helper');
  		  $this->data['user_id']                 = $this->global_data['sessioned_user']['id'];
  		  $this->data['username']                = $this->global_data['sessioned_user']['username'];
	      $this->data['ubranches']               = get_ubranches($this->data['user_id']);
	      $this->data['dep_id']                  = $this->global_data['sessioned_user']['department'];
  			$this->data['module']                  = $this->Modules_model->get_module_by(12,'');
  			$this->data['slider_module']           = $this->Modules_model->get_module_by(13,'');
  			$this->data['about_module']            = $this->Modules_model->get_module_by(14,'');
  			$this->data['services_module']         = $this->Modules_model->get_module_by(15,'');
  			$this->data['products_module']         = $this->Modules_model->get_module_by(16,'');
  			$this->data['portfolio_module']        = $this->Modules_model->get_module_by(17,'');
  			$this->data['blog_module']             = $this->Modules_model->get_module_by(18,'');
  			$this->data['contact_module']          = $this->Modules_model->get_module_by(19,'');
  			$this->data['footer_module']           = $this->Modules_model->get_module_by(20,'');
	      $this->data['clients_req_module']      = $this->Modules_model->get_module_by(21,'');
	      $this->data['gallery_module']          = $this->Modules_model->get_module_by(22,'');
	      $this->data['team_module']             = $this->Modules_model->get_module_by(23,'');
	      $this->data['offers_module']           = $this->Modules_model->get_module_by(24,'');
  			$this->data['language_module']         = $this->Modules_model->get_module_by(25,'');
        $this->data['bookings_module']          = $this->Modules_model->get_module_by(26,'');
	      $this->data['permission']              = user_access($this->data['module']['id']);
	      $this->data['slider_permission']       = user_access(13);
	      $this->data['about_permission']        = user_access(14);
	      $this->data['services_permission']     = user_access(15);
	      $this->data['products_permission']     = user_access(16);
	      $this->data['portfolio_permission']    = user_access(17);
	      $this->data['blog_permission']         = user_access(18);
	      $this->data['contact_permission']      = user_access(19);
	      $this->data['footer_permission']       = user_access(20);
	      $this->data['clients_req_permission']  = user_access(21);
	      $this->data['gallery_permission']      = user_access(22);
	      $this->data['team_permission']         = user_access(23);
	      $this->data['offer_permission']        = user_access(24);
	      $this->data['language_permission']     = user_access(25);
        $this->data['bookings_permission']     = user_access(26);
	      $this->data['ubranches']               = get_ubranches($this->data['user_id']);
		}
		 
		public function index(){
			$data['view'] = 'admin/dashboard_index';
			$this->load->view('admin/includes/layout',$data);
		}

// ------------------------------ slider manager ---------------------------------
    	public function slider_manager(){
      		access_checker(0,0,$this->data['slider_permission']['edit'],0,0,'admin/site_manager');
      		$data['sliders']   = $this->Site_manager_model->get_all_sliders();
        	if($this->input->post('items')){
          		foreach ($this->input->post('items') as $key => $item) {
            		if (isset($item['id'])) {
		                $item_data   = $this->Site_manager_model->get_slider_item($item['id']);
		                $file_name   = do_upload("items-".$key."-img",'slider','site_assets/images');
		                $item['img'] = $file_name; 
                		if ($item['img'] !=null) {
                			//die(print_r('exist'));
	                		if (($item['old_img'] != 0 )&&($item['img'] != $item['old_img'])) {
	                			if(file_exists("site_assets/images/slider/".$item['old_img'])){
	                	    		unlink("site_assets/images/slider/".$item['old_img']);
	                     		}
	                		}
                		}else{
                 			unset($item['img']);	
                		} 
                		unset($item['old_img']);
                		$updated     = $this->Site_manager_model->update_slider($item,$item['id']);
                		if ($updated > 0) {
                    		loger('update',$this->data['slider_module']['id'],$this->data['slider_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated slider: '.$item_data['id']);
                		}
              		}else{
		              	$file_name   = do_upload("items-".$key."-img",'slider','site_assets/images');
		                $item['img'] = $file_name;  
		                $item_id = $this->Site_manager_model->add_slider($item);
	               		if ($item_id) {
	                		loger('Create', $this->data['slider_module']['id'], $this->data['slider_module']['name'], $item_id, 0, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added slider:'.$item['id'].'');
	                 	}
	              	}
	            }
		    	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
		    	redirect('admin/site_manager/slider_manager');
        	}
      		$data['view'] = 'admin/site_manager/slider_manager';
      		$this->load->view('admin/includes/layout',$data);       
    	}

		public function delete_slider(){
	  		access_checker(0,0,0,$this->data['slider_permission']['remove'],0,'');
      		if ($this->input->post('id')) {
	          	$id = $this->input->post('id');
	          	$item = $this->Site_manager_model->get_slider_item($id);
	          	$this->db->delete('slider',array('id' => $item['id']));	
	          	loger('Delete', $this->data['slider_module']['id'], $this->data['slider_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted slider:'.$item['id'].'');
          		echo json_encode(true);
          		exit();
        	}  
      	}   

// ------------------------------ About manager ---------------------------------
    	public function about_manager($meta_key=''){
      		access_checker(0,0,$this->data['about_permission']['edit'],0,0,'admin/site_manager');
      		if(!$this->input->post('submit')){
	        	$data['about']         = $this->Site_manager_model->get_site_metaData('about_us','single');
            $data['who_we']        = $this->Site_manager_model->get_site_metaData('who_we_are','single');
            $data['what_do']       = $this->Site_manager_model->get_site_metaData('what_we_do','single');
            $data['achievements']  = $this->Site_manager_model->get_site_metaData('achievements','single');
	    	  }
        	if($this->input->post('submit')){
	        	$file_name   = do_upload("img",'about','site_assets/images');
            learn_translate($this->input->post('header'));
            learn_translate($this->input->post('title'));
            learn_translate($this->input->post('paragraph'));
	        	$fdata       = array(
                        					'header'    => $this->input->post('header'),
                        					'title'     => $this->input->post('title'),
                        					'paragraph' => $this->input->post('paragraph'),
                        				);
            $item_data   = $this->Site_manager_model->get_site_metaData('about','single');
            $updated     = $this->Site_manager_model->update_about($fdata,$meta_key);
              if ($updated > 0) {
                  loger('update',$this->data['about_module']['id'],$this->data['about_module']['name'],0,0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'updated about data');
              }
			    $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
			    redirect('admin/site_manager/about_manager');
        	}
	      	$data['view'] = 'admin/site_manager/about_manager';
	      	$this->load->view('admin/includes/layout',$data);       
    	}   

// ------------------------------ Services manager ---------------------------------
    	public function service_manager(){
       		access_checker(0,0,$this->data['services_permission']['edit'],0,0,'admin/site_manager');
       		$data['services']         = $this->Site_manager_model->get_site_metaData('services','multi');
       		$data['font_icons']       = $this->General_model->get_font_icons();
        	if($this->input->post('items')){
          		foreach ($this->input->post('items') as $key => $item) {
            		if (isset($item['id'])) {
                		$item_data     = $this->Site_manager_model->get_site_metaData_item($item['id'],'services');
                		$updated     = $this->Site_manager_model->update_site_metaData_item($item,$item['id']);
                		if ($updated > 0) {
                    		loger('update',$this->data['services_module']['id'],$this->data['services_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated service: '.$item_data['title']);
                		}
              		}else{
                		$item_id = $this->Site_manager_model->add_site_metaData_item($item);
               			if ($item_id) {
                			loger('Create', $this->data['services_module']['id'], $this->data['services_module']['name'], $item_id, 0, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added service:'.$item['title'].'');
                 		}
              		}
            	}
	    		$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
	    		redirect('admin/site_manager/service_manager');
        	}
      		$data['view'] = 'admin/site_manager/services_manager';
      		$this->load->view('admin/includes/layout',$data);       
    	}

		public function delete_service(){
	  		access_checker(0,0,0,$this->data['services_permission']['remove'],0,'');
      		if ($this->input->post('id')) {
          		$id = $this->input->post('id');
          		$item = $this->Site_manager_model->get_site_metaData_item($id,'services');
          		$this->db->delete('site_meta_data',array('id' => $item['id'],'meta'=>'services'));	
          		loger('Delete', $this->data['services_module']['id'], $this->data['services_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted slider:'.$item['title'].'');
          		echo json_encode(true);
          		exit();
        	}  
      	}   
// ------------------------------ Offers manager ---------------------------------
    	public function offer_manager(){
       		access_checker(0,0,$this->data['offer_permission']['edit'],0,0,'admin/site_manager');
       		$data['offers']         = $this->Site_manager_model->get_offers();
       		$data['activity']       = $this->Site_manager_model->get_activities();
       		$data['hotels']         = $this->Site_manager_model->get_hotels();
       		$data['sub_activity']   = $this->Site_manager_model->get_sub_activities();
        	if($this->input->post('items')){
          		foreach ($this->input->post('items') as $key => $item) {
                learn_translate($item['name']);
                learn_translate($item['data']);
            		if (isset($item['id'])) {
                		$item_data     = $this->Site_manager_model->get_offer($item['id']);
                		$file_name            = do_upload('items-'.$key.'-image', 'offer','site_assets/images');
                		if ($file_name) {
	            			$item['image']       = $file_name;
                		}
                		$updated       = $this->Site_manager_model->update_offer($item,$item['id']);
                		if ($updated > 0) {
                    		loger('update',$this->data['offers_module']['id'],$this->data['offers_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated offer: '.$item_data['name']);
                		}
              		}else{
              			$file_name            = do_upload('items-'.$key.'-image', 'offer','site_assets/images');
            			$item['image']       = $file_name;
                		$item_id = $this->Site_manager_model->add_offer($item);
               			if ($item_id) {
                			loger('Create', $this->data['offers_module']['id'], $this->data['offers_module']['name'], $item_id, 0, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added offer:'.$item['name'].'');
                 		}
              		}
            	}
	    		$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
	    		redirect('admin/site_manager/offer_manager');
        	}
      		$data['view'] = 'admin/site_manager/offers_manager';
      		$this->load->view('admin/includes/layout',$data);       
    	}

		public function delete_offer(){
	  		access_checker(0,0,0,$this->data['offer_permission']['remove'],0,'');
      		if ($this->input->post('id')) {
          		$id = $this->input->post('id');
          		$item = $this->Site_manager_model->get_offer($id);
          		$this->db->delete('offers',array('id' => $item['id']));	
          		loger('Delete', $this->data['offers_module']['id'], $this->data['offers_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted offer:'.$item['name'].'');
          		echo json_encode(true);
          		exit();
        	}  
      	}   

// ------------------------------ portfolio manager ---------------------------------
    	public function portfolio_manager(){
      		access_checker(0,0,$this->data['portfolio_permission']['edit'],0,0,'admin/site_manager');
      		$data['portfolios']   = $this->Site_manager_model->get_site_metaData('portfolio','multi');
        	if($this->input->post('items')){
          		foreach ($this->input->post('items') as $key => $item) {
            		if (isset($item['id'])) {
		                $item_data   = $this->Site_manager_model->get_site_metaData_item($item['id'],'portfolio');
		                $file_name   = do_upload("items-".$key."-img",'portfolio','site_assets/images');
		                $item['img'] = $file_name; 
                		if ($item['img'] !=null) {
	                		if (($item['old_img'] != 0 )&&($item['img'] != $item['old_img'])) {
	                			if(file_exists("site_assets/images/portfolio/".$item['old_img'])){
	                	    		unlink("site_assets/images/portfolio/".$item['old_img']);
	                     		}
	                		}
                		}else{
                 			unset($item['img']);	
                		} 
		                unset($item['old_img']);
		                $updated     = $this->Site_manager_model->update_site_metaData_item($item,$item['id']);
		                if ($updated > 0) {
                    		loger('update',$this->data['portfolio_module']['id'],$this->data['portfolio_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated portfolio: '.$item_data['id']);
                		}
              		}else{
		              	$file_name   = do_upload("items-".$key."-img",'portfolio','site_assets/images');
		                $item['img'] = $file_name;  
		                $item_id = $this->Site_manager_model->add_site_metaData_item($item);
               			if ($item_id) {
                			loger('Create', $this->data['portfolio_module']['id'], $this->data['portfolio_module']['name'], $item_id, 0, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added portfolio:'.$item['id'].'');
                 		}
              		}
            	}
	    		$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
	    		redirect('admin/site_manager/portfolio_manager');
        	}
      		$data['view'] = 'admin/site_manager/portfolio_manager';
      		$this->load->view('admin/includes/layout',$data);       
    	}

		public function delete_portfolio(){
	  		access_checker(0,0,0,$this->data['portfolio_permission']['remove'],0,'');
      		if ($this->input->post('id')) {
          		$id = $this->input->post('id');
          		$item = $this->Site_manager_model->get_site_metaData_item($id,'portfolio');
         		$this->db->delete('site_meta_data',array('id' => $item['id'],'meta'=>'portfolio'));	
          		loger('Delete', $this->data['portfolio_module']['id'], $this->data['portfolio_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted portfolio:'.$item['id'].'');
          		echo json_encode(true);
          		exit();
        	}  
      	}   

// ------------------------------ gallery manager ---------------------------------
    public function gallery_manager(){
      access_checker(0,0,$this->data['gallery_permission']['edit'],0,0,'admin/site_manager');
      $data['gallerys']   = $this->Site_manager_model->get_site_metaData('gallery','multi');
        if($this->input->post('items')){
          foreach ($this->input->post('items') as $key => $item) {
            learn_translate($item['header']);
            learn_translate($item['title']);
            learn_translate($item['points']);
            if (isset($item['id'])) {
                $item_data   = $this->Site_manager_model->get_site_metaData_item($item['id'],'gallery');
                $file_name   = do_upload("items-".$key."-img",'gallery','site_assets/images');
                $item['img'] = $file_name; 
                if ($item['img'] !=null) {
                  //die(print_r('exist'));
                  if (($item['old_img'] != 0 )&&($item['img'] != $item['old_img'])) {
                    if(file_exists("site_assets/images/gallery/".$item['old_img'])){
                        unlink("site_assets/images/gallery/".$item['old_img']);
                       }
                  }
                }else{
                 unset($item['img']); 
                } 
                unset($item['old_img']);
                $item['points'] = str_replace(' ', '_',$item['points']);
                $updated     = $this->Site_manager_model->update_site_metaData_item($item,$item['id']);
                if ($updated > 0) {
                    loger('update',$this->data['gallery_module']['id'],$this->data['gallery_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated gallery: '.$item_data['id']);
                }
              }else{
                $file_name   = do_upload("items-".$key."-img",'gallery','site_assets/images');
                $item['img'] = $file_name;  
                $item_id = $this->Site_manager_model->add_site_metaData_item($item);
               if ($item_id) {
                loger('Create', $this->data['gallery_module']['id'], $this->data['gallery_module']['name'], $item_id, 0, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added gallery:'.$item['id'].'');
                 }
              }
            }
      $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
      redirect('admin/site_manager/gallery_manager');
        }
      $data['view'] = 'admin/site_manager/gallery_manager';
      $this->load->view('admin/includes/layout',$data);       
    }

    // public function add_gallery($id, $type, $key){
    //   access_checker(0,0,0,$this->data['gallery_permission']['edit'],0,'');
    //   if($type == 'new'){
    //     $file_name   = do_upload("items-".$key."-img",'gallery','site_assets/images');
    //     $fdata       = array(
    //       'header'    => $this->input->post('header'),
    //       'title'     => $this->input->post('title'),
    //       'points'    => $this->input->post('points'),
    //       'img'       => $file_name,
    //     );
    //     $item_id = $this->Site_manager_model->add_site_metaData_item($fdata);
    //     if ($item_id) {
    //       loger('Create', $this->data['gallery_module']['id'], $this->data['gallery_module']['name'], $item_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0,'added gallery:'.$item_id.'');
    //     }
    //   }else{
    //     $item_data   = $this->Site_manager_model->get_site_metaData_item($id,'gallery');
    //     $file_name   = do_upload("items-".$key."-img",'gallery','site_assets/images');
    //     $fdata       = array(
    //       'header'    => $this->input->post('header'),
    //       'title'     => $this->input->post('title'),
    //       'points'    => $this->input->post('points'),
    //       'img'       => $file_name,
    //     );
    //     if (!$fdata['img']) {
    //       unset($fdata['img']); 
    //     }
    //     $updated     = $this->Site_manager_model->update_site_metaData_item($fdata,$id);
    //     if ($updated > 0) {
    //       loger('update',$this->data['gallery_module']['id'],$this->data['gallery_module']['name'],$id,0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'updated gallery: '.$id);
    //     }
    //   }
    //   redirect('admin/site_manager/gallery_manager');
    // } 

  public function delete_gallery(){
    access_checker(0,0,0,$this->data['gallery_permission']['remove'],0,'');
      if ($this->input->post('id')) {
          $id = $this->input->post('id');
          $item = $this->Site_manager_model->get_site_metaData_item($id,'gallery');
         $this->db->delete('site_meta_data',array('id' => $item['id'],'meta'=>'gallery'));  
          loger('Delete', $this->data['gallery_module']['id'], $this->data['gallery_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted gallery:'.$item['id'].'');
          echo json_encode(true);
          exit();
        }  
      } 

// ------------------------------ team manager ---------------------------------
    public function team_manager(){
      access_checker(0,0,$this->data['team_permission']['edit'],0,0,'admin/site_manager');
      $data['teams']   = $this->Site_manager_model->get_site_metaData('team','multi');
        if($this->input->post('items')){
          foreach ($this->input->post('items') as $key => $item) {
            if (isset($item['id'])) {
                $item_data   = $this->Site_manager_model->get_site_metaData_item($item['id'],'team');
                $file_name   = do_upload("items-".$key."-img",'team','site_assets/images');
                $item['img'] = $file_name; 
                if ($item['img'] !=null) {
                  if (($item['old_img'] != 0 )&&($item['img'] != $item['old_img'])) {
                    if(file_exists("site_assets/images/team/".$item['old_img'])){
                        unlink("site_assets/images/team/".$item['old_img']);
                       }
                  }
                }else{
                 unset($item['img']); 
                } 
                unset($item['old_img']);
                $updated     = $this->Site_manager_model->update_site_metaData_item($item,$item['id']);
                if ($updated > 0) {
                    loger('update',$this->data['team_module']['id'],$this->data['team_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated team: '.$item_data['id']);
                }
              }else{
                $file_name   = do_upload("items-".$key."-img",'team','site_assets/images');
                $item['img'] = $file_name;  
                $item_id = $this->Site_manager_model->add_site_metaData_item($item);
               if ($item_id) {
                loger('Create', $this->data['team_module']['id'], $this->data['team_module']['name'], $item_id, 0, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added team:'.$item['id'].'');
                 }
              }
            }
      $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
      redirect('admin/site_manager/team_manager');
        }
      $data['view'] = 'admin/site_manager/team_manager';
      $this->load->view('admin/includes/layout',$data);       
    }

  public function delete_team(){
    access_checker(0,0,0,$this->data['team_permission']['remove'],0,'');
      if ($this->input->post('id')) {
          $id = $this->input->post('id');
          $item = $this->Site_manager_model->get_site_metaData_item($id,'team');
         $this->db->delete('site_meta_data',array('id' => $item['id'],'meta'=>'team'));  
          loger('Delete', $this->data['team_module']['id'], $this->data['team_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted team:'.$item['id'].'');
          echo json_encode(true);
          exit();
        }  
      }   

// ------------------------------ contact manager ---------------------------------
    public function contact_manager(){
       access_checker(0,0,$this->data['contact_permission']['edit'],0,0,'admin/site_manager');
       $data['contacts']         = $this->Site_manager_model->get_site_metaData('contact_us','multi');
        if($this->input->post('items')){
          foreach ($this->input->post('items') as $key => $item) {
            if (isset($item['id'])) {
                $item_data     = $this->Site_manager_model->get_site_metaData_item($item['id'],'contact_us');
                $updated     = $this->Site_manager_model->update_site_metaData_item($item,$item['id']);
                if ($updated > 0) {
                    loger('update',$this->data['contact_module']['id'],$this->data['contact_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated contact: '.$item_data['header']);
                }
              }
            }
	    $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
	    redirect('admin/site_manager/contact_manager');
        }
      $data['view'] = 'admin/site_manager/contact_manager';
      $this->load->view('admin/includes/layout',$data);       
    }  

    public function blank_translate(){
      learn_translate(' ');
      redirect('admin/site_manager/lang_manager');
    }

// ------------------------------ footer manager ---------------------------------
    public function footer_manager(){
      access_checker(0,0,$this->data['footer_permission']['edit'],0,0,'admin/site_manager');
      if(!$this->input->post('submit')){
	      $data['footer_about']          = $this->Site_manager_model->get_site_metaData('footer_about','single');
          $data['footer_info']           = $this->Site_manager_model->get_site_metaData('footer_info','single');
          $data['footer_tel']            = $this->Site_manager_model->get_site_metaData('footer_tel','single');
          $data['footer_email']          = $this->Site_manager_model->get_site_metaData('footer_email','single');
          $data['footer_working']        = $this->Site_manager_model->get_site_metaData('footer_working','single');
	    }
        if($this->input->post('items')){
          foreach ($this->input->post('items') as $key => $item) {
            if (isset($item['header'])) {
              learn_translate($item['header']);
            }
            if (isset($item['title'])) {
              learn_translate($item['title']);
            }
            if ($item['paragraph']) {
              learn_translate($item['paragraph']);
            }
            if (isset($item['id'])) {
                $item_data   = $this->Site_manager_model->get_site_metaData_item($item['id'],$item['meta']);
                $updated     = $this->Site_manager_model->update_site_metaData_item($item,$item['id']);
                if ($updated > 0) {
                    loger('update',$this->data['footer_module']['id'],$this->data['footer_module']['name'],$item['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($item, JSON_UNESCAPED_UNICODE),0,0,'updated footer data: '.$item_data['id']);
                }
              }
            }
	    $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
	    redirect('admin/site_manager/footer_manager');
        }
      $data['view'] = 'admin/site_manager/footer_manager';
      $this->load->view('admin/includes/layout',$data);       
    }  

//--------------------------------------- Categories and products -----------------------------------------

    public function category_manager(){
      access_checker(0,0,$this->data['products_permission']['edit'],0,0,'admin/site_manager');
      if(!$this->input->post('submit')){
	      $data['categories']          = $this->Site_manager_model->get_categories();
	    }
        if($this->input->post()){
        	$file_name   = do_upload("img",'products','site_assets/images');
        	$serial     = strtoupper(str_pad(dechex( mt_rand( 2, 1648575 ) ), 6, '0', STR_PAD_LEFT)); 
        	$fdata      = array(
								  'group_name'    => $this->input->post('group_name'),
								  'serial'        => $serial,
								  'img'           => $file_name,
								  'rank'          => $this->input->post('rank'),
								  );
        	if ($fdata['img'] !=null) {
                if (($this->input->post('old_img') != 0 )&&($fdata['img'] != $this->input->post('old_img'))) {
                	if(file_exists("site_assets/images/products/".$this->input->post('old_img'))){
                	    unlink("site_assets/images/products/".$this->input->post('old_img'));
                     }
                }
             }else{
                unset($fdata['img']);	
            } 
        	if ($this->input->post('id')) {
        		    unset($item['serial']);
	                $item_data   = $this->Site_manager_model->get_category($this->input->post('id'));
	                $updated     = $this->Site_manager_model->update_category($fdata,$this->input->post('id'));
	                if ($updated > 0) {
	                    loger('update',$this->data['products_module']['id'],$this->data['products_module']['name'],$item_data['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'updated category: '.$item_data['id']);
	                }
              }else{
              	$file_name   = do_upload("items-".$key."-img",'products','site_assets/images');
                $item['img'] = $file_name;  
                $item_id = $this->Site_manager_model->add_category($fdata);
               if ($item_id) {
                loger('Create', $this->data['products_module']['id'], $this->data['products_module']['name'], $item_id, 0, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added category:'.$item_id.'');
                 }
              }
		    $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
		    redirect('admin/site_manager/category_manager');
	     }
      $data['view'] = 'admin/site_manager/category_manager';
      $this->load->view('admin/includes/layout',$data);       
    }  

    public function delete_category(){
	  access_checker(0,0,0,$this->data['products_permission']['remove'],0,'');
      if ($this->input->post('id')) {
          $id = $this->input->post('id');
          $item = $this->Site_manager_model->get_category($id);
          $this->db->update('site_groups', array('deleted' => 1), "id = ".$item['id']);	
          loger('Delete', $this->data['products_module']['id'], $this->data['products_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted category:'.$item['id'].'');
          echo json_encode(true);
          exit();
        }  
      } 

//-----------------------------------products manager

    public function products_index($gid){
      access_checker($this->data['products_permission']['view'],0,0,0,0,'admin/dashboard');	
      $data['category']         = $this->Site_manager_model->get_category($gid);
  		$data['view']          = 'admin/site_manager/products_index';
  		$this->load->view('admin/includes/layout',$data);

	  }  


	 public function products_ajax($gid){
	      $dt_att  = $this->datatables_att();
	      $items   = $this->Site_manager_model->get_products($gid,$dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');

	      $data = array();
	      $i    =  1;
	       foreach($items as $item) {
	           $arr = array(); 
	           $tools=array();
	            if ($this->data['permission']['remove'] == 1) {
	            	if ($item['deleted'] ==0) {
	                     $tools[] = '<div class="info"><a href="javascript: void(0);" onclick="del('.$item['id'].',\''.$item['item_name'].'\',\'items\',\'admin/site_manager\',\'delete_product\')" titel="Delete item">
			                          <span style="color:red;">Delete</span>
			                     </a> </div> ';
			           }else{
	                 	 $tools[] = '<a href="javascript: void(0);" onclick="reOpen('.$item['id'].',\''.$item['item_name'].'\',\'items\',\'admin/site_manager\',\'reopen_product\')" titel="Delete item">
	                          <span style="color:blue;">Reopen</span>
	                     </a> </div> ';
			                 }
	                  }

	                $arr[] = '<span>'.$i.'.</span>';
	                $arr[] = '<a  href="'.base_url('admin/site_manager/product/'. $item['id']).'" titel="View Item"><span>'.$item['item_name'].'</span></a><br>'.implode("", $tools);
	                $arr[] = '<a  href="'.base_url('admin/site_manager/product/'. $item['id']).'" titel="View Item"><span>'.$item['serial'].'</span></a><br>';
	                $arr[] = '<span>'.$item['description'].'</span>'; 
	                $arr[] = '<span>'.$item['company'].'</span>';
	               
	                $data[] =$arr;
	                $i++;
	            }
	     
	           $output = array(
	                 "draw" => $dt_att['draw'],
	                 "recordsTotal"    => count($this->Site_manager_model->get_all_products($gid)),
	                 "recordsFiltered" => $this->Site_manager_model->get_products($gid,$dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
	                 "data" => $data
	            );
	      echo json_encode($output);
	      exit();
	  
	     }


        public function product_process(){
        	$img_name    = do_upload("img",'products/product','site_assets/images');
        	$file_name   = do_upload("file",'products/product','site_assets/images');
        	$serial = strtoupper(str_pad(dechex( mt_rand( 0, 1048575 ) ), 7, '0', STR_PAD_LEFT)); 
    			$data = [
    			         'group_id'             => $this->input->post('group_id'),
    			         'item_name'            => $this->input->post('item_name'),
    			         'serial'               => $serial,
    			         'company'              => $this->input->post('company'),
    			         'rank'                 => $this->input->post('rank'),
    			         'description'          => $this->input->post('description'),
    			         'img'                  => $img_name,
    			         'file'                 => $file_name,
    			        ];

				if ($data['img'] !=null || $data['file'] !=null) {
	                if (($this->input->post('old_img') != 0 &&$data['img'] != $this->input->post('old_img')) || ($this->input->post('old_file') != 0 &&$data['file'] != $this->input->post('old_file'))) {
	                	if(file_exists("site_assets/images/products/product/".$this->input->post('old_img'))){
	                	    unlink("site_assets/images/products/product/".$this->input->post('old_img'));
	                     }
	                    if(file_exists("site_assets/images/products/product/".$this->input->post('old_file'))){
	                	    unlink("site_assets/images/products/product/".$this->input->post('old_file'));
	                     }
	                }
	             }else{
	                unset($data['img']);
	                unset($data['file']);	
	            } 

				if (!$this->input->post('id')) {
					$item   = $this->Site_manager_model->get_product_by($serial); 
			            if ($item) {
			            	$this->session->set_flashdata(['alert'=>'Warning','msg'=>'there is an item with the same serial, so please insert it once again!']);
			            	redirect('admin/site_manager/products_index/'.$this->input->post('group_id'));
			              }
					
					$item_id = $this->Site_manager_model->add_product($data);
				    if($item_id){	
				  	  $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
				  	  loger('Create', $this->data['products_module']['id'], $this->data['products_module']['name'], $item_id, 0, 0, json_encode($data, JSON_UNESCAPED_UNICODE), 0, 0,'added product:'.$item_id.'');
		             redirect('admin/site_manager/products_index/'.$this->input->post('group_id'));

				     }
				   }else{
				   	unset($data['serial']);
	                $item   = $this->Site_manager_model->get_product($this->input->post('id'));
				   	$updated = $this->Site_manager_model->update_product($this->input->post('id'),$data);
				   	if ($updated) {
				   	   $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
				   	    loger('update',$this->data['products_module']['id'],$this->data['products_module']['name'],$item['id'],0,json_encode($item, JSON_UNESCAPED_UNICODE),json_encode($data, JSON_UNESCAPED_UNICODE),0,0,'updated product: '.$item['id']);
				   	 }

	               redirect('admin/site_manager/product/'.$this->input->post('id'));
			   }      
		  
	       }


	    public function product($item_id){
          access_checker($this->data['products_permission']['view'],0,0,0,0,'admin/dashboard');	
          $data['item']  = $this->Site_manager_model->get_product($item_id);
          $data['category'] = $this->Site_manager_model->get_category($data['item']['group_id']);
	        if ($data['item']['deleted'] == 1) {
	         	 $this->session->set_flashdata(['alert'=>'Sorry','msg'=>'This Record has been Deleted before!']);
	         	 redirect('admin/items/items/index/'.$data['group']['id']);
	           }

			$data['view'] = 'admin/site_manager/product_manager';
		    $this->load->view('admin/includes/layout',$data);
	      } 

	    public function delete_product($id){
	      access_checker(0,0,0,$this->data['products_permission']['remove'],0,'');
          $item = $this->Site_manager_model->get_product($id);
	      $this->db->update('site_group_items', array('deleted' => 1), "id = ".$id);
          loger('Delete', $this->data['products_module']['id'], $this->data['products_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'Deleted product:'.$item['id'].'');
          echo json_encode(true);
          exit();

		} 

		public function reopen_product($id){
	        access_checker(0,0,0,$this->data['products_permission']['remove'],0,'');
	        $item = $this->Site_manager_model->get_product($id);
	        $this->db->update('site_group_items', array('deleted' =>0), "id = ".$id);
         	  loger('Reopened', $this->data['products_module']['id'], $this->data['products_module']['name'], $item['id'],0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 0,'reopened product:'.$item['id'].'');
	        echo json_encode(true);
            exit();
		}      

// ------------------------------ Clients Requests manager ---------------------------------

      public function clients_req_manager(){
        access_checker($this->data['clients_req_permission']['view'],0,0,0,0,'admin/dashboard'); 
        $data['view']          = 'admin/site_manager/clients_req_manager';
        $this->load->view('admin/includes/layout',$data);
      }  

     public function clients_req_ajax(){
        $dt_att  = $this->datatables_att();
        $items   = $this->Site_manager_model->get_clients_req($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');
        $data = array();
        $i    =  1;
         foreach($items as $item) {
             $arr = array(); 
             $tools=array();
                //if ($item['status'] ==0) {
                      $tools[] = '<div class="info">
                                   <a href="javascript: void(0);" onclick="get_mailerView('.$item['id'].',`'.$item['email'].'`)">
                                      <span style="color:red;">Send</span>
                                   </a>';
                      // }

                  $arr[] = '<span>'.$i.'.</span>';
                  $arr[] = '<span style="color:'.$item['status_color'].'">'.$item['client_name'].'</span><br>'.implode("", $tools);
                  $arr[] = '<span style="color:'.$item['status_color'].'">'.$item['email'].'</span>';
                  $arr[] = '<span style="color:'.$item['status_color'].'">'.$item['subject'].'</span>';
                  $arr[] = '<span style="color:'.$item['status_color'].'">'.$item['message'].'</span>';
                  $arr[] = '<span style="color:'.$item['status_color'].'">'.$item['mail_status'].'</span>';
                  $data[] =$arr;
                  $i++;
              }
       
             $output = array(
                   "draw" => $dt_att['draw'],
                   "recordsTotal"    => count($this->Site_manager_model->get_all_clients_req()),
                   "recordsFiltered" => $this->Site_manager_model->get_clients_req($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
                   "data" => $data
              );
        echo json_encode($output);
        exit();
    
       }

    public function client_mailer(){
      $formdata=[];
      parse_str($this->input->post('formdata'),$formdata);     
      $updateData['status'] ='22'; 
      $updated = $this->Site_manager_model->update_client_mail($formdata['client_id'],$updateData);
      $result = mail_clients($formdata['email'],$formdata['subject'],$formdata['message']);
      echo json_encode(true);
      exit();  
              
    }
// ------------------------------ booking Requests manager ---------------------------------
 public function bookings_manager(){
        access_checker($this->data['bookings_permission']['view'],0,0,0,0,'admin/dashboard'); 
        $data['view']          = 'admin/site_manager/booking_manager';
        $this->load->view('admin/includes/layout',$data);
     }  
     public function booking_ajax(){
        $dt_att  = $this->datatables_att();
        $statuss      = $this->Site_manager_model->get_site_metaData('booking_status','multi');
        $items   = $this->Site_manager_model->get_bookings($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');
        $data = array();
        $i    =  1;
         foreach($items as $item) {
            $arr = array(); 
            $tools=array();
            $status_opt = '';
            foreach ( $statuss as $status) {
              $status_opt .= '<a style="margin-left:5px;" href="javascript: void(0);"
                              onclick="changer('.$item['id'].',`admin/site_manager`,`bookings`,`status`,'.$status['id'].')">
                                    <b style="color:'.$status['paragraph'].'">'.$status['header'].'</b>
                              </a>
                              <div class="dropdown-divider"></div>';
              }
                      $tools[] = '<a class="text-dark" href="javascript: void(0);" data-toggle="dropdown" aria-haspopup="true"
                                      aria-expanded="false">
                                      <button type="button" class="btn btn-outline-secondary btn-sm" style="color:'.$item['status_color'].'">'.$item['header'].'</button>
                                   </a>
                                  <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -2px, 0px); top: 0px; left: 0px; will-change: transform;">
                                  <div style="margin-top: 15px;">
                                  '.$status_opt.'
                                  </div>
                                </div>
                            </div>';
                  $arr[] = '<span>'.$i.'.</span>';
                  $arr[] = '<strong class="text-dark">Hotel Name: </strong><span>'.$item['hotel_name'].'</span><br>
                            <strong class="text-dark">Activity Name: </strong><span>'.$item['activity_name'].'</span><br>
                            <strong class="text-dark">Sub Activity Name: </strong><span>'.$item['sub_name'].'</span>';
                  $arr[] = '<span style="color:'.$item['status_color'].'">
                               <strong class="text-dark">Guest: </strong>'.$item['first_name'].' '.$item['last_name'].'<br>
                               <strong class="text-dark">Email: </strong>'.$item['email'].'<br>
                               <strong class="text-dark">Phone: </strong>'.$item['phone'].'
                            </span>
                            <br>'.implode("", $tools);
                  $arr[] = '<span style="color:'.$item['status_color'].'">'.$item['date'].'</span>';
                  $arr[] = '<span style="color:'.$item['status_color'].'">'.$item['pax'].'</span>';
                  $arr[] = '<span style="color:'.$item['status_color'].'">'.return_money_formater($item['price'],$item['currency']).'</span>';
                  $arr[] = '<span style="color:'.$item['status_color'].'">'.return_money_formater($item['total_cost'],$item['currency']).'</span>';
                  $data[] =$arr;
                  $i++;
              }
       
             $output = array(
                   "draw" => $dt_att['draw'],
                   "recordsTotal"    => count($this->Site_manager_model->get_all_bookings()),
                   "recordsFiltered" => $this->Site_manager_model->get_bookings($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
                   "data" => $data
              );
        echo json_encode($output);
        exit();
    
       }
    public function status_change($id, $column, $status_id){
      access_checker(0,0,$this->data['bookings_permission']['edit'],0,0,'');
      status_changer('bookings','status', $status_id, $id);
      loger('Status Change', $this->data['bookings_module']['id'], $this->data['bookings_module']['name'],$id,0, 0, 0, 0, 0,'Changed booking status:'.$id.'');
      echo json_encode(true);
      exit();
    }
//============================================== language manager =============================
  public function lang_manager(){
        access_checker($this->data['language_permission']['view'],0,0,0,0,'admin/dashboard'); 
        $data['view']          = 'admin/site_manager/lang_manager';
        $this->load->view('admin/includes/layout',$data);
      }  
    
   public function lang_manager_ajax(){
      $dt_att  = $this->datatables_att();
      $rows   = $this->Site_manager_model->get_languages($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');

      $data = array();
      $i    =  1;
       foreach($rows as $row) {
           $arr = array(); 
           $tools=array();
                if ($this->data['language_permission']['edit'] == 1) {
                   $tools[] = "<div class='info'>
                                  <a href='javascript: void(0);' onclick='fillLanguageModal(".json_encode($row).")'>
                                     <span style='color:blue;'>Edit</span>
                                  </a> 
                               </div> ";
                     }

                $arr[] = '<span>'.$i.'.</span>';
                $arr[] = '<a  href="javascript: void(0);" ><span class="text-danger">'.$row['english'].'</span></a><br>'.implode("", $tools);
                $arr[] = '<span>'.$row['german'].'</span>'; 
                $arr[] = '<span>'.$row['french'].'</span>'; 
                $arr[] = '<span>'.$row['arabic'].'</span>';
               
                $data[] =$arr;
                $i++;
            }
     
           $output = array(
                 "draw" => $dt_att['draw'],
                 "recordsTotal"    => count($this->Site_manager_model->get_all_languages()),
                 "recordsFiltered" => $this->Site_manager_model->get_languages($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
                 "data" => $data
            );
      echo json_encode($output);
      exit();
  
     }

     public function lang_translator(){
      access_checker(0,0,$this->data['language_permission']['edit'],0,0,'admin/site_manager');
        if($this->input->post()){
          $fdata      = array(
                  'english'    => $this->input->post('english'),
                  'german'     => $this->input->post('german'),
                  'french'     => $this->input->post('french'),
                  'arabic'     => $this->input->post('arabic'),
                  );
          if ($this->input->post('id')) {
                  $item_data   = $this->Site_manager_model->get_language($this->input->post('id'));
                  $updated     = $this->Site_manager_model->update_language($this->input->post('id'),$fdata);
                  if ($updated > 0) {
                      loger('update',$this->data['language_module']['id'],$this->data['language_module']['name'],$item_data['id'],0,json_encode($item_data, JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'updated language: '.$item_data['id']);
                  }
              }else{
                $item_id = $this->Site_manager_model->add_language($fdata);
               if ($item_id) {
                loger('Create', $this->data['language_module']['id'], $this->data['language_module']['name'], $item_id, 0, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added language:'.$item_id.'');
                 }
              }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Records have been updated Successfully!']);
        redirect('admin/site_manager/lang_manager');
       }     
    } 

//=================================================== end of language manager module
   public function notifications(){
    if (!$this->input->is_ajax_request()) {
         redirect('admin/dashboard'); 
      }
        $notifs = $this->General_model->get_notifications($this->data['user_id'],$this->data['ubranches'],$this->data['dep_id']);
          
            $data = array();

            $count = 0;
            
            if ($notifs) {
              
              foreach ($notifs as $notifs) {
            
          $data[] =$notifs;

          $count +=1;
          }
        $data['count'] = $count;  
        
            }else{
           
              $data[] =['key'=>'<div class="dropdown-divider"></div><h5 class="m-b-0 centered">لا توجد إخطارات  </h5><br>'];
           
            }

         echo json_encode($data);
            
           
           exit();

      } 


    public function get_search_results(){
         
          $s_results = $this->General_model->search_in_mc($this->data['ubranches'],$this->input->post('search_key'));
            
              $data = array();

              $count = 0;
              
              if ($s_results) {
              
            $data[] =$s_results;

            }else{
             
                $data[] =['key'=>'<div class="dropdown-divider"></div><h5 class="m-b-0 centered">No results found</h5><br>'];
             
              }

           echo json_encode($data);
              
             
             exit();

        }     


    public function delete_notify($id){

        $this->db->update('notifications', array('readedby' =>$this->data['user_id'],'deleted' => 1,'status' => 1), "id = ".$id);

        $this->notifications();
      }

	

}

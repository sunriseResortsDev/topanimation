<?php defined('BASEPATH') OR exit('No direct script access allowed');
  class Cron extends CI_Controller{
    function __construct() {
      parent::__construct();
      $this->load->helper('mail_helper');
    }

    public function backup_me(){
        $this->load->helper('file');
        $this->load->dbutil();
        $prefs = array(     
            'format'      => 'zip',             
            'filename'    => 'store_backup.sql'
            );
        $backup =& $this->dbutil->backup($prefs); 
        $db_name = 'store_backup-on-'. date("Y-m-d-H-i-s") .'.zip';
        write_file(FCPATH.'backups/'.$db_name, $backup); 
        $this->load->helper('download');
        force_download($db_name, $backup);
      } 
   
  }

?>
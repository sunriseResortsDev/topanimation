<?php

	class MY_Controller extends CI_Controller{
		
		function __construct(){
			parent::__construct();
		    	$this->load->model('admin/User_model');
                $this->load->model('admin/General_model');
                $this->load->model('admin/log_model');
                $this->load->model('admin/Branches_model');
			        if (!$this->session->has_userdata('is_admin_login')) {
	        			    if (strpos($this->uri->uri_string(), 'admin/auth/login') === FALSE) {
	                		     	$this->session->set_userdata(array(
	                    			'red_url' => $this->uri->uri_string()
	                			));
	        			    }
	        			redirect(base_url('admin/auth/login'));
	    			}
	    			else{       				 
	    				$this->global_data['sessioned_user'] = $this->User_model->get_user_by_id($_SESSION['user_id'], TRUE);
	    				$this->load->vars($this->global_data);
	    			}
                $this->global_data['branches']   = $this->Branches_model->get_branches(get_ubranches($this->global_data['sessioned_user']['id']));	
                require_once(APPPATH.'vendor/autoload.php');

		   }

		function datatables_att(){

			 $draw        = intval($this->input->post("draw"));
	         $start       = intval($this->input->post("start"));
	         $length      = intval($this->input->post("length"));
	         if ($length == '-1') {
	         	 $length ='1000000000000000000';
	         }
	         $order       = $this->input->post("order[0][dir]");
	         $order_col   = $this->input->post("order[0][column]");
	         $col_name    = $this->input->post("columns[$order_col][name]");
	         $search      = trim($this->input->post("search[value]"));

	         $dt_att = [
	         	         'draw'      => $draw,
                         'start'     => $start,
                         'length'    => $length,
                         'order'     => $order,
                         'order_col' => $order_col,
                         'col_name'  => $col_name,
                         'search'    => $search,                         
	                   ];
	               
	         return $dt_att;
		}   


	public function get_submenu($model,$func,$type_id=false){

		   if (!$type_id) {
		   	    if ($this->input->post()) {
		   	    	$type_id = $this->input->post('search');
		   	    }
		      }

			$parts = $this->$model->$func($type_id);
	      
	        $data = array();
	        
	        if ($parts) {
	        	
	          foreach ($parts as $part) {
					
				$data[] =$part;
			  }
			
	        }else{
	       
	        	$data[] =['key'=>'عذرا لا يوجد بيانات لهذا الإختيار  '];
	       
	        }

			   
		   echo json_encode($data);
	        
	       
	       exit();

		} 

     
    public function upload($module_id,$form_id,$folder) {

        $file_name = do_upload("upload",$folder);

        if (!$file_name) {

              die(json_encode($this->data['error']));

          } else {

          	if (!$this->input->post('table')) {
	              $id= $this->General_model->add_file($module_id,$form_id, $file_name,$_SESSION['user_id']);
          	   }else{
          	   	 $id= $this->General_model->add_file_intable($this->input->post('table'),$form_id, $file_name,$_SESSION['user_id'],$module_id);
          	   }


              die("{}");

            }

         }


     
    public function remove_file($form_id, $id,$module_id) {

        $file_name = $_POST['key'];

          if (!$id) {

              die(json_encode($this->data['error']));

          } else {

          	if (!$this->input->post('key')) {
                  
                  $this->General_model->remove_file($id,'',$module_id);

          	  }else{

                  $this->General_model->remove_file($id,$this->input->post('key'),$module_id);
          	  }

              die("{}");

          }

        }  	


        public function check_field($table,$col,$exist_value=''){

			 $item   = $this->General_model->search_in($table,$col,$this->input->post('name'),$exist_value); 

			 if ($item) {
	              echo json_encode('exist');
		          exit();
			   }else{
			   	  echo json_encode('not_exist');
		          exit();
			   }
            
		  }
  

 

  }

?>

    
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 if (!function_exists('initiate_modal')){
   
   function initiate_modal($size='',$head=false,$form_link=false,$body=false){
   
    $ci=get_instance();
    if ($size) {
        $data['size'] = $size;
      }else{
        $data['size'] ='';
      }
    $data['head'] = $head;
    $data['form_link'] = $form_link;
    $data['body'] = $body;
    $ci->load->view('admin/html_parts/modal_body',$data);
  }

}

 if (!function_exists('initiate_alert')){
   
   function initiate_alert(){
   
     $ci=get_instance();

     $ci->load->view('admin/html_parts/alert_body');
  }

}

if (!function_exists('upfiles_js')){
   
   function upfiles_js($cr_path,$uploads,$module_id,$temp_id,$folder,$table=''){
   
    $ci=get_instance();
    
    $data['cr_path']   = $cr_path;
    $data['uploads']   = $uploads;
    $data['module_id'] = $module_id;
    $data['temp_id']   = $temp_id;
    $data['folder']    = $folder;
    $data['table']     = $table;
    $ci->load->view('admin/html_parts/upfiles.js.php',$data);
  }

}



?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 if (!function_exists('get_roles')){
   
   function get_roles(){
   
    $ci=get_instance();
	  return $ci->db->get('user_groups')->result_array();
  }

}

if (!function_exists('get_departments')){
   
   function get_departments(){
   
    $ci=get_instance();
	return $ci->db->get('departments')->result_array();
  }

}


if (!function_exists('status_changer')){
   
   function status_changer($table,$column,$value,$id){
    
    $ci=get_instance();
	
	$ci->db->update($table, array($column => $value), "id = ".$id);
  }
}

 if (!function_exists('get_ubranches')){
   
   function get_ubranches($user_id){
   
    $ci=get_instance();

    $ci->db->select('user_branches.branch_id');
    
    $ci->db->where('user_branches.user_id',$user_id);
    
    $user_hotels= $ci->db->get('user_branches')->result_array();
    
    $uhotels = array();
        
        foreach ($user_hotels as $hotel) {
        
          $uhotels[] = $hotel['branch_id'];
        
        }

     return $uhotels;   
  }

}


if (!function_exists('get_currency')){
   
   function get_currency(){
   
    $ci=get_instance();
    return $ci->db->get('currencies')->result_array();
  }

}


if (!function_exists('get_companies')){
   
   function get_companies(){
   
    $ci=get_instance();
    return $ci->db->get_where('companies',array('deleted'=>0))->result_array();
  }

}


if (!function_exists('get_file_code')){
   
   function get_file_code($table,$start){
   
    $ci=get_instance();
    $ci->db->order_by('timestamp','DESC');
      if ($table) {
          $ci->db->select(''.$table.'.id');
          $row = $ci->db->get(''.$table.'')->row_array();
       }else{
          $ci->db->select('files.id');
          $row = $ci->db->get('files')->row_array();
       }

      if ($row['id']) {
          $row['id'] +=1;
        }else{
         $row['id'] = 1; 
        }
    return ($start.(date('Y').($row['id'])));
  }

}

if (!function_exists('learn_translate')){
  function learn_translate($phrase){
    $ci=get_instance();
    $query = 'SELECT languages.* FROM languages 
              WHERE english = "'.$phrase.'" ';
    $row   = $ci->db->query($query)->row_array();
    $data  = ['english'=>$phrase];
    if (!$row) {
        $ci->db->insert('languages', $data);
    }
   }
}
if (!function_exists('translate')){
  function translate($phrase,$lang){
    $ci=get_instance();
    $query = 'SELECT languages.* FROM languages 
              WHERE english = "'.$phrase.'" OR german = "'.$phrase.'" OR french = "'.$phrase.'" OR arabic = "'.$phrase.'" ';
    $row   = $ci->db->query($query)->row_array();
    if ($row[$lang]) {
        return $row[$lang];
    }else{
         return $phrase;
    }
   
   }
}





?>
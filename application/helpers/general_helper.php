<?php 

if (!function_exists('user_access')){
   
   function user_access($module_id){
     
     $CI = get_instance();
     
     $CI->load->model('admin/User_model');
     
     $user = $CI->User_model->get_user_by_id($_SESSION['user_id'], TRUE);
     
     $user_permissions = $CI->User_model->get_user_access($module_id,$user['id']);
 
     return  $user_permissions;

   }
}


if (!function_exists('access_checker')){
   
   function access_checker($view,$creat,$edit,$remove,$sign,$red){

   	 $CI = get_instance();
     
     if ($view != 1 && $creat != 1 && $edit != 1 && $remove != 1 && $sign != 1 ) {
				
				  $CI->session->set_flashdata(['alert'=>'تحذير ','msg'=>' عذرا هذا الجزء من البرنامج يحتاج صلاحية أعلي  !']);

	              redirect($red);
		}

   }
}


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 if (!function_exists('array_assoc_value_exists')){
   
  function array_assoc_value_exists($arr, $index, $search) {
    foreach ($arr as $key => $value) {
      if ($value[$index] == $search) {
        return TRUE;
      }
    }
    return FALSE;
  }

}


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 if (!function_exists('loger')){
   
   function loger($action,$module_id,$target,$target_id, $mini_target_id,$data=false,$new_data=false,$items=false,$new_items=false,$comment){

     $CI = get_instance();

     $CI->load->model('admin/General_model');

     $user = $CI->User_model->get_user_by_id($_SESSION['user_id'], TRUE);

     $form_data = array(
				         'user_id'          => $user['id'],
				         'action'           => $action,
				         'module_id'        => $module_id,
				         'target'           => $target,
				         'target_id'        => $target_id,
				         'mini_target_id'   => $mini_target_id,
				         'data'             => $data,
				         'new_data'         => $new_data,
				         'items'            => $items,
				         'new_items'        => $new_items,
				         'comments'         =>$comment,
				         'ip'               => $CI->input->ip_address(),
				       );

     $log_id = $CI->General_model->logger($form_data);

   }
}

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 if (!function_exists('exact_data')){
   
   function exact_data($old,$new){ 
     if(is_array($old) && is_array($new)){
   	   $field    = implode(' , ', array_keys( array_diff_assoc(json_decode($old,true), json_decode($new,true))));
     	 return($field);
      }else{
        return($old.' to '.$new) ; 
      }

	 // $was      = implode(',', array_diff_assoc(json_decode($old,true), json_decode($new,true)));
     // $become   = implode(',', array_diff_assoc(json_decode($new,true), json_decode($old,true)));
	 // return ($field." From ".$was." to ".$become);


   }
}

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

   if (!function_exists('do_upload')){
     
       function do_upload($field,$folder,$up_path='') {

        $CI = get_instance();
        if ($up_path) {
         
            $config['upload_path'] = $up_path.'/'.$folder;

        }else{
        
             $config['upload_path'] = 'assets/uploads/'.$folder;

        }

        $config['allowed_types'] = '*';

        $CI->load->library('Upload', $config);

        if ( ! $CI->upload->do_upload($field)) {

              $data['error'] = array('error' => $CI->upload->display_errors());

              return FALSE;

            }else {

              $file = $CI->upload->data();

              return $file['file_name'];

             }
 
         }
     }

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
         // mailer == 1 just normal notify
         // mailer == 2 just mailer
         // mailer == 3 both notify and mailer public to department
         // mailer == 4 both notify and mailer private to one user
     if (!function_exists('notify')){
       function notify($module_id,$to_uid='',$to_branch_id='',$to_dep_id='',$head,$message,$link,$form_id='',$type='',$mail_message='',$role_id='',$mailer=''){

         $CI = get_instance();

         $CI->load->model('admin/General_model');

         $user = $CI->User_model->get_user_by_id($_SESSION['user_id'], TRUE);

         if ($mailer == 1 || $mailer == 3 || $mailer == 4) {
             $form_data = array(
                         'module_id'        => $module_id,
                         'uid'              => $user['id'],
                         'to_uid'           => $to_uid,
                         'to_branch_id'     => $to_branch_id,
                         'to_dep_id'        => $to_dep_id,
                         'head'             => $head,
                         'message'          => $message,
                         'link'             => $link,
                       );

                 if ($mailer == 4) {
                          unset($form_data['to_dep_id']);
                          unset($form_data['to_branches_id']);
                  }

              $notify_id = $CI->General_model->add_notify($form_data);
            }

         if ($mailer == 2 || $mailer == 3 || $mailer == 4) {
           
              $emails = $CI->General_model->get_emails_queue($role_id,$to_hid,$to_dep_id);
           //die(print_r($emails));
              mailer($user['id'],$module_id,$form_id,$type,$emails,$mail_message);

           }

       }
}


 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

     if (!function_exists('mailer')){
       
       function mailer($uid='',$module_id,$form_id,$type,$emails,$message){

         $CI = get_instance();

         $CI->load->model('admin/General_model');

         $form_data = array(
                     'uid'              => $uid,
                     'module_id'        => $module_id,
                     'form_id'          => $form_id,
                     'type'             => $type,
                     'emails'           => $emails,
                     'message'          => $message
                   );

         $notify_id = $CI->General_model->add_mail($form_data);

       }
  }


 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

     if (!function_exists('get_id_bycode')){
       
       function get_id_bycode($table,$code){

         $CI = get_instance();

         $CI->load->model('admin/General_model');

         $req_id = $CI->General_model->get_code_id($table,$code);

         return $req_id['id']; 

       }
  }

  if (!function_exists('get_activityHotels')){
    function get_activityHotels($activity){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $hotels = $CI->General_model->get_activityHotels($activity);
      return $hotels; 
    }
  }

  if (!function_exists('get_activitySubs')){
    function get_activitySubs($activity, $hotel){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $sub_activities = $CI->General_model->get_activitySubs($activity, $hotel);
      return $sub_activities; 
    }
  }

  if (!function_exists('get_subs')){
    function get_subs($reqested){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $sub_activities = $CI->General_model->get_activitySub($reqested);
      return $sub_activities; 
    }
  }

  if (!function_exists('get_offers')){
    function get_offers($activity, $hotel, $sub){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $sub_activities = $CI->General_model->get_offers($activity, $hotel, $sub);
      return $sub_activities; 
    }
  }

  if (!function_exists('get_special')){
    function get_special(){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $sub_activities = $CI->General_model->get_special();
      return $sub_activities; 
    }
  }

   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

     if (!function_exists('money_formater')){
       
       function money_formater($money,$currency){

         $CI = get_instance();

         $CI->load->model('admin/General_model');

         $symbol = $CI->General_model->get_currency_symbol($currency);

         echo($symbol.' '.$money);

       }
  }

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

     if (!function_exists('return_money_formater')){
       
       function return_money_formater($money,$currency){

         $CI = get_instance();

         $CI->load->model('admin/General_model');

         $symbol = $CI->General_model->get_currency_symbol($currency);

         return ($symbol.' '.$money);

       }
  }

  if (!function_exists('status_changer')) {
    function status_changer($table, $column, $value, $id) {
        $ci = get_instance();
        $ci->db->update($table, array($column => $value), "id = " . $id);
    }
}



?>
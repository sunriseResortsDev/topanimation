<?php

if (!function_exists('pdfStyling')) {
	function pdfStyling(){
		$html ='<html>
					<head>
						<style>
							body {font-family: sans-serif;
							  font-size: 10pt;
							 }
							p { margin: 0pt; }
							table.items {
							  border: 0.1mm solid #000000;
							 }
							td { vertical-align: top; }
							.items td {
							  border-left: 0.1mm solid #000000;
							  border-right: 0.1mm solid #000000;
							 }
							table thead td { 
							  background-color: #343a40;
							  color:#FFFFFF;
							  text-align: center;
							  border: 0.1mm solid #000000;
							  border-bottom: 0.1mm solid #000000;
							 }
							.items td.blanktotal {
							  background-color: #EEEEEE;
							  border: 0.1mm solid #000000;
							  background-color: #FFFFFF;
							  border: 0mm none #000000;
							  border-top: 0.1mm solid #000000;
							  border-right: 0.1mm solid #000000;
							 }
							.items td.totals {
							  text-align: right;
							  border: 0.1mm solid #000000;
							 }
							.items td.cost{
							  text-align: "." center;
							 }
							.department-row{
							  background-color :#EEEEEE;
							  border: 0 !important;
							 }
							.total-department-row{
							  background-color :#ccc;
							  color:#FFF !important;
							  border: 0 !important;
							  }
							a{ 
							  color: inherit;
							  text-decoration: none; 
							  } 
						</style>
					</head>';
		return $html;			
	  }
 }


if (!function_exists('report_pdf_generator')){
    function report_pdf_generator($data){ 
    	 $ci=get_instance();
			ob_start();
			ini_set("memory_limit","256M");
			ini_set("pcre.backtrack_limit", "5000000");
			$html = pdfStyling();
			$html.='
					<body>
					<!--mpdf
						<htmlpagefooter name="myfooter">
					    <div style="border-top: 1px solid #000000; font-size: 9pt;padding-top: 3mm; ">
						  <table width="100%">
							 <tr>
				               <td width="35%">'.date("Y-m-d h:i:sa").'</td>
				               <td width="25%">'.$data['username'].'</td>
				               <td width="45%" style="text-align: right;">Page {PAGENO} of {nb}</td>
					         </tr>
						  </table>
					    </div>
						</htmlpagefooter>
						<sethtmlpagefooter name="myfooter" value="on" />
					mpdf-->
					<table width="100%">
						<tr>
							<td width="50%" style="text-align: left;"> <img style="width:200px;height:130px;" src="'.base_url('assets/uploads/logos/'.$data['branch']['logo']).'" alt="user"><br>
							</td>
							<td width="50%" style="text-align: right;"><br><br>
							  <h2 style="color:#198bbe;">'.$data['branch']['branch_name'].'</h2><br>
                              <h3><strong>'.$data['report_name'].'</strong></h3>
					          <h4><strong>من: '.$data['from_date'].' إلى: '.$data['to_date'].'</strong></h4>
							</td>
						</tr>
					</table>
					<br />
					<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
				       <thead>';  

			    if ($data['report_fun']=='') {	       
				          $html .='<tr>  
						            <td width="17%"><strong>الملاحظات  </strong></td>
						            <td width="12%"><strong>الحالة </strong></td>
						            <td width="15%"><strong>توقيت الإستلام</strong></td>
						            <td width="12%"><strong>المدفوع  </strong></td>
						            <td width="12%"><strong>التكلفة </strong></td>
						            <td width="17%"><strong>المورد </strong></td>
						            <td width="12%"><strong>الكود </strong></td>
						          </tr>
						        </thead>
						       <tbody>';    
						    foreach ($data['tableData'] as $row) {
						     $html .='<tr class="'.( strstr( $row['serial'],'الإجمالي')?'department-row':'').'">
						               <td style="text-align: right;"></td>
						               <td style="text-align: right;"></td>
						               <td style="text-align: right;"></td>
						               <td style="text-align: right;"></td>
						               <td style="text-align: right;"></td>
						               <td style="text-align: right;"></td>
						               <td style="text-align: right;"></td>
					                  </tr>';
						        }
			       
			        }
			        
					    $html .= ' </tbody></table></body></html>';   
					   	ob_end_clean();
						return($html);
						 
             }
         }    


?>
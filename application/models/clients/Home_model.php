<?php

	class Home_model extends CI_Model{

        public function get_sliders(){
            $this->db->order_by('slider.rank','ASC');
			return $this->db->get_where('slider')->result_array();	
           }  

        public function get_sit_metaData($meta,$type){
            $this->db->order_by('rank','ASC');
        	if ($type=='single') {
				return $this->db->get_where('site_meta_data',array('meta'=>$meta))->row_array();
        	}elseif($type=='multi'){
                return $this->db->get_where('site_meta_data',array('meta'=>$meta))->result_array();
        	}

         }     

        public function get_categories(){
            $this->db->order_by('rank','ASC');
            return $this->db->get_where('site_groups',array('deleted'=>0))->result_array();

         }  

         public function get_category($serial){
            return $this->db->get_where('site_groups',array('serial'=>$serial,'deleted'=>0))->row_array();

         }  

         public function get_category_items($cat_id){
         	$this->db->order_by('site_group_items.rank','ASC');
            return $this->db->get_where('site_group_items',array('group_id'=>$cat_id,'deleted'=>0))->result_array();
         } 

         public function get_gallery_unique(){
         	$this->db->select('site_meta_data.points');
         	$this->db->order_by('site_meta_data.rank','ASC');
         	$this->db->where(array('meta'=>'gallery'));
         	$this->db->group_by('site_meta_data.points');
            return $this->db->get_where('site_meta_data')->result_array();
         }

         public function get_portfolio_images($limit){
                $this->db->limit($limit);
                return $this->db->get_where('site_meta_data',array('meta'=>'gallery'))->result_array();

         } 

        public function add_client_mail($data){
          $this->db->insert('clients_mail_queue', $data);
          return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
         }             

        public function add_booking_trip($data){
          $this->db->insert('bookings', $data);
          return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
         }             


  }
?>	
<?php

	class General_model extends CI_Model{
       
       	function get_activities(){
		    $this->db->select('menu.*, activity.name');
		    $this->db->join('activity', 'menu.activity_id = activity.id','left');
		    $this->db->group_by('activity_id');
		    $this->db->order_by('menu.rank','ASC');
		    return $this->db->get('menu')->result_array();
  	    }

  	    function get_activityHotels($activity_id){
		    $this->db->select('menu.*, hotel.name');
		    $this->db->join('hotel', 'menu.hotel_id = hotel.id','left');
		    $this->db->where('menu.activity_id', $activity_id);
		    $this->db->group_by('hotel_id');
		    $this->db->order_by('menu.rank','ASC');
		    return $this->db->get('menu')->result_array();
  	    }

  	    function get_activitySubs($activity_id, $hotel_id){
		    $this->db->select('menu.*, sub_activity.name');
		    $this->db->join('sub_activity', 'menu.sub_id = sub_activity.id','left');
		    $this->db->where('menu.activity_id', $activity_id);
		    $this->db->where('menu.hotel_id', $hotel_id);
		    $this->db->group_by('sub_id');
		    $this->db->order_by('menu.rank','ASC');
		    return $this->db->get('menu')->result_array();
  	    }

  	    function get_activitySub($id){
		    $this->db->select('menu.*, activity.name AS activity_name, hotel.name AS hotel_name, sub_activity.name AS sub_name');
		    $this->db->join('activity', 'menu.activity_id = activity.id','left');
		    $this->db->join('hotel', 'menu.hotel_id = hotel.id','left');
		    $this->db->join('sub_activity', 'menu.sub_id = sub_activity.id','left');
		    $this->db->where('menu.id', $id);
		    return $this->db->get('menu')->row_array();
  	    }

  	    function get_offers($activity, $hotel, $sub){
		    $this->db->select('offers.*, activity.name AS activity_name, hotel.name AS hotel_name, sub_activity.name AS sub_name');
		    $this->db->join('activity', 'offers.activity_id = activity.id','left');
		    $this->db->join('hotel', 'offers.hotel_id = hotel.id','left');
		    $this->db->join('sub_activity', 'offers.sub_id = sub_activity.id','left');
		    $this->db->where('offers.activity_id', $activity);
		    $this->db->where('offers.hotel_id', $hotel);
		    $this->db->where('offers.sub_id', $sub);
		    return $this->db->get('offers')->result_array();
  	    }

  	    function get_offer($id){
		    $this->db->select('offers.*, activity.name AS activity_name, hotel.name AS hotel_name, sub_activity.name AS sub_name');
		    $this->db->join('activity', 'offers.activity_id = activity.id','left');
		    $this->db->join('hotel', 'offers.hotel_id = hotel.id','left');
		    $this->db->join('sub_activity', 'offers.sub_id = sub_activity.id','left');
		    $this->db->where('offers.id', $id);
		    return $this->db->get('offers')->row_array();
  	    }

  	    function get_special(){
		    $this->db->select('offers.*, activity.name AS activity_name, hotel.name AS hotel_name, sub_activity.name AS sub_name');
		    $this->db->join('activity', 'offers.activity_id = activity.id','left');
		    $this->db->join('hotel', 'offers.hotel_id = hotel.id','left');
		    $this->db->join('sub_activity', 'offers.sub_id = sub_activity.id','left');
		    $this->db->where('offers.special', 1);
		    return $this->db->get('offers')->result_array();
  	    }
         
       public function logger($data) {

			$this->db->insert('log', $data);

			return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
		
		} 

         
        function get_file($module_id,$form_id){

		    $this->db->select('files.*,users.fullname');

		    $this->db->join('users', 'files.user_id = users.id','left');
		   
		    $this->db->where('files.module_id', $module_id);
		   
		    $this->db->where('files.form_id', $form_id);
		   
		    return $this->db->get('files')->result_array();
  	      
  	      }

  	    function get_file_from_table($form_id,$table){

		    $this->db->select(''.$table.'.*,users.fullname');

		    $this->db->join('users', ''.$table.'.user_id = users.id','left');
		   
		    $this->db->where(''.$table.'.form_id', $form_id);
		   
		    return $this->db->get(''.$table.'')->result_array();
  	      
  	      }  

  	    function get_last_file($module_id,$form_id){

		    $this->db->select('files.*,users.fullname');

		    $this->db->join('users', 'files.user_id = users.id','left');
		   
		    $this->db->where('files.module_id', $module_id);
		   
		    $this->db->where('files.form_id', $form_id);

		    $this->db->order_by('id','DESC');
		   
		    return $this->db->get('files')->row_array();
  	      
  	      }  

     	function remove_file($id,$table='',$module_id) {
           if ($table) {
           	  $file = $this->db->get_where(''.$table.'',array('id'=>$id))->row_array();
		      $this->db->query('DELETE FROM '.$table.' WHERE id = '.$id);
		      loger('Delete',$module_id,'Files', $file['form_id'],$id,json_encode($file, JSON_UNESCAPED_UNICODE),json_encode(array('table'=>$table), JSON_UNESCAPED_UNICODE),0,0,'Deleted file No#.' .$id.'from  '.$table);
		    }else{
		      $file = $this->db->get_where('files',array('id'=>$id))->row_array();	
		      $this->db->query('DELETE FROM files WHERE id = '.$id);
		      loger('Delete',$module_id,'Files', $file['form_id'],$id,json_encode($file, JSON_UNESCAPED_UNICODE),json_encode(array('table'=>$table), JSON_UNESCAPED_UNICODE),0,0,'Deleted file No#.' .$id.'from g-files ');
		    }
		  
		  }	


		function add_file($module_id,$form_id, $name, $user_id) {
			
	  		$this->db->query('INSERT INTO files(module_id,form_id, file_name, user_id) VALUES("'.$module_id.'","'.$form_id.'","'.$name.'","'.$user_id.'")');
	  		loger('Create',$module_id,'Files',$form_id,0,json_encode(array('table'=>'files','file_name'=>$name), JSON_UNESCAPED_UNICODE),0,0,0,'added file: '.$name.'');
	  	 }

	  	function add_file_intable($table,$form_id, $name, $user_id,$module_id) {

	  		$this->db->query('INSERT INTO '.$table.'(form_id, file_name, user_id) VALUES("'.$form_id.'","'.$name.'","'.$user_id.'")');
	  	    loger('Create',$module_id,'Files',$form_id,0,json_encode(array('table'=>$table,'file_name'=>$name), JSON_UNESCAPED_UNICODE),0,0,0,'added file:'.$name.'' );
	  	 } 


	  	function update_files($assumed_id, $module_id,$form_id,$table='') {
	     if ($table) {
	        $file = $this->db->get_where(''.$table.'',array('form_id'=>$assumed_id))->result_array();
	     	 $this->db->query('UPDATE '.$table.' SET form_id = "'.$form_id.'" WHERE form_id = "'.$assumed_id.'"' );
	     	 loger('Update',$module_id,'Files',$form_id,0,json_encode($file, JSON_UNESCAPED_UNICODE),json_encode(array('table'=>'files'), JSON_UNESCAPED_UNICODE),0,0,'updated files of form No#. : '.$form_id );
	        }else{  	
	        $file = $this->db->get_where('files',array('form_id'=>$assumed_id))->result_array();
	        $this->db->query('UPDATE files SET form_id = "'.$form_id.'" WHERE form_id = "'.$assumed_id.'" AND module_id ="'.$module_id.'" ' );
	        loger('Update',$module_id,'Files',$form_id,0,json_encode($file, JSON_UNESCAPED_UNICODE),json_encode(array('table'=>$table), JSON_UNESCAPED_UNICODE),0,0,'updated files of form No#. : '.$form_id );
	       }
   		    return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
  	     
  	     } 

		public function get_count($table,$branches_ids='',$del='',$status=''){
							    
			    if ($branches_ids) {
			 
				    $this->db->where_in($table.'.branch_id',$branches_ids);

			    }
              
               if ($del) {
               	
			    $this->db->where($table.'.deleted',0);

               }

               if ($status) {
               	
			    $this->db->where($table.'.'.$status,$status);

               }

				return $this->db->get($table)->num_rows();
			
			}

         public function search_in($table,$col,$name,$exist_value=''){
               
               if (!$exist_value) {

				       return $this->db->get_where($table,array($col => $name,'deleted'=> 0))->row_array();

                  }elseif($exist_value){

                  	return $this->db->get_where($table,array($col => $name,'deleted'=> 0,$col.'!=' => str_replace ("%20", " ", $exist_value)))->row_array();
                  
                  }

             }  


         
         public function add_notify($data) {
           $dep_users = $this->db->get_where('users',array('deleted'=>0,'disabled'=>0,'department'=>$data['to_branch_id']))->result_array();
		   foreach ($dep_users as $user) {
		   	   unset($data['to_dep_id']);
		   	   $data['to_uid'] = $user['id'];
			   $this->db->insert('notifications', $data);
		     }
		   return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
		
		 }    


         public function get_notifications($uid,$ubranches='',$dep_id = ''){

         	 $query = 'SELECT notifications.*
		          	   	FROM notifications 
	  	    	        WHERE notifications.deleted = 0 AND 
	  	    	        (to_branch_id IN ('.implode(',',$ubranches).') AND to_dep_id = '.$dep_id.' OR to_uid = '.$uid.')
	  	    	        ORDER BY timestamp DESC';
 
             $records = $this->db->query($query);
             
             return $records->result_array();
           
           }     


		   public function get_emails_queue($role_id,$branches_ids,$dep_id=''){

	         	 $query = 'SELECT user_ubranches.*
			          	   	FROM user_ubranches 
		  	    	        WHERE user_ubranches.branch_id = '.$branches_ids.' AND  user_ubranches.role_id like "%'.$role_id.'%"';
	 
	             $records = $this->db->query($query);
	             
	             $rows    =  $records->result_array();
                 //die(print_r($rows));
	             $uids = array();
	              
	              foreach ($rows as $row) {
	             	 array_push($uids, $row['user_id']);
	               }

	               if ($dep_id) {
	                   $this->db->select('users.email');
	                   $this->db->where_in('users.id',$uids);
	                   $emails = $this->db->get_where('users',array('department'=>$dep_id))->result_array();
	               }else{
	                   $this->db->select('users.email');
	                   $this->db->where_in('users.id',$uids);
	                   $emails = $this->db->get('users')->result_array();
	               }
	               $qemails = array();
	               foreach ($emails as $email) {
	               	array_push($qemails, $email['email']);
	               }

	              return implode(',',$qemails);
           
           }        
	

	       public function add_mail($data) {

				  $this->db->insert('mails_queue', $data);

				  return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
			
			 }


			public function get_code_id($table,$code) {
                  $this->db->select(''.$table.'.id');
                  $this->db->where(''.$table.'.code',$code);             
                  return $this->db->get(''.$table.'')->row_array();
			
			 } 


			public function get_currency_symbol($currency_name) {
                  $this->db->select('currencies.symbol');
                  $this->db->where('currencies.name',$currency_name);             
                  $row = $this->db->get('currencies')->row_array();
			      return $row['symbol'];
			 }   

            public function get_font_icons(){
		     return $this->db->get('font_icons')->result_array();
            }          	       	  


   }

?>
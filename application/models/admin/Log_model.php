<?php

	class Log_model extends CI_Model{

		public function get_logs($start,$length,$search=false,$order,$col_name,$filter_count){
		       if ($search) {

		    	         $query = 'SELECT log.*,modules.name AS module_name,users.fullname
		    	                FROM log
		    	                LEFT JOIN modules ON log.module_id = modules.id
		    	                LEFT JOIN users ON log.user_id = users.id
			  	    	        WHERE
			  	    	       (action like "%'.$search.'%" OR target like "'.$search.'%" OR modules.name like "'.$search.'%"
			  	    	        OR users.fullname like "'.$search.'%"  OR comments like "'.$search.'%"  OR log_time like "'.$search.'%") 
			  	    	        ORDER BY '.$col_name.' '.$order.'';

		          	if ($filter_count == 'count') { 

		      	     	 $records = $this->db->query($query);
		                 return $records->num_rows();

			           }else{

		      	     	 $query .=' LIMIT '.$start.','.$length.'';
			             $records = $this->db->query($query);
			             return $records->result_array();

			            }   

		          }elseif(!$search){

		          		 $this->db->select('log.*,modules.name AS module_name,users.fullname');
		          		 $this->db->join('modules','log.module_id = modules.id','left');
		          		 $this->db->join('users','log.user_id = users.id','left');
	                     $this->db->order_by($col_name,$order);
	              
	              if ($filter_count == 'count') {
	    
		                 return $this->db
		              	 ->get("log")
		                 ->num_rows();
	              
	              }else{

			             return $this->db
			             ->limit($length,$start)
			             ->get("log")
			             ->result_array();
	              	
	              }   
	          }
	    }


        public function get_all_logs(){
			
			return $this->db->get('log')->result_array();

		  }  


		public function get_log($id){
			$this->db->select('log.*,modules.name AS module_name,users.fullname');
			$this->db->join('modules','log.module_id = modules.id','left');
			$this->db->join('users','log.user_id = users.id','left');
			$this->db->where('log.id',$id);
			return $this->db->get('log')->result_array();
		}  





  }
?>	
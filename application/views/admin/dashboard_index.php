  <?php
   $slider_permission           = user_access(13);
   $about_permission            = user_access(14);
   $services_permission         = user_access(15);
   $products_permission         = user_access(16);
   $portfolio_permission        = user_access(17);
   $blog_permission             = user_access(18);
   $contact_permission          = user_access(19);
   $footer_permission           = user_access(20);
   $clients_req_permission      = user_access(21);
   $gallery_permission          = user_access(22);
   $team_permission             = user_access(23);
   $offer_permission            = user_access(24);
   $language_permission         = user_access(25);
   $bookings_permission         = user_access(26);
 ?>

<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row">
      <?php if($slider_permission['view'] == 1){?> 
        <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/slider_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-sliders-h" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Slider'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>  
      <?php if($about_permission['view'] == 1){?>
        <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/about_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-info-circle" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'About'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>  
      <?php if($services_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/service_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-location-arrow" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Services'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?> 
      <?php if($products_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/category_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-list-ol" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Products'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>
       <?php if($portfolio_permission['view'] == 1){?>
        <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/portfolio_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-laptop" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Portfolio'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>  
      <?php if($gallery_permission['view'] == 1){?>
        <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/gallery_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-cloud-upload-alt" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'gallery'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>  
      <?php if($team_permission['view'] == 1){?>
        <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/team_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-address-book" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Team'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>  
      <?php if($blog_permission['view'] == 15){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/blog_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-warehouse" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Blog'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?> 
      <?php if($contact_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/contact_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-user-circle" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Contact'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>
      <?php if($footer_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/footer_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-copyright" style="font-size:150%;"></i></h1>
                    <h6 class="text-white"><?php echo 'Footer'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>
      <?php if($clients_req_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/clients_req_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-users" style="font-size:150%;"></i></h1>
                    <h6 class="text-white" style="font-size:12px;"><?php echo 'Clients Requests Manager'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>
      <?php if($bookings_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/bookings_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-users" style="font-size:150%;"></i></h1>
                    <h6 class="text-white" style="font-size:12px;"><?php echo 'Bookings Manager'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>
      <?php if($offer_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/offer_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-globe" style="font-size:150%;"></i></h1>
                    <h6 class="text-white" style="font-size:12px;"><?php echo 'Offers Manager'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>
      <?php if($language_permission['view'] == 1){?>
         <div class="col-md-6 col-lg-2 col-xlg-3">
          <a href="<?php echo base_url('admin/site_manager/lang_manager');?>">
            <div class="card card-hover">
                <div class="box bg-dark text-center">
                    <h1 class="font-light text-white"><i class="fas fa-globe" style="font-size:150%;"></i></h1>
                    <h6 class="text-white" style="font-size:12px;"><?php echo 'Languages Translator'?></h6>
                </div>
            </div>
          </a>
        </div>   
      <?php }?>
    </div> 
  </div>

<?php
   $setting_permission          = user_access(1);
   $slider_permission           = user_access(13);
   $about_permission            = user_access(14);
   $services_permission         = user_access(15);
   $products_permission         = user_access(16);
   $portfolio_permission        = user_access(17);
   $blog_permission             = user_access(18);
   $contact_permission          = user_access(19);
   $footer_permission           = user_access(20);
   $clients_req_permission      = user_access(21);
   $gallery_permission          = user_access(22);
   $team_permission             = user_access(23);
   $offer_permission            = user_access(24);
   $language_permission         = user_access(25);
?>

<aside class="left-sidebar no-print  float-right" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar" >
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item"> 
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin/site_manager');?>" aria-expanded="false">
                               <i class="fas fa-bars"></i><span class="hide-menu"><strong>Dashboard</strong></span>
                           </a>
                        </li>
                        <?php if($slider_permission['view']==1){?>
                          <li class="sidebar-item">
                             <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/slider_manager');?>" aria-expanded="false"><i class="fas fa-sliders-h"></i><span class="hide-menu"><strong>Sliders</strong></span></a>
                          </li>
                        <?php }?> 
                        <?php if($about_permission['view']==1){?>
                          <li class="sidebar-item"> 
                              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin/site_manager/about_manager');?>" aria-expanded="false"><i class="fas fa-info-circle"></i><span class="hide-menu"><strong>About</strong></span>
                              </a>
                          </li>
                         <?php }?>
                         <?php if($services_permission['view']==1){?> 
                           <li class="sidebar-item">
                             <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/service_manager');?>" aria-expanded="false"><i class="fas fa-location-arrow"></i><span class="hide-menu"><strong>Services</strong></span></a>
                          </li>
                         <?php }?> 
                         <?php if($products_permission['view']==1){?>
                          <li class="sidebar-item"> 
                              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin/site_manager/category_manager');?>" aria-expanded="false"><i class="fas fa-list-ol"></i><span class="hide-menu"><strong>Categories</strong></span></a>
                          </li>
                         <?php }?> 
                         <?php if($portfolio_permission['view']==1){?>
                          <li class="sidebar-item">
                             <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/portfolio_manager');?>" aria-expanded="false"><i class="fas fa-laptop"></i><span class="hide-menu"><strong>Portfolio</strong></span></a>
                          </li>
                        <?php }?> 
                        <?php if($gallery_permission['view']==1){?>
                          <li class="sidebar-item">
                             <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/gallery_manager');?>" aria-expanded="false"><i class="fas fa-cloud-upload-alt"></i><span class="hide-menu"><strong>Gallery</strong></span></a>
                          </li>
                        <?php }?> 
                         <?php if($team_permission['view']==1){?>
                          <li class="sidebar-item">
                             <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/team_manager');?>" aria-expanded="false"><i class="fas fa-address-book"></i><span class="hide-menu"><strong>Team</strong></span></a>
                          </li>
                        <?php }?> 
                        <?php if($blog_permission['view']==1){?>
                          <li class="sidebar-item"> 
                              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin/site_manager/blog_manager');?>" aria-expanded="false"><i class="fas fa-warehouse"></i><span class="hide-menu"><strong>Blog</strong></span>
                              </a>
                          </li>
                         <?php }?>
                         <?php if($contact_permission['view']==1){?> 
                           <li class="sidebar-item">
                             <a class="sidebar-link waves-dark" href="<?php echo base_url('admin/site_manager/contact_manager');?>" aria-expanded="false"><i class="fas fa-user-circle"></i><span class="hide-menu"><strong>Contact US</strong></span></a>
                          </li>
                         <?php }?> 
                         <?php if($footer_permission['view']==1){?>
                          <li class="sidebar-item"> 
                              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin/site_manager/footer_manager');?>" aria-expanded="false"><i class="fas fa-copyright"></i><span class="hide-menu"><strong>Footer</strong></span></a>
                          </li>
                         <?php }?> 
                         <?php if($clients_req_permission['view']==1){?>
                          <li class="sidebar-item"> 
                              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin/site_manager/clients_req_manager');?>" aria-expanded="false"><i class="fas fa-users"></i><span class="hide-menu"><strong>Clients Requests Manager</strong></span></a>
                          </li>
                         <?php }?>
                         <?php if($offer_permission['view']==1){?>
                          <li class="sidebar-item"> 
                              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin/site_manager/offer_manager');?>" aria-expanded="false"><i class="fas fa-globe"></i><span class="hide-menu"><strong>Offers Manager</strong></span></a>
                          </li>
                         <?php }?>
                         <?php if($language_permission['view']==1){?>
                          <li class="sidebar-item"> 
                              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin/site_manager/lang_manager');?>" aria-expanded="false"><i class="fas fa-globe"></i><span class="hide-menu"><strong>Languages Translator</strong></span></a>
                          </li>
                         <?php }?>
                         <?php if($setting_permission['view']==1){?>
                          <li class="sidebar-item">
                            <a href="<?php echo base_url('admin/log_activity');?>" class="sidebar-link"><i class="fas fa-shield-alt"></i><span class="hide-menu"><strong>Listener</strong></span></a>
                          </li>
                          <li class="sidebar-item">
                            <a href="<?php echo base_url('admin/log_activity/backup_me');?>" class="sidebar-link"><i class="fas fa-database"></i><span class="hide-menu"><strong>DB Backup</strong></span></a>
                          </li>  
                         <?php }?>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>

    <div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                         <span class="page-title-toSave hidden">Client Profile</span>
                         <?php if($this->data['permission']['edit'] == 1){?>
                                <button id="add-new" data-toggle="modal" data-target="#smallmodal" class="btn btn-outline-primary btn-sm">
                                <strong style="font-size:20px;"><i class="fa fa-cloud"></i>&nbsp;<span class="page-info-data"><?php echo $client['client_name']?></span></strong> </button>
                          <?php }else{?>
                                <h4 class="page-title"><span class="page-info-data"><?php echo $client['client_name']?></span></h4>
                          <?php }?>       
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">الرئيسية</a></li>
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/clients');?>">العملاء </a></li>
                              <li class="breadcrumb-item active" aria-current="page"><?php echo $client['client_name']?></li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
          <div class="row">
              <div class="col-12">
                 <div class="card">
                    <div class="card-body">
                     <div class="row">    
                     <div class="col-lg-4">
                         <div class="el-card-item"> <a class="btn default btn-outline image-popup-vertical-fit el-link" href="<?php echo $image_link;?>"><img  style="width:90%;height:200px;" src="<?php echo $image_link;?>" alt="user"></a>
                              <div class="el-card-content centered">
                                  <h4 class="m-b-0 "><?php echo $client['client_name']?> Files</h4>
                              </div>
                              <div>
                                <div class="accordion" id="accordionExample">
                                  <div class="card m-b-0">
                                    <?php foreach($uploads as $upload){?>
                                      <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                          <a href="javascript: void(0);" data-toggle="collapse" data-target="#collapse<?php echo $upload['id']?>" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                              <i class="m-r-5 fas fa-image" aria-hidden="true"></i>
                                              <span><?php echo $upload['file_name']?></span>
                                          </a>
                                        </h5>
                                      </div>
                                      <div id="collapse<?php echo $upload['id']?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample" style="">
                                        <div class="card-body">
                                          <a class="btn default btn-outline image-popup-vertical-fit     el-link" href="<?php echo base_url('assets/uploads/clients/'.$upload['file_name']);?>"><img  style="width:150px;height:80px;" src="<?php echo $image_link;?>" alt="user"></a>
                                        </div>
                                      </div>
                                   <?php }?>
                                  </div>
                              </div>
                            </div>
                          </div>
                         </div> 
                         <div class="col-lg-8" style="margin-top: 50px;"> 
                             <div class="float-right"> 
                              <strong style="font-size:16px;"> إسم  العميل  :</strong><?php echo $client['client_name']?>
                             </div><br><br>
                             <div class="float-right">
                             <?php echo $client['phone']?> <strong style="font-size:16px;">: التليفون  </strong>
                             </div><br><br>
                             <div class="float-right"> 
                              <strong style="font-size:16px;">العنوان  :</strong><?php echo $client['address']?>
                             </div><br><br>
                             <div class="float-right"> 
                               <strong style="font-size:16px;">الشخص المتواصل معه  :</strong> <?php echo $client['contact']?> 
                             </div><br><br>
                             <div class="float-right"> 
                             <?php echo $client['cont_email']?> <strong style="font-size:16px;">: العنوان الإلكتروني  </strong>
                             </div><br><br>
                             <div class="float-right"> 
                               <?php if($client['cash']<0){?>
                                 <?php echo abs($client['cash'])?> <strong style="font-size:16px;" class="text-danger">: عليه مبلغ </strong>
                               <?php }else{?>
                                 <?php echo $client['cash']?> <strong style="font-size:16px;" class="text-danger">: له مبلغ </strong>
                               <?php }?>
                             </div>
                         </div>
                        </div>
                     </div>
                  </div>
                 <div class="card">
                    <div class="card-body">
                       <h4 class="page-title float-right">المعاملات  مع  <span style="color:red;"><?php echo $client['client_name']?></span></h4><br><br>
                          <table id="client_transactions-list-table" class="table table-hover" style="width:100% !important;">
                              <thead>
                                  <tr>
                                      <th class="tableCol text-right"><strong>المتبقى </strong></th>
                                      <th class="tableCol text-right"><strong>المدفوع </strong></th>
                                      <th class="tableCol text-right"><strong>الإجمالى </strong></th>
                                      <th class="tableCol text-right"><strong>نوع العملية </strong></th>
                                      <th class="tableCol text-right"><strong>تاريخ الشراء </strong></th>
                                      <th class="tableCol text-right"><strong>كود عملية الشراء </strong></th>
                                  </tr>
                              </thead>
                              <tbody>
                              </tbody>
                          </table>
                    </div>
                 </div>
              </div>
            
            </div>                 
      </div>

  <?php 
           initiate_modal(
                         'lg','تعديل  العميل ','admin/clients/client_process',
                          '<input id="supp_id" type="hidden" name="id" value="'.$client['id'].'">
                           <input  id="client_name" onkeyup="fieldChecker(\'client_name\',\'admin/clients/check_field/clients/client_name/'.$client['client_name'].'\')" type="text" name="client_name" class="form-control " placeholder="إسم العميل  " value="'.$client['client_name'].'" required><div class="invalid-feedback">Please change the name it is already exist</div>
                           <input id="phone" type="text" name="phone" class="form-control" placeholder="التليفون " value="'.$client['phone'].'" required><br>
                           <input id="address" type="text" name="address" class="form-control" placeholder="العنوان  " value="'.$client['address'].'" ><br>
                           <input id="contact" type="text" name="contact" class="form-control" placeholder="المتواصل معه  "
                           value="'.$client['contact'].'"><br>
                           <input id="cont_email" type="email" name="cont_email" class="form-control" placeholder="العنوان الإلكتروني  " value="'.$client['cont_email'].'"><br>
                            <input type="hidden" name="supp_id" value="'.$client['id'].'" />
                                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />         
                            <br>'
                         )
  ?>

<?php upfiles_js('admin/clients',$uploads,$this->data['module']['id'],$client['id'],'clients');?>
<script type="text/javascript">
   initDTable(
                 "client_transactions-list-table",
                    [
                     {"name":"payed"},
                     {"name":"payed"},
                     {"name":"total_cost"},
                     {"name":"status_name"},
                     {"name":"delivery_date"},
                     {"name":"serial"},],
                  "<?php echo base_url("admin/clients/client_transactions/".$client['id']) ?>"
                  ,'','5'
           );
</script>

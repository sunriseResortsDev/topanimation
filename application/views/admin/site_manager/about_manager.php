
    <div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                         <h4 class="page-title">About Manager</h4>      
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/site_manager');?>">Site Manager</a></li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
        <div class="row">
          <div class="col-lg-6">
            <?php  echo form_open(base_url('admin/site_manager/about_manager/about_us'), 'class="form-horizontal" enctype="multipart/form-data"');  ?> 
              <div class="card">
                <div class="card-header">
                  <h6>About Us</h6>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-12">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Header</label>
                      <input type="text" id="header" name="header" class="form-control" value="<?php if (isset($about)){echo $about['header'];}?>" required>
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Title</label>
                      <input type="text" id="title" name="title" class="form-control" value="<?php if (isset($about)){echo $about['title'];}?>" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Paragraph</label>
                      <textarea id="paragraph" name="paragraph" class="form-control" rows="6" required><?php if (isset($about)){echo $about['paragraph'];}?></textarea>
                    </div>
                  </div>
                 </div> 
                 <div class="card-footer">
                  <input type="submit" name="submit" class="btn btn-dark" value="Save">
                 </div>
              </div>
            <?php echo form_close( ); ?>
           </div>
           <div class="col-lg-6">
            <?php  echo form_open(base_url('admin/site_manager/about_manager/who_we_are'), 'class="form-horizontal" enctype="multipart/form-data"');  ?> 
              <div class="card">
                <div class="card-header">
                  <h6>Who we are</h6>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-12">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Header</label>
                      <input type="text" id="header" name="header" class="form-control" value="<?php if (isset($who_we)){echo $who_we['header'];}?>" required>
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Title</label>
                      <input type="text" id="title" name="title" class="form-control" value="<?php if (isset($who_we)){echo $who_we['title'];}?>" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Paragraph</label>
                      <textarea id="paragraph" name="paragraph" class="form-control" rows="6" required><?php if (isset($who_we)){echo $who_we['paragraph'];}?></textarea>
                    </div>
                  </div>
                 </div> 
                 <div class="card-footer">
                  <input type="submit" name="submit" class="btn btn-dark" value="Save">
                 </div>
              </div>
            <?php echo form_close( ); ?>
           </div> 
          </div>
           <div class="row">
          <div class="col-lg-6">
            <?php  echo form_open(base_url('admin/site_manager/about_manager/what_we_do'), 'class="form-horizontal" enctype="multipart/form-data"');  ?> 
              <div class="card">
                <div class="card-header">
                  <h6>What We Do</h6>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-12">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Header</label>
                      <input type="text" id="header" name="header" class="form-control" value="<?php if (isset($what_do)){echo $what_do['header'];}?>" required>
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Title</label>
                      <input type="text" id="title" name="title" class="form-control" value="<?php if (isset($what_do)){echo $what_do['title'];}?>" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Paragraph</label>
                      <textarea id="paragraph" name="paragraph" class="form-control" rows="6" required><?php if (isset($what_do)){echo $what_do['paragraph'];}?></textarea>
                    </div>
                  </div>
                 </div> 
                 <div class="card-footer">
                  <input type="submit" name="submit" class="btn btn-dark" value="Save">
                 </div>
              </div>
            <?php echo form_close( ); ?>
           </div>
           <div class="col-lg-6">
            <?php  echo form_open(base_url('admin/site_manager/about_manager/achievements'), 'class="form-horizontal" enctype="multipart/form-data"');  ?> 
              <div class="card">
                <div class="card-header">
                  <h6>Achievements</h6>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-12">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Header</label>
                      <input type="text" id="header" name="header" class="form-control" value="<?php if (isset($achievements)){echo $achievements['header'];}?>" required>
                    </div> 
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Title</label>
                      <input type="text" id="title" name="title" class="form-control" value="<?php if (isset($achievements)){echo $achievements['title'];}?>" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <label class="text-right control-label col-form-label" style="font-size:12px;">*Paragraph</label>
                      <textarea id="paragraph" name="paragraph" class="form-control" rows="6" required><?php if (isset($achievements)){echo $achievements['paragraph'];}?></textarea>
                    </div>
                  </div>
                 </div> 
                 <div class="card-footer">
                  <input type="submit" name="submit" class="btn btn-dark" value="Save">
                 </div>
              </div>
            <?php echo form_close( ); ?>
           </div> 
          </div>
      </div>
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
    <?php $this->load->view('admin/html_parts/loader_div');?>       
    <?php $this->load->view('admin/site_manager/site_manager.js.php');?> 
   
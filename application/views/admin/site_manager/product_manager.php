    <div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                         <h5 class="page-title"><?php echo $item['item_name']?>&nbsp;
                            <button id="add-new" data-toggle="modal" data-target="#smallmodal" class="btn btn-outline-cyan btn-sm">
                                <strong style="font-size:20px;"><i class="fa fa-cloud"></i>&nbsp;Edit</strong> 
                            </button>
                         </h5>      
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/site_manager/products_index/'.$category['id']);?>">Products</a></li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
          <div class="row">
              <div class="col-9">
                 <div class="card">
                    <div class="card-body">
                     <div> <strong style="font-size:14px;">Item Name: </strong><?php echo $item['item_name']?></div><br>
                     <div> <strong style="font-size:14px;">Group Name: </strong><?php echo $category['group_name']?></div><br>
                     <div> <strong style="font-size:14px;">Item Serial: </strong><?php echo $item['serial']?></div><br>
                     <div> <strong style="font-size:14px;">Description: </strong><?php echo $item['description']?></div><br>
                     <div> 
                        <strong style="font-size:14px;">File:</strong>
                        <a style="color:#212529" class="btn default btn-outline" href="<?php echo base_url('site_assets/img/products/product/'.$item['file']);?>" target="_heopenit"> 
                         <span><?php echo $item['file']?></span>&nbsp;<i style="color:#28b779" class="fas fa-file-powerpoint"></i>
                        </a>
                     </div>
                   </div>
                 </div>
              </div>
              <div class="col-lg-3">
                  <div class="card">
                      <div class="card-body">
                        <div class="row">
                           <div class="col-sm-12">
                             <div class="el-card-item"> <a class="btn default btn-outline image-popup-vertical-fit el-link" href="<?php echo base_url('site_assets/img/products/product/'.$item['img']);?>"><img  style="width:100%;height:200px;" src="<?php echo base_url('site_assets/img/products/product/'.$item['img']);?>" alt="user"></a>
                              </div>
                            </div>
                        </div>
                      </div>
                    </div>
                </div> 
            </div>                 
      </div>
  <?php initiate_modal(
                         'lg','Edit '.$item['item_name'].'','admin/site_manager/product_process',
                          '<input id="id" type="hidden" name="id" value="'.$item['id'].'">
                           <input id="group_id" type="hidden" name="group_id" value="'.$category['id'].'">
                           <input  id="itemName" onkeyup="fieldChecker(\'itemName\',\'admin/items/items/check_field/group_items/item_name/'.$item['item_name'].'\')" type="text" name="item_name" class="form-control" value="'.$item['item_name'].'" required><div class="invalid-feedback">Please change the name it is already exist</div>
                           <label for="category name" class="text-right control-label col-form-label" style="font-size:12px;">*Company</label>
                           <input type="text" name="company" class="form-control " placeholder="Company Name" value="'.$item['company'].'">
                           <label for="category name" class="text-right control-label col-form-label" style="font-size:12px;">*Description</label>
                           <textarea id="description" name="description" class="form-control" placeholder="Description" >'.$item['description'].'</textarea>
                           <label for="category name" class="text-right control-label col-form-label" style="font-size:12px;">*Rank</label>
                           <input type="text" name="rank" class="form-control " placeholder="Rank"  value="'.$item['rank'].'">
                           <label for="category name" class="text-right control-label col-form-label" style="font-size:12px;">*Image</label>
                           <input type="file" name="img" class="form-control" accept="image/*"/>
                           <input type="hidden" name="old_img" class="form-control" value="'.$item['img'].'"/>
                           <label for="category name" class="text-right control-label col-form-label" style="font-size:12px;">*File</label>
                           <input type="file" name="file" class="form-control"/>
                           <input type="hidden" name="old_file" class="form-control" value="'.$item['file'].'"/>
                            <br>'
                         )?>


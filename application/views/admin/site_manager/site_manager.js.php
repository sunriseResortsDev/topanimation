<script type="text/javascript">

	function removeRow(path,rowId,type){ 
      var x = confirm("Are you sure you want to delete this Item?");
        if (x == true){
           if (type == 'new'){
                 $('#Items-list-table #row'+rowId+'').remove(); 
             }else{
                 deleteItems(path,rowId,type);
             }
          }else{
           return false;
          }
       }


    function deleteItems(path,rowId,id){
       var url =getUrl()+path;
        $.ajax({
              url: url,
              dataType: "json",
              type : 'POST',
              data:{id:id},
              success: function(data){
                $('#Items-list-table #row'+rowId+'').remove(); 
              }
         });
     }

    function removeCategory(path,id){ 
      var x = confirm("Are you sure you want to delete this Item?");
        if (x == true){
                 deleteItems(path,1,id);
                 $('#categoryDiv'+id+'').remove(); 
          }else{
           return false;
          }
       }


    // function deleteItems(path,rowId,id){
    //    var url =getUrl()+path;
    //     $.ajax({
    //           url: url,
    //           dataType: "json",
    //           type : 'POST',
    //           data:{id:id},
    //           success: function(data){
    //             $('#Items-list-table #row'+rowId+'').remove(); 
    //           }
    //      });
    //  }

//-------------------------------------- slider manager ------------------------------
	function addSlider(){
	    let  allstoredIds   = $('#allstoredIds').val().split(',');
	    let  rowId          =  Math.max.apply(Math, allstoredIds)+1;
	    allstoredIds.push(rowId);
	    $('#allstoredIds').val(allstoredIds);
	    $('#Items-list-table').append('<tr class="rowIds" id="row'+rowId+'">  <td class="hidden">'+rowId+'</td>       <td><input type="text" name="items['+rowId+'][first_title]" placeholder="First Header" class="form-control" required/></td>  <td><input type="text" name="items['+rowId+'][second_title]" placeholder="Second Header" class="form-control" required/></td>  <td><input type="number" name="items['+rowId+'][rank]" placeholder="Rank" class="form-control" required/></td> <td class="text-center"></td>  <td><input type="file"  name="items-'+rowId+'-img" class="form-control" accept="image/*"  placeholder="Sample"/></td>  <td class="text-center"><button type="button" name="remove" onclick="removeRow(`admin/site_manager/delete_slider`,'+rowId+',`new`)"  class="btn btn-light btn-sm btn-circle btn_remove"><i class="fas fa-trash-alt text-danger"></i></button></td>   </tr>');
	   }

//-------------------------------------- about manager ------------------------------

	function addAbout_point(){
	    let  allstoredIds   = $('#allstoredIds').val().split(',');
	    let  rowId          =  Math.max.apply(Math, allstoredIds)+1;
	    allstoredIds.push(rowId);
	    $('#allstoredIds').val(allstoredIds);
	    $('#Items-list-table').append('<tr class="rowIds" id="row'+rowId+'">  <td class="hidden">'+rowId+'</td>       <td><input type="text" name="items['+rowId+'][point]" placeholder="Point" class="form-control" required/></td> <td><input type="number" name="items['+rowId+'][rank]" placeholder="Rank" class="form-control" required/></td>   <td class="text-center"><button type="button" name="remove" onclick="removeRow(`admin/site_manager/`,'+rowId+',`new`)"  class="btn btn-light btn-sm btn-circle btn_remove"><i class="fas fa-trash-alt text-danger"></i></button></td>   </tr>');
	   }

//-------------------------------------- service manager ------------------------------
    <?php 
      $options = '<option value=""></option>';
      if (isset($font_icons)) {
              foreach ($font_icons as $icon) {
                  $options .= ' <option value="fa '.$icon['icon_name'].'"  data-subtext="<i class="fa '.$icon['icon_name'].'" style="font-size:22px;"></i>">fa '.$icon['icon_name'].'</option>';
              }
        }
   ?>
  function addService(){
      let  allstoredIds   = $('#allstoredIds').val().split(',');
      let  rowId          =  Math.max.apply(Math, allstoredIds)+1;
      let  options        = '<?php echo $options?>';
      allstoredIds.push(rowId);
      $('#allstoredIds').val(allstoredIds);
      $('#Items-list-table').append('<tr class="rowIds" id="row'+rowId+'">  <td class="hidden">'+rowId+'</td>       <td class="hidden"><input type="hidden" name="items['+rowId+'][header]"/><input type="hidden" name="items['+rowId+'][meta]" value="services"/></td>  <td><input type="text" name="items['+rowId+'][title]" placeholder="Title" class="form-control" required/></td>  <td><textarea name="items['+rowId+'][paragraph]" class="form-control" required></textarea></td>  <td><select name="items['+rowId+'][img]" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Icon" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>'+options+'</select> </td>  <td class="text-center"><button type="button" name="remove" onclick="removeRow(`admin/site_manager/delete_service`,'+rowId+',`new`)"  class="btn btn-light btn-sm btn-circle btn_remove"><i class="fas fa-trash-alt text-danger"></i></button></td>   </tr>');
       $(document).ready(function(){
        $('.selectpicker').selectpicker('render');
        $('.selectpicker').selectpicker('setStyle','btn btn-dark');
       });
     }

//-------------------------------------- offers manager ------------------------------
  <?php 
    $options = '<option value=""></option>';
    if (isset($activity)) {
      foreach ($activity as $active) {
        $options .= '<option value="'.$active['id'].'">'.$active['name'].'</option>';
      }
    }
  ?>
  <?php 
    $options1 = '<option value=""></option>';
    if (isset($hotels)) {
      foreach ($hotels as $hotel) {
        $options1 .= '<option value="'.$hotel['id'].'">'.$hotel['name'].'</option>';
      }
    }
  ?>
  <?php 
    $options2 = '<option value=""></option>';
    if (isset($sub_activity)) {
      foreach ($sub_activity as $sub) {
        $options2 .= '<option value="'.$sub['id'].'">'.$sub['name'].'</option>';
      }
    }
  ?>
  function addService(){
      let  allstoredIds   = $('#allstoredIds').val().split(',');
      let  rowId          =  Math.max.apply(Math, allstoredIds)+1;
      let  options        = '<?php echo $options?>';
      let  options1       = '<?php echo $options1?>';
      let  options2       = '<?php echo $options2?>';
      allstoredIds.push(rowId);
      $('#allstoredIds').val(allstoredIds);
      $('#Items-list-table').append('<tr class="rowIds" id="row'+rowId+'">     <td class="hidden">'+rowId+'</td>     <td class="hidden">  </td>     <td><select name="items['+rowId+'][activity_id]" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Activity" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>'+options+'</select></td>     <td><select name="items['+rowId+'][hotel_id]" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>'+options1+'</select></td>     <td><select name="items['+rowId+'][sub_id]" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Sub Activity" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>'+options2+'</select></td>     <td><input type="text" name="items['+rowId+'][name]" placeholder="Name" class="form-control" required/></td>     <td><textarea name="items['+rowId+'][data]" class="form-control" required></textarea></td>     <td><input type="file" name="items-'+rowId+'-image" placeholder="Image" class="form-control"/></td>     <td><input type="number" name="items['+rowId+'][price]" placeholder="Price" class="form-control"/></td>     <td><select name="items['+rowId+'][currency]" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Currency" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required><option value=""></option><option value="EGP">EGP</option><option value="$">$</option><option value="Euro">Euro</option></select></td>     <td><select name="items['+rowId+'][special]" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Special" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required><option value=""></option><option value="1">Special Offer</option><option value="0">Normal Offer</option></select></td>     <td class="text-center"><button type="button" name="remove" onclick="removeRow(`admin/site_manager/delete_offer`,'+rowId+',`new`)"  class="btn btn-light btn-sm btn-circle btn_remove"><i class="fas fa-trash-alt text-danger"></i></button></td>     </tr>');
       $(document).ready(function(){
        $('.selectpicker').selectpicker('render');
        $('.selectpicker').selectpicker('setStyle','btn btn-dark');
       });
     }

//-------------------------------------- slider manager ------------------------------
	function addPortfolio(){
	    let  allstoredIds   = $('#allstoredIds').val().split(',');
	    let  rowId          =  Math.max.apply(Math, allstoredIds)+1;
	    allstoredIds.push(rowId);
	    $('#allstoredIds').val(allstoredIds);
	    $('#Items-list-table').append('<tr class="rowIds" id="row'+rowId+'">  <td class="hidden">'+rowId+'</td>       <td><input type="hidden" name="items['+rowId+'][meta]" value="portfolio"/><input type="text" name="items['+rowId+'][header]" placeholder="Acheivement" class="form-control" required/></td>  <td><input type="number" name="items['+rowId+'][title]" placeholder="Reviews" class="form-control" required/></td> <td> <textarea  name="items['+rowId+'][paragraph]" class="form-control" rows="3" required></textarea></td>  <td><input type="number" name="items['+rowId+'][rank]" placeholder="Rank" class="form-control" required/></td> <td class="text-center"></td>  <td><input type="file"  name="items-'+rowId+'-img" class="form-control" accept="image/*"  placeholder="Sample"/></td>  <td class="text-center"><button type="button" name="remove" onclick="removeRow(`admin/site_manager/delete_portfolio`,'+rowId+',`new`)"  class="btn btn-light btn-sm btn-circle btn_remove"><i class="fas fa-trash-alt text-danger"></i></button></td>   </tr>');
	   }


  function addGallery(){
      let  allstoredIds   = $('#allstoredIds').val().split(',');
      let  rowId          =  Math.max.apply(Math, allstoredIds)+1;
      allstoredIds.push(rowId);
      $('#allstoredIds').val(allstoredIds);
      $('#Items-list-table').append('<tr class="rowIds" id="row'+rowId+'">  <td class="hidden">'+rowId+'</td>       <td><input type="hidden" name="items['+rowId+'][meta]" value="gallery"/><input type="text" name="items['+rowId+'][header]" placeholder="Header" class="form-control" required/></td>  <td><input type="text" name="items['+rowId+'][title]" placeholder="Title" class="form-control" required/></td> <td><input type="text" name="items['+rowId+'][points]" placeholder="Link" class="form-control" required/></td>  <td><input type="number" name="items['+rowId+'][rank]" placeholder="Rank" class="form-control" required/></td> <td class="text-center"></td>  <td><input type="file"  name="items-'+rowId+'-img" class="form-control" accept="image/*"  placeholder="Sample"/></td>  <td class="text-center"><button type="button" name="remove" onclick="removeRow(`admin/site_manager/delete_gallery`,'+rowId+',`new`)"  class="btn btn-light btn-sm btn-circle btn_remove"><i class="fas fa-trash-alt text-danger"></i></button></td>   </tr>');
     }

 //-------------------------------------- team manager ------------------------------
  function addTeam(){
      let  allstoredIds   = $('#allstoredIds').val().split(',');
      let  rowId          =  Math.max.apply(Math, allstoredIds)+1;
      allstoredIds.push(rowId);
      $('#allstoredIds').val(allstoredIds);
      $('#Items-list-table').append('<tr class="rowIds" id="row'+rowId+'">  <td class="hidden">'+rowId+'</td>       <td><input type="hidden" name="items['+rowId+'][meta]" value="team"/><input type="text" name="items['+rowId+'][header]" placeholder="Employee Name" class="form-control" required/></td>  <td><input type="text" name="items['+rowId+'][title]" placeholder="Title" class="form-control" required/></td> <td><input type="number" name="items['+rowId+'][rank]" placeholder="Rank" class="form-control" required/></td> <td class="text-center"></td>  <td><input type="file"  name="items-'+rowId+'-img" class="form-control" accept="image/*"  placeholder="Sample"/></td>  <td class="text-center"><button type="button" name="remove" onclick="removeRow(`admin/site_manager/delete_team`,'+rowId+',`new`)"  class="btn btn-light btn-sm btn-circle btn_remove"><i class="fas fa-trash-alt text-danger"></i></button></td>   </tr>');
     }



</script>
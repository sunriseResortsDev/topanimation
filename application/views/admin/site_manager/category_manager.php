   <div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                         <h5 class="page-title">Category Manager
                            <button id="add-new" data-toggle="modal" data-target="#smallmodal" class="btn btn-outline-cyan btn-sm">
                                <strong style="font-size:20px;"><i class="fa fa-cloud"></i>&nbsp;Add New</strong> 
                            </button>
                         </h5>      
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/site_manager');?>">Site Manager</a></li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
       <div class="row el-element-overlay">
            <?php foreach($categories as $category){?>
                <div class="col-lg-3 col-md-6" id="categoryDiv<?php echo $category['id']?>">
                    <div class="card">
                        <div class="el-card-item">
                            <div class="el-card-avatar el-overlay-1"> <img style="height:250px;" src="<?php if($category['img']!=null){echo base_url('site_assets/img/products/'.$category['img']);}elseif($category['img']==null){echo base_url('site_assets/img/auth-bg.jpg');}?>" alt="user">
                                <div class="el-overlay">
                                    <ul class="list-style-none el-info">
                                        <li class="el-item"><a class="btn default btn-outline el-link" data-toggle="modal" data-target="#smallmodal" href="javascript: void(0);" onclick="editCategory('<?php echo $category['id']?>')"><i class="fas fa-pencil-alt"></i></a></li>
                                        <li class="el-item"><a class="btn default btn-outline el-link" href="<?php echo base_url('admin/site_manager/products_index/'.$category['id']);?>"><i class="fas fa-expand-arrows-alt"></i></a>
                                        <li class="el-item"><a class="btn default btn-outline el-link" onclick="removeCategory('admin/site_manager/delete_category','<?php echo $category['id']?>')"><i class="fas fa-trash-alt"></i></a>
                                    </ul>
                                </div>
                            </div>
                            <div class="el-card-content">
                                <h6 class="m-b-0" id="group_name<?php echo $category['id']?>"><?php echo $category['group_name']?></h6> <span class="text-muted"><?php echo $category['serial']?></span>
                                <span class="hidden" id="rank<?php echo $category['id']?>"><?php echo $category['rank']?></span>
                                <span class="hidden" id="img<?php echo $category['id']?>"><?php echo $category['img']?></span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }?>    
            </div>
        </div>
        <?php initiate_modal(
                         'lg','Category manager','admin/site_manager/category_manager',
                          '<input id="cat_id" type="hidden" name="id">
                           <label for="category name" class="text-right control-label col-form-label" style="font-size:12px;">*Category Name</label>
                           <input  id="group_name" type="text" name="group_name" class="form-control" placeholder="Catgeory Name" required>
                           <label for="hotel" class="text-right control-label col-form-label" style="font-size:12px;">*Rank</label>
                           <input id="rank" type="number" name="rank" class="form-control" placeholder="Rank" required>
                           <label for="Image" class="text-right control-label col-form-label" style="font-size:12px;">*Image</label> 
                           <input type="file"  name="img" class="form-control" accept="image/*"/>
                           <input type="hidden" id="img" name="old_img">        
                           <br>'
                         )?>
    <?php $this->load->view('admin/html_parts/loader_div');?>       
    <?php $this->load->view('admin/site_manager/site_manager.js.php');?> 
    <script type="text/javascript">
        function editCategory(cat_id){
          var group_name = $("#group_name"+cat_id+"").text();
          var rank       = $("#rank"+cat_id+"").text();
          var img        = $("#img"+cat_id+"").text();
             $(".modal-body #cat_id").val( cat_id );
             $(".modal-body #group_name").val( group_name );
             $(".modal-body #rank").val( rank );
             $(".modal-body #img").val( img );
             $('.modal-header .modal-title').empty();
             $('.modal-header .modal-title').text('Edit ' +group_name+'');

        }
        $(document).ready(function(){
          $("#add-new").click(function() {
               $(".modal-body #cat_id").val('');
               $(".modal-body #group_name").val('');
               $(".modal-body #rank").val('');
               $(".modal-body #img").val('');
          });

    });
    </script>
   
  <main class="page-content">
    <section style="background-image: url(<?php echo base_url('site_assets/images/dived/1298266-top-freediving-wallpaper-1920x1080-for-ipad-pro.jpg');?>);" class="section-30 section-sm-40 section-md-66 section-lg-bottom-90 bg-gray-dark page-title-wrap">
      <div class="shell">
        <div class="page-title">
          <h2></h2>
        </div>
      </div>
    </section>
    <section class="section-40 section-sm-bottom-100 section-lg-bottom-165">
        <div class="shell">
          <h3><?php echo translate($about['header'], $this->data['language'])?></h3>
          <div class="range range-md-justify offset-top-30 offset-sm-top-40">
            <div class="cell-sm-6">
              <figure><img src="<?php echo base_url('site_assets/images/about/'.$about['img'])?>" alt="" width="570" height="386"/> </figure>
            </div>
            <div class="cell-sm-6 offset-top-30 offset-sm-top-0">
              <div class="inset-md-left-40 inset-lg-left-70 text-gray-darker">
                <p><?php echo translate($about['paragraph'], $this->data['language'])?></p>
                <ul>
                <?php foreach($points as $row){?>
                  <li>
                    <i class="fa fa-check text-primary"></i> <?php echo translate($row['point'], $this->data['language'])?>
                  </li>
                <?php }?>
              </ul>
              </div>
            </div>
          </div>
        </div>
    </section>
  </main>

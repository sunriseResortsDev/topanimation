  <main class="page-content">
    <section style="background-image: url(<?php echo base_url('site_assets/images/dived/1298266-top-freediving-wallpaper-1920x1080-for-ipad-pro.jpg');?>);" class="section-30 section-sm-40 section-md-66 section-lg-bottom-90 bg-gray-dark page-title-wrap">
      <div class="shell">
        <div class="page-title">
          <h2></h2>
        </div>
      </div>
    </section>
    <section class="section-30 section-sm-bottom-100">
      <div class="shell">
        <div class="range">
          <div class="cell-md-9 cell-lg-7">
            <h3> <?php echo translate('Contact Us', $this->data['language']) ?> </h3>
            <?php  echo form_open('', 'class="rd-mailform form-modern offset-top-22" id=""contact-us-all-form"'); ?>
              <div class="range range-7">
                <div class="cell-sm-3">
                  <div class="form-group">
                    <input id="feedback-name" type="text" name="client_name" data-constraints="@Required" class="form-control contact-us-form" required="required">
                    <label for="feedback-name" class="form-label"> <?php echo translate('Your Name', $this->data['language']) ?></label>
                  </div>
                </div>
                <div class="cell-sm-3">
                  <div class="form-group">
                    <input id="feedback-email" type="email" name="email" data-constraints="@Email @Required" class="form-control contact-us-form" required>
                    <label for="feedback-email" class="form-label"> <?php echo translate('Your Email', $this->data['language']) ?></label>
                  </div>
                </div>
                <div class="cell-sm-6 offset-top-30">
                  <div class="form-group">
                    <input id="feedback-subject" type="text" name="subject" data-constraints="@Required" class="form-control contact-us-form" required>
                    <label for="feedback-subject" class="form-label"> <?php echo translate('Subject', $this->data['language']) ?></label>
                  </div>
                </div>
                <div class="cell-xs-7 offset-top-30">
                  <div class="form-group">
                    <div class="textarea-lined-wrap">
                      <textarea id="feedback-message" name="message" data-constraints="@Required" class="form-control contact-us-form"></textarea>
                      <label for="feedback-message" class="form-label"> <?php echo translate('Message', $this->data['language']) ?></label>
                    </div>
                  </div>
                </div>
                <div class="cell-xs-5 offset-top-30 offset-xs-top-30 offset-sm-top-50">
                  <input type="submit" onclick="processFormData()" class="btn btn-primary btn-block" id="submit-button-contact_us" value="Send">
                </div>
                <div class="cell-xs-2 offset-top-22 offset-xs-top-30 offset-sm-top-50">
                  <button type="reset" class="btn btn-silver-outline btn-block"> <?php echo translate('Reset', $this->data['language']) ?></button>
                </div>
              </div>
           <?php echo form_close(); ?> 
          </div>
          <div class="cell-xs-12" style="padding-top:20px;">
            <iframe  src="https://www.google.com/maps/embed/v1/place?q=10th%20of%20Rammadan%20Desert%2C%20Egypt%2C&key=AIzaSyA8sKMHDLlwPv5S33kOy7E9iCIv5Jcv8YA" width="100%" height="380" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </section>
  </main>
  <div class="modal fade" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;padding-top:15% !important" 
       aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document ">
        <div class="modal-content">
            <div class="modal-body">
              <h5 class="text-center" id="loading-part">
                <div class="" id="process-loader">
                   <img src="<?php echo base_url();?>assets/images/spinner.gif"
                       style = " width: 30%;height:30%;">
                </div>
                 <?php echo translate('Please Wait', $this->data['language']) ?>
              </h5>
              <div id="success-part">
                <h1 class="text-center">
                  <strong style="font-size:100px;color:#3EC1D5;margin-left:5% !important"><i class="fa fa-check-circle"></i></strong>
                </h1>
                <h6 class="text-center"> <?php echo translate('Success', $this->data['language']) ?></h6>
              </div>
            </div>
        </div>
    </div>
  </div>
   <script type="text/javascript">
     function processFormData(){
      if ( !$('#feedback-name').val() ||  !$('#feedback-email').val() ||  !$('#feedback-subject').val() ||  !$('#feedback-message').val()) {
            return false;
      }else{
            $('#success-modal').modal('show');
            $('#success-modal .modal-body #success-part').hide();
            $('#success-modal .modal-body #loading-part').show();
             var formdata = $('form').serialize();
             console.log(formdata);
             $.ajax({
               type: 'post',
               url: '<?php echo base_url("clients/home/contact_us") ?>',
               data: {formdata:formdata},
               success: function(data) { 
                 setTimeout(function(){
                  $('#success-modal .modal-body #loading-part').hide();
                  $('#success-modal .modal-body #success-part').show();
                  document.getElementById("contact-us-all-form").reset();
                 // $('.contact-us-form').val('');
                  }, 1000);
                   
                   }
              });
      }
    }

   //   $(function () {
   //    $('#contact-us-all-form').on('submit', function (e) {
   //       e.preventDefault();         
   //      if ( !$('#feedback-name').val() ||  !$('#feedback-email').val() ||  !$('#feedback-subject').val() ||  !$('#feedback-message').val()) {
   //        return false;
   //      }
   //      $('#success-modal').modal('show');
   //      $('#success-modal .modal-body #success-part').hide();
   //      $('#success-modal .modal-body #loading-part').show();
   //       var formdata = $('form').serialize();
   //       $.ajax({
   //         type: 'post',
   //         url: '<?php //echo base_url("clients/home/contact_us") ?>',
   //         data: {formdata:formdata},
   //         success: function(data) { 
   //           setTimeout(function(){
   //            $('#success-modal .modal-body #loading-part').hide();
   //            $('#success-modal .modal-body #success-part').show();
   //            document.getElementById("contact-us-all-form").reset();
   //           // $('.contact-us-form').val('');
   //            }, 1000);
               
   //             }
   //        });
   //    });
   // });
  </script>

<!-- Portfolio start -->
<section id="portfolio" class="portfolio portfolio-box">
  <div class="container">
    <div class="row">
      <div class="col-md-12 heading text-center">
        <span class="icon-pentagon wow bounceIn"><i class="fa fa-camera"></i></span>
        <h2 class="title2"><?php echo translate('Gallery',$this->data['language'])?>
          <span class="title-desc"><?php echo translate('Photos Of Our Latest events',$this->data['language'])?></span>
        </h2>
      </div>
    </div> <!-- Title row end -->

    <!--Isotope filter start -->
    <div class="row text-center">
      <div class="isotope-nav" data-isotope-nav="isotope">
        <ul>
          <li><a href="#" class="active" data-filter="*"><?php echo translate('All',$this->data['language'])?></a></li>
          <?php foreach($points as $point){?>
            <li>
              <a href="#" data-filter=".<?php echo $point['points']?>">
                <?php echo ucfirst( str_replace('_', ' ',translate($point['points'],$this->data['language'])))?>
              </a>
            </li>
          <?php }?>
        </ul>
      </div>
    </div><!-- Isotope filter end -->

    <div class="row">
      <div id="isotope" class="isotope">
        <!-- gallery images  -->
        <?php foreach($gallerys as $row){?>
          <div class="col-sm-3 <?php echo $row['points']?> isotope-item">
            <div class="grid">
              <figure class="effect-oscar">
                <img src="<?php echo base_url('site_assets/images/gallery/'.$row['img'])?>" style="height:230px;" alt="">
                <figcaption>
                  <h3><?php echo translate($row['header'],$this->data['language'])?></h3>
                  <a class="view icon-pentagon" data-rel="prettyPhoto" href="<?php echo base_url('site_assets/images/gallery/'.$row['img'])?>"><i class="fa fa-search"></i></a>            
                </figcaption>     
              </figure>
            </div>
          </div><!-- Isotope item end -->
        <?php }?> 
        <!--  -->
      </div><!-- Isotope content end -->
    </div><!-- Content row end -->
  </div><!-- Container end -->
</section><!-- Portfolio end -->


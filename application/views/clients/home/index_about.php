<section id="about" class="about angle">
  <div class="container">
    <div class="row">
      <div class="landing-tab clearfix">
        <ul class="nav nav-tabs nav-stacked col-md-3 col-sm-5">
          <li class="active">
            <a class="animated fadeIn" href="#tab_a" data-toggle="tab">
              <span class="tab-icon"><i class="fa fa-info"></i></span>
              <div class="tab-info">
                <h3><?php echo translate($about['header'], $this->data['language'])?></h3>
              </div>
            </a>
          </li>
          <li>
            <a class="animated fadeIn" href="#tab_b" data-toggle="tab">
              <span class="tab-icon"><i class="fa fa-briefcase"></i></span>
              <div class="tab-info">
                <h3><?php echo translate($who_we['header'], $this->data['language'])?></h3>
              </div>
            </a>
          </li>
          <li>
            <a class="animated fadeIn" href="#tab_c" data-toggle="tab">
              <span class="tab-icon"><i class="fa fa-support"></i></span>
              <div class="tab-info">
                 <h3><?php echo translate($what_do['header'], $this->data['language'])?></h3>
              </div>
            </a>
          </li>
          <li>
            <a class="animated fadeIn" href="#tab_d" data-toggle="tab">
              <span class="tab-icon"><i class="fa fa-pagelines"></i></span>
              <div class="tab-info">
                <h3><?php echo translate($achievements['header'], $this->data['language'])?></h3>
              </div>
            </a>
          </li>
        </ul>
        <div class="tab-content col-md-9 col-sm-7">
          <div class="tab-pane active animated fadeInRight" id="tab_a">
            <i class="fa fa-pagelines big"></i>
            <h3><?php echo translate($about['title'], $this->data['language'])?></h3> 
            <p><?php echo translate($about['paragraph'], $this->data['language'])?></p>
          </div>
          <div class="tab-pane animated fadeInLeft" id="tab_b">
            <i class="fa fa-briefcase big"></i>
            <h3><?php echo translate($who_we['title'], $this->data['language'])?></h3> 
            <p><?php echo translate($who_we['paragraph'], $this->data['language'])?></p>              
          </div>
          <div class="tab-pane animated fadeIn" id="tab_c">
            <i class="fa fa-support big"></i>
            <h3><?php echo translate($what_do['title'], $this->data['language'])?></h3> 
            <p><?php echo translate($what_do['paragraph'], $this->data['language'])?></p>      
          </div>
          <div class="tab-pane animated fadeIn" id="tab_d">
            <i class="fa fa-trophy big"></i>
            <h3><?php echo translate($achievements['title'], $this->data['language'])?></h3> 
            <p><?php echo translate($achievements['paragraph'], $this->data['language'])?></p>
          </div>
        </div><!-- tab content -->
      </div><!-- Overview tab end -->
    </div><!--/ Content row end -->
  </div><!-- Container end -->
</section>

<!-- more about -->

<section id="image-block" class="image-block no-padding">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 ts-padding" style="height:650px;background:url(<?php echo base_url('site_assets/images/image-block-bg.jpg')?>) 50% 50% / cover no-repeat;">
      </div>
      <div class="col-md-6 ts-padding img-block-right">
        <div class="img-block-head text-center">
          <h2>Know More About Our Company</h2>
          <h3>Why Choose Us</h3>
          <p>Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Proin gravida nibh vel velit auctor Aenean sollicitudin, adipisicing elit sed lorem quis bibendum auctor.
          </p>
        </div>

        <div class="gap-30"></div>

        <div class="image-block-content">
          <span class="feature-icon pull-left" ><i class="fa fa-bicycle"></i></span>
          <div class="feature-content">
            <h3>Tons of Features</h3>
            <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut</p>
          </div>
        </div><!--/ End 1st block -->

        <div class="image-block-content">
          <span class="feature-icon pull-left" ><i class="fa fa-diamond"></i></span>
          <div class="feature-content">
            <h3>PowerPack Theme</h3>
            <p>Proin gravida nibh vel velit auctor Aenean sollicitudin adipisicing</p>
          </div>
        </div><!--/ End 1st block -->

        <div class="image-block-content">
          <span class="feature-icon pull-left" ><i class="fa fa-street-view"></i></span>
          <div class="feature-content">
            <h3>Day Night Support</h3>
            <p>Simply dummy text and typesettings industry has been the industry</p>
          </div>
        </div><!--/ End 1st block -->


      </div>
    </div>
  </div>
</section>

<!-- Counter Strat -->
<section class="ts_counter no-padding">
  <div class="container-fluid">
    <div class="row facts-wrapper wow fadeInLeft text-center">
      <div class="facts one col-md-3 col-sm-6">
        <span class="facts-icon"><i class="fa fa-user"></i></span>
        <div class="facts-num">
          <span class="counter">1200</span>
        </div>
        <h3>Clients</h3> 
      </div>

      <div class="facts two col-md-3 col-sm-6">
        <span class="facts-icon"><i class="fa fa-institution"></i></span>
        <div class="facts-num">
          <span class="counter">1277</span>
        </div>
        <h3>Item Sold</h3> 
      </div>

      <div class="facts three col-md-3 col-sm-6">
        <span class="facts-icon"><i class="fa fa-suitcase"></i></span>
        <div class="facts-num">
          <span class="counter">869</span>
        </div>
        <h3>Projects</h3> 
      </div>

      <div class="facts four col-md-3 col-sm-6">
        <span class="facts-icon"><i class="fa fa-trophy"></i></span>
        <div class="facts-num">
          <span class="counter">76</span>
        </div>
        <h3>Awwards</h3> 
      </div>

    </div>
  </div><!--/ Container end -->
</section><!--/ Counter end -->


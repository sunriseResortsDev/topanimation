
<!-- start slider -->
<?php $this->load->view('clients/home/slider');?>
<!-- end slider -->

<!-- About tab start -->
<div id="about"></div>  
<!-- About end -->

<!-- gallery tab start -->
<div id="gallery"></div>  
<!-- gallery end -->

<!-- services tab start -->
<div id="services"></div>  
<!-- services end -->

<!-- services tab start -->
<div id="team"></div>  
<!-- services end -->

<!-- Testimonial start-->
<section class="testimonial parallax parallax3">
  <div class="parallax-overlay"></div>
  <div class="container">
    <div class="row">
      <div id="testimonial-carousel" class="owl-carousel owl-theme text-center testimonial-slide">
        <div class="item">
          <div class="testimonial-thumb">
            <img src="<?php echo base_url('site_assets/images/team/testimonial1.jpg')?>" alt="testimonial">
          </div>
          <div class="testimonial-content">
            <p class="testimonial-text">
              Lorem Ipsum as their default model text, and a search for ‘lorem ipsum’ will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose. Lorem Ipsum is that it as opposed to using.
            </p>
            <h3 class="name">Sarah Arevik<span>Chief Executive</span></h3>
          </div>
        </div>
        <div class="item">
          <div class="testimonial-thumb">
            <img src="<?php echo base_url('site_assets/images/team/testimonial2.jpg')?>" alt="testimonial">
          </div>
          <div class="testimonial-content">
            <p class="testimonial-text">
              Lorem Ipsum as their default model text, and a search for ‘lorem ipsum’ will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose. Lorem Ipsum is that it as opposed to using.
            </p>
            <h3 class="name">Narek Bedros<span>Sr. Manager</span></h3>
          </div>
        </div>
        <div class="item">
          <div class="testimonial-thumb">
            <img src="<?php echo base_url('site_assets/images/team/testimonial3.jpg')?>" alt="testimonial">
          </div>
          <div class="testimonial-content">
            <p class="testimonial-text">
              Lorem Ipsum as their default model text, and a search for ‘lorem ipsum’ will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose. Lorem Ipsum is that it as opposed to using.
            </p>
            <h3 class="name">Taline Lucine<span>Sales Manager</span></h3>
          </div>
        </div>
      </div><!--/ Testimonial carousel end-->
    </div><!--/ Row end-->
  </div><!--/  Container end-->
</section><!--/ Testimonial end-->


<!-- Newsletter start -->
<section id="newsletter" class="newsletter">
  <div class="container">
    <div class="row">
      <div class="col-md-12 heading text-center">
        <span class="icon-pentagon wow bounceIn animated"><i class="fa fa-envelope"></i></span>
        <h2 class="title2">Subscribe With Us
          <span class="title-desc">We Love to Work with Passion</span>
        </h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <form action="#" method="post" id="newsletter-form" class="newsletter-form wow bounceIn" data-wow-duration=".8s">
          <div class="form-group">
            <input type="email" name="email" id="newsletter-form-email" class="form-control form-control-lg" placeholder="Enter your email address" autocomplete="off">
            <button class="btn btn-primary solid">Subscribe</button>
          </div>
        </form>
      </div>
    </div><!--/ Content row end -->
  </div><!--/ Container end -->
</section><!-- Newsletter end -->
<script type="text/javascript">
  getViewAjax('clients/home','index_about','','about');
  getViewAjax('clients/home','index_gallery','','gallery');
  getViewAjax('clients/home','index_services','','services');
  getViewAjax('clients/home','index_team','','team');
</script>
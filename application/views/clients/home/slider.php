
  <!-- Slider start -->
  <section id="home" class="no-padding">  
    <div id="main-slide" class="ts-flex-slider">
      <div class="flexSlideshow flexslider">
        <ul class="slides">
          <?php foreach($sliders as $slider){?>
            <li>
              <div class="overlay2">
                <img class="" src="<?php echo base_url('site_assets/images/slider/'.$slider['img'])?>" alt="slider">
              </div>
              <div class="flex-caption slider-content">
                  <div class="col-md-12 text-center">
                    <h2 class="animated2"><?php echo translate($slider['first_title'], $this->data['language'])?></h2>
                    <h3 class="animated3"><?php echo translate($slider['second_title'], $this->data['language']) ?></h3>
                  </div>
              </div>
            </li>
          <?php }?>  
        </ul>
      </div>
    </div><!--/ Main slider end -->     
  </section> <!--/ Slider end -->
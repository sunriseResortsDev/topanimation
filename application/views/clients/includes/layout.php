<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  ================================================== -->
  <meta charset="utf-8">
    <title>Top Animation Club</title>
    <meta name="description" content="">  
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  ================================================== -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- Favicons
  ================================================== -->
  <link rel="icon" href="img/favicon/favicon-32x32.png" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon/favicon-144x144.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon/favicon-72x72.png">
  <link rel="apple-touch-icon-precomposed" href="img/favicon/favicon-54x54.png">
  
  <!-- CSS
  ================================================== -->
  
  <!-- Bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url('site_assets/css/bootstrap.min.css');?>">
  <!-- Template styles-->
  <link rel="stylesheet" href="<?php echo base_url('site_assets/css/style.css');?>">
  <!-- Responsive styles-->
  <link rel="stylesheet" href="<?php echo base_url('site_assets/css/responsive.css');?>">
  <!-- FontAwesome -->
  <link rel="stylesheet" href="<?php echo base_url('site_assets/css/font-awesome.min.css');?>">
  <!-- Animation -->
  <link rel="stylesheet" href="<?php echo base_url('site_assets/css/animate.css');?>">
  <!-- Prettyphoto -->
  <link rel="stylesheet" href="<?php echo base_url('site_assets/css/prettyPhoto.css');?>">
  <!-- Owl Carousel -->
  <link rel="stylesheet" href="<?php echo base_url('site_assets/css/owl.carousel.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('site_assets/css/owl.theme.css');?>">
  <!-- Flexslider -->
  <link rel="stylesheet" href="<?php echo base_url('site_assets/css/flexslider.css');?>">
  <!-- Flexslider -->
  <link rel="stylesheet" href="<?php echo base_url('site_assets/css/cd-hero.css');?>">
  <!-- Style Swicther -->
  <link id="style-switch" href="<?php echo base_url('site_assets/css/presets/preset1.css');?>" media="screen" rel="stylesheet" type="text/css">
  <!-- initialize jQuery Library -->
  <script type="text/javascript" src="<?php echo base_url('site_assets/js/jquery.js');?>"></script>
  <!-- site custome js scripts -->
  <script type="text/javascript" src="<?php echo base_url('site_assets/js/main_scripts.js');?>"></script>

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<div class="page">
    <!-- start header container -->
      <input type="hidden" id="baseurl" name="baseurl" value="<?php echo base_url(); ?>" />
    <?php $this->load->view($navbar);?>
    <!-- end header container -->
            
    <?php $this->load->view($view);?>
           
          
    <!-- Start Footer bottom Area -->
      <?php 
          $footer_about          = $this->Home_model->get_sit_metaData('footer_about','single');
          $footer_info           = $this->Home_model->get_sit_metaData('footer_info','single');
          $footer_tel            = $this->Home_model->get_sit_metaData('footer_tel','single');
          $footer_email          = $this->Home_model->get_sit_metaData('footer_email','single');
      ?>
    
<!-- Footer start -->
  <footer id="footer" class="footer footer2">
    <div class="container">

      <div class="row">

        <div class="col-md-4 col-sm-12 footer-widget">
          <h3 class="widget-title">Flickr Photos</h3>

          <div class="img-gallery">
            <div class="img-container">
              <a class="thumb-holder" data-rel="prettyPhoto" href="<?php echo base_url('site_assets/images/gallery/1.jpg');?>">
                <img src="<?php echo base_url('site_assets/images/gallery/1.jpg');?>" alt="">
              </a>
              <a class="thumb-holder" data-rel="prettyPhoto" href="<?php echo base_url('site_assets/images/gallery/2.jpg');?>">
                <img src="<?php echo base_url('site_assets/images/gallery/2.jpg');?>" alt="">
              </a>
              <a class="thumb-holder" data-rel="prettyPhoto" href="<?php echo base_url('site_assets/images/gallery/3.jpg');?>">
                <img src="<?php echo base_url('site_assets/images/gallery/3.jpg');?>" alt="">
              </a>
              <a class="thumb-holder" data-rel="prettyPhoto" href="<?php echo base_url('site_assets/images/gallery/4.jpg');?>">
                <img src="<?php echo base_url('site_assets/images/gallery/4.jpg');?>" alt="">
              </a>
              <a class="thumb-holder" data-rel="prettyPhoto" href="<?php echo base_url('site_assets/images/gallery/5.jpg');?>">
                <img src="<?php echo base_url('site_assets/images/gallery/5.jpg');?>" alt="">
              </a>
              <a class="thumb-holder" data-rel="prettyPhoto" href="<?php echo base_url('site_assets/images/gallery/6.jpg');?>">
                <img src="<?php echo base_url('site_assets/images/gallery/6.jpg');?>" alt="">
              </a>
              <a class="thumb-holder" data-rel="prettyPhoto" href="<?php echo base_url('site_assets/images/gallery/6.jpg');?>">
                <img src="<?php echo base_url('site_assets/images/gallery/7.jpg');?>" alt="">
              </a>
              <a class="thumb-holder" data-rel="prettyPhoto" href="<?php echo base_url('site_assets/images/gallery/6.jpg');?>">
                <img src="<?php echo base_url('site_assets/images/gallery/8.jpg');?>" alt="">
              </a>
              <a class="thumb-holder" data-rel="prettyPhoto" href="<?php echo base_url('site_assets/images/gallery/6.jpg');?>">
                <img src="<?php echo base_url('site_assets/images/gallery/9.jpg');?>" alt="">
              </a>
            </div>
          </div>

            
        </div><!--/ end flickr -->

        <div class="col-md-4 col-sm-12 footer-widget">
          <h3 class="widget-title">Quick Links</h3>

          <ul class="unstyled arrow">
            <li><a href="#">About CraftTheme</a></li>
            <li><a href="#">How We Help</a></li>
            <li><a href="#">Upcoming Events</a></li>
            <li><a href="#">Customer Support</a></li>
            <li><a href="#">Fill a Form</a></li>
            <li><a href="#">Latest News</a></li>
            <li><a href="#">Features</a></li>
            <li><a href="#">Shortcodes</a></li>
            <li><a href="#">Portfolio</a></li>
            <li><a href="#">404 Error Page</a></li>
            <li><a href="#">Coming Soon</a></li>
            <li><a href="#">Contact</a></li>
          </ul>
  
        </div><!--/ End Recent Posts-->


        <div class="col-md-3 col-sm-12 footer-widget footer-about-us">
          <h3 class="widget-title">About Craft</h3>
          <p>We are a awward winning multinational company. We believe in quality and standard worldwide.</p>
          <p><strong>Address: </strong>1102 Saint Marys, Junction City, KS</p>
          <div class="row">
            <div class="col-md-6">
              <strong>Email: </strong>
              <p>info@craftbd.com</p>
            </div>
            <div class="col-md-6">
              <strong>Phone No.</strong>
              <p>+(785) 238-4131</p>
            </div>
          </div>
          <br/>
          <p><a href="#" class="btn btn-primary solid square">Purchase Now <i class="fa fa-long-arrow-right"></i></a></p>
        </div><!--/ end about us -->




      </div><!-- Row end -->
    </div><!-- Container end -->
  </footer><!-- Footer end -->
  

  <!-- Footer start -->
  <section id="copyright" class="copyright angle">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <ul class="footer-social unstyled">
            <li>
              <a title="Twitter" href="#">
                <span class="icon-pentagon wow bounceIn"><i class="fa fa-twitter"></i></span>
              </a>
              <a title="Facebook" href="#">
                <span class="icon-pentagon wow bounceIn"><i class="fa fa-facebook"></i></span>
              </a>
              <a title="Google+" href="#">
                <span class="icon-pentagon wow bounceIn"><i class="fa fa-google-plus"></i></span>
              </a>
              <a title="linkedin" href="#">
                <span class="icon-pentagon wow bounceIn"><i class="fa fa-linkedin"></i></span>
              </a>
              <a title="Pinterest" href="#">
                <span class="icon-pentagon wow bounceIn"><i class="fa fa-pinterest"></i></span>
              </a>
              <a title="Skype" href="#">
                <span class="icon-pentagon wow bounceIn"><i class="fa fa-skype"></i></span>
              </a>
              <a title="Dribble" href="#">
                <span class="icon-pentagon wow bounceIn"><i class="fa fa-dribbble"></i></span>
              </a>
            </li>
          </ul>
        </div>
      </div><!--/ Row end -->
      <div class="row">
        <div class="col-md-12 text-center">
          <div class="copyright-info">
               &copy; Copyright 2019 Themefisher. <span>Designed by <a href="https://themefisher.com">Themefisher.com</a></span>
              </div>
        </div>
      </div><!--/ Row end -->
       <div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix">
        <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
      </div>
    </div><!--/ Container end -->
  </section><!--/ Footer end -->

  <!-- Javascript Files
  ================================================== -->

  <!-- Bootstrap jQuery -->
  <script type="text/javascript" src="<?php echo base_url('site_assets/js/bootstrap.min.js');?>"></script>
  <!-- Style Switcher -->
  <script type="text/javascript" src="<?php echo base_url('site_assets/js/style-switcher.js');?>"></script>
  <!-- Owl Carousel -->
  <script type="text/javascript" src="<?php echo base_url('site_assets/js/owl.carousel.js');?>"></script>
  <!-- PrettyPhoto -->
  <script type="text/javascript" src="<?php echo base_url('site_assets/js/jquery.prettyPhoto.js');?>"></script>
  <!-- Bxslider -->
  <script type="text/javascript" src="<?php echo base_url('site_assets/js/jquery.flexslider.js');?>"></script>
  <!-- Owl Carousel -->
  <script type="text/javascript" src="<?php echo base_url('site_assets/js/cd-hero.js');?>"></script>
  <!-- Isotope -->
  <script type="text/javascript" src="<?php echo base_url('site_assets/js/isotope.js');?>"></script>
  <script type="text/javascript" src="<?php echo base_url('site_assets/js/ini.isotope.js');?>"></script>
  <!-- Wow Animation -->
  <script type="text/javascript" src="<?php echo base_url('site_assets/js/wow.min.js');?>"></script>
  <!-- SmoothScroll -->
  <script type="text/javascript" src="<?php echo base_url('site_assets/js/smoothscroll.js');?>"></script>
  <!-- Eeasing -->
  <script type="text/javascript" src="<?php echo base_url('site_assets/js/jquery.easing.1.3.js');?>"></script>
  <!-- Counter -->
  <script type="text/javascript" src="<?php echo base_url('site_assets/js/jquery.counterup.min.js');?>"></script>
  <!-- Waypoints -->
  <script type="text/javascript" src="<?php echo base_url('site_assets/js/waypoints.min.js');?>"></script>
  <!-- Template custom -->
  <script type="text/javascript" src="<?php echo base_url('site_assets/js/custom.js');?>"></script>
  </div><!-- Body inner end -->
</body>
</html>
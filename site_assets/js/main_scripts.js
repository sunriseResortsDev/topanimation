
function getUrl(){
 var baseurl = document.getElementById('baseurl').value;
 return(baseurl);
} 
function spinnerLoad(){
  $(document).ajaxStart(function(){
    $("#loader").show()
  });

  $(document).ajaxStop(function(){
    $("#loader").hide()
  });
}
$('#gobalSearch').change(function(){
  var url= getUrl()+'admin/site_manager/get_search_results';      
  $.ajax({
    url: url,
    dataType: "json",
    type : 'POST',
    data:{search_key:$(this).val()},
    success: function(data){
     $(".gseach-div").empty();
     $(".gseach-div").append('<br><h5 class="m-b-0 centered">search results</h5>');
     $.each(data, function(i,item){
      if (data[i].id == undefined) {
       $(".gseach-div").append('<a class="dropdown-item" href="#">'+data[i].key+'</a>');
     }else{
      $(".gseach-div").append('<a href="'+getUrl()+data[i].id+'"><span class="mail-desc">'+data[i].serial+'</span></a>');
    }            
  });
   }
 });

})
function getViewAjax(controller,fun,form_id,insteadOf=''){ 
 var url = getUrl()+controller+'/'+fun+'/'+form_id;
 jQuery.ajaxSetup({async:false});
 $('#'+insteadOf+'').empty();
 $('#'+insteadOf+'').load( ""+url+"" );
}  

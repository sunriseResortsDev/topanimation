function getUrl(){
   var baseurl = document.getElementById('baseurl').value;
   return(baseurl);
  } 


function del(id,mes,table_id,controller,func){

  var x = confirm("هل أنت متأكد إنك تريد غلق  "+mes+"؟");
    if (x == true){
        var table = $('#'+table_id+'-list-table').DataTable();
               $.ajax({
               type: 'post',
               url: getUrl()+controller+'/'+func+'/'+id,
               success: function() {
                        table.ajax.reload();
                     },
                 });
               
      }else{
           return false;
     }

  }

function reOpen(id,mes,table_id,controller,func){

  var x = confirm("هل أنت متأكد من إنك تريد إعادة فتح "+mes+"؟");
    if (x == true){
        var table = $('#'+table_id+'-list-table').DataTable();
               $.ajax({
               type: 'post',
               url: getUrl()+controller+'/'+func+'/'+id,
               success: function() {
                        table.ajax.reload();
                     },
                 });
               
      }else{
           return false;
     }

  }

  function changer(id,controller,table_id,column,value){
  var x = confirm("Are you sure you want to change this");
    if (x == true){
        var table = $('#'+table_id+'-list-table').DataTable();
               $.ajax({
               type: 'post',
               url: getUrl()+controller+'/status_change/'+id+'/'+column+'/'+value,
               success: function() {
                        table.ajax.reload();
                     },
                 });
               
      }else{
           return false;
     }

  }
 function initDTable(tid,cols,furl,data='',ord=''){
  var combdata = '';
  var myorder = '';
  if (data != '') { combdata = data}else{combdata = '{}'};
  if (ord != '') { myorder = ord}else{myorder = 0}
     $(document).ready(function() {
       $('#'+tid+'').DataTable({
          "processing"  : true,
          "serverSide" : true,
          "pagingType": "full_numbers",
          "searching": true,
          "draw" :true,
          "pageLength" :20,
          "language": {
                        "lengthMenu":'<select class="form-control">'+
                          '<option value="10">10</option>'+
                          '<option value="20">20</option>'+
                          '<option value="30">30</option>'+
                          '<option value="40">40</option>'+
                          '<option value="50">50</option>'+
                          '<option value="-1">All</option>'+
                          '</select>'
                      },
          "responsive": true,
          "order": [[ myorder, "desc" ]],
          "columns": cols,                               
            "ajax": {
               'async': false,
                url  : furl,
                type : 'POST',
                data : combdata
                 },
           });
        } );
     }


 function removeButtonDesign(){
   let btn = function(api, node, config) {
     $(node).removeClass('dt-button');
     }
   return btn;  
   }
function initDTableButtons(tid,cols,furl,data='',ord='',reportData){
  //b =>buttons
  //l =>lenghth menu
  //f =>search 
  //all =>'Blfrtip'
  var combdata = '';
  var myorder = '';
  if (data != '') { combdata = data}else{combdata = '{}'};
  if (ord != '') { myorder = ord}else{myorder = 0}
     $(document).ready(function() {
       let table = $('#'+tid+'').DataTable({
          dom:"<'row'<'col-sm-1'l><'col-sm-3 'B><'col-sm-8'f>>" +
              "<'row'<'col-sm-12'tr>>" +
              "<'row'<'col-sm-5'i><'col-sm-7'p>>", 
          buttons:[ 
                    {
                      extend: 'collection',
                      className: 'btn btn-outline-secondary btn-sm',
                      text: 'Export <i class="far fa-file-alt "></i>',
                      background:false,
                      buttons: [
                                { extend: 'copy', className: 'dropdown-item', init: removeButtonDesign(),},
                                //{ extend: 'csv', className: '', init: removeButtonDesign(),},
                                { extend: 'excel', 
                                  messageTop: ''+reportData.total+'                                      من  '+combdata.searchBy.fromDate+'       إلى '+combdata.searchBy.toDate+'  ',
                                  title: ''+reportData.reportName+'',
                                  init: removeButtonDesign(), 
                                 },
                                // { 
                                //   extend: 'pdf', 
                                //   className: '',
                                //   orientation: 'portrait',
                                //   pageSize: 'A4',
                                //   messageTop: ''+reportData.total+'                                From '+combdata.searchBy.fromDate+' To '+combdata.searchBy.toDate+'',
                                //   title: ''+reportData.reportName+'',
                                //   download: 'open',
                                //   init: removeButtonDesign(), 
                                //  },
                                { extend: 'print', className: '', 
                                  messageTop: ''+reportData.total+'                                      من  '+combdata.searchBy.fromDate+'       إلى '+combdata.searchBy.toDate+'  ',
                                  title: '                                                                             '+reportData.reportName+'',
                                  init: removeButtonDesign(), },      
                               ],
                     },
                     {    
                      extend: 'colvis',
                      text: 'Visable',
                      background:false,
                      init: removeButtonDesign(),
                     },
                  ],
          "processing"  : true,
          "serverSide" : true,
          "pagingType": "full_numbers",
          "searching": true,
          "draw" :true,
          "pageLength" :20,
          "language": {
                        "lengthMenu":'<select class="form-control">'+
                          '<option value="10">10</option>'+
                          '<option value="20">20</option>'+
                          '<option value="30">30</option>'+
                          '<option value="40">40</option>'+
                          '<option value="50">50</option>'+
                          '<option value="-1">All</option>'+
                          '</select>'
                      },
          "responsive": true,
          "order": [[ myorder, "desc" ]],
          "columns": cols,                               
            "ajax": {
               'async': false,
                url  : furl,
                type : 'POST',
                data : combdata
                 },
           });
       table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
        } );
     }     



function initDTable_normal(tid,ord,cols){
     $(document).ready(function() {
       $('#'+tid+'').DataTable({
          "pagingType": "full_numbers",
          "searching": true,
          "draw" :true,
          "pageLength" :20,
          "lengthMenu": [10, 25, 50,100],
          "responsive": true,
          "order": [[ ord, "desc" ]],
          "columns": cols,
           });
        } );
     }    

function fieldChecker(id,path){
        var name = $("#"+id+"").val();
        $.ajax({
              url:getUrl()+path,
              dataType: "json",
              type : 'POST',
              data    : {name:name},
              success: function(data){
                         if (data =='exist') {
                             $("#"+id+"").removeClass("is-valid");
                             $("#"+id+"").addClass("is-invalid");
                             $(".invalid-feedback").show();
                             $(".modal-footer #modalSubmit").attr("disabled", true);
                           }else if(data =='not_exist'){
                             $("#"+id+"").removeClass("is-invalid");
                             $("#"+id+"").addClass("is-valid");
                             $(".invalid-feedback").hide();
                             $(".modal-footer #modalSubmit").attr("disabled", false);
                          }
                        }
                });
       }


  setInterval(function(){getNotifications();}, 300000); 

  function getNotifications(){

     var url= getUrl()+'admin/site_manager/notifications';      
            $.ajax({
               'async':false,
                url: url,
                dataType: "json",
                  success: function(data){
                   $("#notifiCount").empty();
                   $("#notifiCount").append(data.count);
                   $("#notifications").empty();
                    $("#notifications").append('<br><h5 class="m-b-0 centered">Notifications</h5>');
                    $.each(data, function(i,item){
                      delete data['count'];
                      if (data[i].id == undefined) {
                        $("#notifications").append('<div class="d-flex no-block align-items-center p-10"><div class="m-l-10" style="margin-left:10px;"><h5 class="m-b-0 centered">'+data[i].key+'</h5><span class="mail-desc"></span></div></div>');
                        }else{
                        $("#notifications").append('<div class="d-flex no-block align-items-center p-10 link border-top"> <span class="btn btn btn-light btn-circle" onclick="delNotify('+data[i].id+')"><i class="fa fa-trash-alt text-danger"></i></span> <a onclick="delNotify('+data[i].id+')" href="'+getUrl()+data[i].link+'" class=""><div class="m-l-10"><h5 class="m-b-0"><span>'+data[i].head+'</span></h5><span class="mail-desc">'+data[i].message+'</span> </div>  </a> </div>');  
                       }            
                    });
                  }
             })
       }

  function delNotify(id){
     var url=getUrl()+'admin/site_manager/delete_notify/'+id;      
          $.ajax({
             'async':false,
              url: url,
              dataType: "json",
                success: function(data){
                      getNotifications();    
                  },
               }); 
                
        } 



function spinnerLoad(){
  $(document).ajaxStart(function(){
      $("#loader").show()
    });

  $(document).ajaxStop(function(){
      $("#loader").hide()
    });
}

function openFullscreen() {
          if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
           (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (document.documentElement.requestFullScreen) {  
              document.documentElement.requestFullScreen();  
            } else if (document.documentElement.mozRequestFullScreen) {  
              document.documentElement.mozRequestFullScreen();  
            } else if (document.documentElement.webkitRequestFullScreen) {  
              document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
            }  
          } else {  
            if (document.cancelFullScreen) {  
              document.cancelFullScreen();  
            } else if (document.mozCancelFullScreen) {  
              document.mozCancelFullScreen();  
            } else if (document.webkitCancelFullScreen) {  
              document.webkitCancelFullScreen();  
            }  
          }  
    }

    $('#gobalSearch').change(function(){
        var url= getUrl()+'admin/site_manager/get_search_results';      
        $.ajax({
            url: url,
            dataType: "json",
            type : 'POST',
            data:{search_key:$(this).val()},
              success: function(data){
               $(".gseach-div").empty();
                $(".gseach-div").append('<br><h5 class="m-b-0 centered">search results</h5>');
                $.each(data, function(i,item){
                  if (data[i].id == undefined) {
                     $(".gseach-div").append('<a class="dropdown-item" href="#">'+data[i].key+'</a>');
                    }else{
                    $(".gseach-div").append('<a href="'+getUrl()+data[i].id+'"><span class="mail-desc">'+data[i].serial+'</span></a>');
                   }            
                });
              }
         });
        
       })

    
function getViewAjax(controller,fun,form_id,insteadOf=''){ 
   var url = getUrl()+controller+'/'+fun+'/'+form_id;
   jQuery.ajaxSetup({async:false});
   $('#'+insteadOf+'').empty();
   $('#'+insteadOf+'').load( ""+url+"" );
}  

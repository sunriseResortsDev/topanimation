-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2020 at 08:29 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `top`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity`
--

INSERT INTO `activity` (`id`, `name`, `deleted`) VALUES
(1, 'Aqua Center', 0),
(2, 'Diving', 0),
(3, 'SPA', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(30) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pax` int(5) NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `offer_id` int(11) NOT NULL,
  `price` decimal(20,2) NOT NULL,
  `currency` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `total_cost` decimal(20,2) NOT NULL,
  `status` int(2) NOT NULL,
  `sent_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `type`, `first_name`, `last_name`, `date`, `pax`, `email`, `phone`, `offer_id`, `price`, `currency`, `total_cost`, `status`, `sent_at`, `timestamp`) VALUES
(1, '', 'Menna Allah', 'hisham', '2020-01-16', 3, 'hany.hisham@sunrise-resorts.com', '1234567890', 9, '2000.00', '$', '6000.00', 37, NULL, '2020-01-10 09:09:34'),
(2, '', 'hany', 'hisham', '2020-01-16', 2, 'hany.hisham@sunrise-resorts.com', '1234567890', 9, '2000.00', 'EGP', '4000.00', 35, NULL, '2020-01-10 08:55:52'),
(3, '', 'hany', 'hisham', '2020-01-17', 2, 'hany.hisham@sunrise-resorts.com', '1234567890', 9, '2000.00', '$', '4000.00', 36, NULL, '2020-01-10 09:07:36');

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` int(11) NOT NULL,
  `branch_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `group_id` int(11) NOT NULL,
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SUN',
  `logo` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `branch_name`, `group_id`, `code`, `logo`, `deleted`) VALUES
(1, 'Diveing Center', 1, 'DVC', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`, `lang`) VALUES
('243u7nuv9s257aejqp3aj7g16bcnu5rl', '::1', 1579275917, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('34iovm57a0l0qlo01f6g23deg7rcnrq4', '::1', 1579893513, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537393839333531333b757365725f69647c733a313a2231223b6e616d657c733a353a2261646d696e223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('3buigpkqg69rdfm9bl7u5342bteemvip', '::1', 1579620488, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537393632303438323b, ''),
('3t0fnpngdu03b0r8jn5jd2c7jlgco40j', '::1', 1579188649, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537393136353532393b757365725f69647c733a313a2231223b6e616d657c733a353a2261646d696e223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('4v83eie8dtgjjni14b3r5ac20vdtm3e2', '::1', 1579891563, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537393839313536333b, ''),
('640nhk9muuv89ckugqctnmogrhl6dm56', '::1', 1579912851, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537393839333531333b757365725f69647c733a313a2231223b6e616d657c733a353a2261646d696e223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('6l8iocsg3qfb1a52ldlg4qajh0b32lif', '::1', 1580062759, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538303036323735393b757365725f69647c733a313a2231223b6e616d657c733a353a2261646d696e223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('84b33k0vr0cc4iuhukfku5hfi2j6gv39', '::1', 1580061232, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538303036313233323b, ''),
('84b9se40l9jefpkdes9s348qovkf3aqg', '::1', 1580067432, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538303036373433323b757365725f69647c733a313a2231223b6e616d657c733a353a2261646d696e223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('9nb1okk5ghhgt5rf24s0t8h2jnnjb838', '::1', 1580067033, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538303036373033333b757365725f69647c733a313a2231223b6e616d657c733a353a2261646d696e223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('9ti0124fig8fd4arkp74nbfs1i52lrfu', '::1', 1580063934, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538303036333933343b757365725f69647c733a313a2231223b6e616d657c733a353a2261646d696e223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('b7u3etgok8pn1ihmuhfq27pqsemvk0tk', '::1', 1580061558, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538303036313535383b757365725f69647c733a313a2231223b6e616d657c733a353a2261646d696e223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('cug9j083r5a9k36l7um73u3odpcun5ss', '::1', 1580065110, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538303036353131303b757365725f69647c733a313a2231223b6e616d657c733a353a2261646d696e223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('cuh4rul49av8h0887nhgacqna1njlf2o', '::1', 1579333404, 0x7265645f75726c7c733a33343a2261646d696e2f736974655f6d616e616765722f67616c6c6572795f6d616e61676572223b5f5f63695f6c6173745f726567656e65726174657c693a313537393333333339303b, ''),
('e1rq2cq1ifs4to513puqn8ujce8akp5j', '::1', 1579892838, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537393839323833383b757365725f69647c733a313a2231223b6e616d657c733a353a2261646d696e223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('e3kmps01qd914sn0vh7lq7sa8ug3cfkc', '::1', 1579620483, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537393632303438323b, ''),
('ghekm2914a7uq0d6l2ib5ddabmdp1gto', '::1', 1580110992, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('gl1unlb8ivra5h6o393k53qjmagqo5do', '::1', 1579810125, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537393830393938353b, ''),
('h2612lsiu6q1sbo9n63iih1a51am66mu', '::1', 1579809280, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537393830393238303b, ''),
('h49pre2dni6e9gosmhfkbelt05tg430n', '::1', 1579891937, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537393839313933373b, ''),
('hgahm8fj8vtf4ofj1da9kgjukaf5qb5p', '::1', 1579626413, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537393632363430373b, ''),
('i4gsv4q1vi03c05pk30h8vop2vi7emn6', '::1', 1580065833, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538303036353833333b757365725f69647c733a313a2231223b6e616d657c733a353a2261646d696e223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('ih19n99ebq4ng64q9n3ocb03rcil3rbr', '::1', 1580063426, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538303036333432363b757365725f69647c733a313a2231223b6e616d657c733a353a2261646d696e223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('jq5v7hnk4snmrrbcpks20u9gl94nho7f', '::1', 1579807976, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537393830373937363b, ''),
('m5kkvucf0m1e6s58t86e5abbvs365dm6', '::1', 1579306818, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('m8a1lrthh2i19sf7jqd215qgrlbe13tk', '::1', 1580066549, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538303036363534393b757365725f69647c733a313a2231223b6e616d657c733a353a2261646d696e223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('mtp523udlaev41okp09ca8fgcqvntf9m', '::1', 1580067736, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538303036373733363b757365725f69647c733a313a2231223b6e616d657c733a353a2261646d696e223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('n11hqav8vaah22hpeu7dgcaa31v3imil', '::1', 1579892451, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537393839323435313b757365725f69647c733a313a2231223b6e616d657c733a353a2261646d696e223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('n2hgcfc2drusnv0utp5kk89e6faarq8j', '::1', 1579247422, 0x7265645f75726c7c733a33323a2261646d696e2f736974655f6d616e616765722f6e6f74696669636174696f6e73223b, ''),
('pu49p38bheo58fjckoekj8hm8r2iqt9v', '::1', 1579807673, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537393830373637333b, ''),
('q1o4466rllpve8aasrf6qbfpvadrlgsb', '::1', 1579807147, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537393830373134373b, ''),
('qd3v832bgj0fubk3pb1i7c2nffo7s0f8', '::1', 1579891146, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537393839313134363b, ''),
('qg9s3ctsre1dokbp3hffqv8lbdc0ct6j', '::1', 1580060611, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538303036303631313b, ''),
('t8og4hdcf6fmm8lvhlkc6v4q7runm73d', '::1', 1579939051, 0x7265645f75726c7c733a33333a2261646d696e2f736974655f6d616e616765722f736c696465725f6d616e61676572223b5f5f63695f6c6173745f726567656e65726174657c693a313537393933393030363b, ''),
('tg2o6a8o1etdj9eno0f4slv31904fnhm', '::1', 1580077936, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538303036373733363b757365725f69647c733a313a2231223b6e616d657c733a353a2261646d696e223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('uedff6cl1a978ehs9k1d3m0pil7eu9k1', '::1', 1580065472, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538303036353437323b757365725f69647c733a313a2231223b6e616d657c733a353a2261646d696e223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('uh7dlkj0cevlrvc37rss7km62nv2cgvk', '::1', 1579893164, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537393839333136343b757365725f69647c733a313a2231223b6e616d657c733a353a2261646d696e223b69735f61646d696e5f6c6f67696e7c623a313b, ''),
('vqeu08oa4fpr09s7r68o3rj05cdr9clq', '::1', 1579809985, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537393830393938353b, '');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `client_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cash` decimal(20,2) NOT NULL,
  `phone` text COLLATE utf8_unicode_ci,
  `address` text COLLATE utf8_unicode_ci,
  `contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cont_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `updated` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `clients_mail_queue`
--

CREATE TABLE `clients_mail_queue` (
  `id` int(30) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_info` int(11) DEFAULT NULL,
  `client_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `subject` text COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `status` int(2) NOT NULL,
  `sent_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `clients_mail_queue`
--

INSERT INTO `clients_mail_queue` (`id`, `type`, `client_info`, `client_name`, `email`, `subject`, `message`, `status`, `sent_at`, `timestamp`) VALUES
(1, '', NULL, '', '', '', NULL, 24, NULL, '2019-11-13 16:54:49'),
(2, '', NULL, '', '', '', NULL, 24, NULL, '2019-11-13 20:35:28'),
(3, '', NULL, '', '', '', NULL, 24, NULL, '2019-11-13 20:36:07'),
(4, '', NULL, 'محمد طرطور', '', 'sadasdasd', 'test', 24, NULL, '2019-11-13 21:11:59'),
(5, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'test', 24, NULL, '2019-11-13 21:16:24'),
(6, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'test', 24, NULL, '2019-11-13 21:20:31'),
(7, '', NULL, 'محمد طرطور', 'admin@admin.com', 'dsds', 'ttttttttttttt', 24, NULL, '2019-11-13 21:22:52'),
(8, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'sfdsfdsdf', 24, NULL, '2019-11-13 21:25:26'),
(9, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'sadfsdfsdf', 24, NULL, '2019-11-13 21:26:18'),
(10, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'scsdfsdf', 24, NULL, '2019-11-13 21:26:54'),
(11, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'rewrwer', 24, NULL, '2019-11-13 21:29:03'),
(12, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'test', 24, NULL, '2019-11-13 21:30:30'),
(13, '', NULL, 'محمد طرطور', 'admin@admin.com', 'sadasdasd', 'adsfsdfsdfsdfsdfdsf', 24, NULL, '2019-11-16 07:30:32'),
(14, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-09 07:59:43'),
(15, '', NULL, 'Johan ', 'mvmservice@hotmail.com', 'Diving center hurghada', 'Hello there. \r\nIs iT true that dive more group at the Beach of mamlouk palace and garden beach is part of your company? We Will be there in a week and going the get Some info to take diving course. We saw your website On there Facebook.   Thnx.', 24, NULL, '2019-12-11 18:00:15'),
(16, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-13 22:52:51'),
(17, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-14 16:43:48'),
(18, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-15 22:16:21'),
(19, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-18 10:11:56'),
(20, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-18 13:09:23'),
(21, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-19 04:44:36'),
(22, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-19 11:46:10'),
(23, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-20 08:43:54'),
(24, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-20 10:41:48'),
(25, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-21 01:21:04'),
(26, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-21 07:29:10'),
(27, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-21 10:44:01'),
(28, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-21 13:50:01'),
(29, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-22 02:38:28'),
(30, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-22 08:28:23'),
(31, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-22 12:11:19'),
(32, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-23 02:15:25'),
(33, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-23 09:48:54'),
(34, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-23 17:21:37'),
(35, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-23 21:50:13'),
(36, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-24 02:32:35'),
(37, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-24 23:22:45'),
(38, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-25 08:19:19'),
(39, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-25 09:41:02'),
(40, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-26 05:12:04'),
(41, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-26 08:49:29'),
(42, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-27 05:21:45'),
(43, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-27 07:57:24'),
(44, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-27 14:03:47'),
(45, '', NULL, '', '', '', NULL, 24, NULL, '2019-12-27 16:33:17'),
(46, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-03 08:23:35'),
(47, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-03 10:48:46'),
(48, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-04 04:50:45'),
(49, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-04 08:58:09'),
(50, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-04 11:04:45'),
(51, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-05 08:17:21'),
(52, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-06 05:39:13'),
(53, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-06 07:50:32'),
(54, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-07 02:42:28'),
(55, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-07 06:18:29'),
(56, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-07 10:58:39'),
(57, '', NULL, '', '', '', NULL, 24, NULL, '2020-01-07 13:54:56');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(20) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `company_name`, `rank`, `deleted`) VALUES
(1, 'عز', 1, 0),
(2, 'بشاى', 2, 0),
(3, 'سعودى سابك', 3, 0),
(4, 'سرحان', 4, 0),
(5, 'سعودى راجحى', 5, 0),
(6, 'مراكبى', 6, 0),
(7, 'بيانكو', 7, 0),
(8, 'جيوشى', 8, 0),
(9, 'عنتر', 9, 0),
(10, 'مصريين', 10, 0),
(11, 'المصرية', 11, 0),
(12, 'السويدى', 12, 0),
(13, 'الجيش بنى سويف', 13, 0),
(14, 'الجيش العريش', 14, 0),
(15, 'القومية', 15, 0),
(16, 'طرة', 16, 0),
(17, 'المسلح', 17, 0),
(18, 'السويس', 18, 0),
(19, 'سينا', 19, 0),
(20, 'رويال', 20, 0),
(21, 'الدولية', 21, 0),
(22, 'البلاح', 22, 0),
(23, 'نجمة سيناء مصيص', 23, 0),
(24, 'عثمان', 24, 0),
(25, 'بدون', 25, 0);

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(11) NOT NULL,
  `symbol` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `isdefault` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `symbol`, `name`, `isdefault`) VALUES
(1, '$', 'USD', 0),
(2, '€', 'EUR', 0),
(3, 'EGP', 'EGP', 1);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `dep_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `dep_name`, `code`, `rank`) VALUES
(1, 'الإدارة', 'MG', 1),
(2, 'الحسابات', 'ACC', 2);

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `form_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `font_icons`
--

CREATE TABLE `font_icons` (
  `id` int(11) NOT NULL,
  `icon_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `font_icons`
--

INSERT INTO `font_icons` (`id`, `icon_name`, `rank`) VALUES
(1, 'fas fa-arrow-alt-circle-left', 1),
(2, 'fa-music', 2),
(3, 'fa-search', 3),
(4, 'fa-envelope-o', 4),
(5, 'fa-heart', 5),
(6, 'fa-star', 6),
(7, 'fa-star-o', 7),
(8, 'fa-user', 8),
(9, 'fa-film', 9),
(10, 'fa-th-large', 10),
(11, 'fa-th', 11),
(12, 'fa-th-list', 12),
(13, 'fa-check', 13),
(14, 'fa-remove', 14),
(15, 'fa-close', 15),
(16, 'fa-times', 16),
(17, 'fa-search-plus', 17),
(18, 'fa-search-minus', 18),
(19, 'fa-power-off', 19),
(20, 'fa-signal', 20),
(21, 'fa-gear', 21),
(22, 'fa-cog', 22),
(23, 'fa-trash-o', 23),
(24, 'fa-home', 24),
(25, 'fa-file-o', 25),
(26, 'fa-clock-o', 26),
(27, 'fa-road', 27),
(28, 'fa-download', 28),
(29, 'fa-arrow-circle-o-do', 29),
(30, 'fa-arrow-circle-o-up', 30),
(31, 'fa-inbox', 31),
(32, 'fa-play-circle-o', 32),
(33, 'fa-rotate-right', 33),
(34, 'fa-repeat', 34),
(35, 'fa-refresh', 35),
(36, 'fa-list-alt', 36),
(37, 'fa-lock', 37),
(38, 'fa-flag', 38),
(39, 'fa-headphones', 39),
(40, 'fa-volume-off', 40),
(41, 'fa-volume-down', 41),
(42, 'fa-volume-up', 42),
(43, 'fa-qrcode', 43),
(44, 'fa-barcode', 44),
(45, 'fa-tag', 45),
(46, 'fa-tags', 46),
(47, 'fa-book', 47),
(48, 'fa-bookmark', 48),
(49, 'fa-print f', 49),
(50, 'fa-camera', 50),
(51, 'fa-font', 51),
(52, 'fa-bold', 52),
(53, 'fa-italic', 53),
(54, 'fa-text-height', 54),
(55, 'fa-text-width', 55),
(56, 'fa-align-left', 56),
(57, 'fa-align-center', 57),
(58, 'fa-align-right', 58),
(59, 'fa-align-justify', 59),
(60, 'fa-list', 60),
(61, 'fa-dedent', 61),
(62, 'fa-outdent', 62),
(63, 'fa-indent', 63),
(64, 'fa-video-camera', 64),
(65, 'fa-photo', 65),
(66, 'fa-image', 66),
(67, 'fa-picture-o', 67),
(68, 'fa-pencil', 68),
(69, 'fa-map-marker', 69),
(70, 'fa-adjust', 70),
(71, 'fa-tint', 71),
(72, 'fa-edit', 72),
(73, 'fa-pencil-square-o', 73),
(74, 'fa-share-square-o', 74),
(75, 'fa-check-square-o', 75),
(76, 'fa-arrows', 76),
(77, 'fa-step-backward', 77),
(78, 'fa-fast-backward', 78),
(79, 'fa-backward', 79),
(80, 'fa-play', 80),
(81, 'fa-pause', 81),
(82, 'fa-stop', 82),
(83, 'fa-forward', 83),
(84, 'fa-fast-forward', 84),
(85, 'fa-step-forward', 85),
(86, 'fa-eject', 86),
(87, 'fa-chevron-left', 87),
(88, 'fa-chevron-right', 88),
(89, 'fa-plus-circle', 89),
(90, 'fa-minus-circle', 90),
(91, 'fa-times-circle', 91),
(92, 'fa-check-circle', 92),
(93, 'fa-question-circle', 93),
(94, 'fa-info-circle', 94),
(95, 'fa-crosshairs', 95),
(96, 'fa-times-circle-o', 96),
(97, 'fa-check-circle-o', 97),
(98, 'fa-ban', 98),
(99, 'fa-arrow-left', 99),
(100, 'fa-arrow-right', 100),
(101, 'fa-arrow-up', 101),
(102, 'fa-arrow-down', 102),
(103, 'fa-mail-forward', 103),
(104, 'fa-share', 104),
(105, 'fa-expand', 105),
(106, 'fa-compress', 106),
(107, 'fa-plus', 107),
(108, 'fa-minus', 108),
(109, 'fa-asterisk', 109),
(110, 'fa-exclamation-circl', 110),
(111, 'fa-gift', 111),
(112, 'fa-leaf', 112),
(113, 'fa-fire', 113),
(114, 'fa-eye', 114),
(115, 'fa-eye-slash', 115),
(116, 'fa-warning', 116),
(117, 'fa-exclamation-trian', 117),
(118, 'fa-plane', 118),
(119, 'fa-calendar', 119),
(120, 'fa-random', 120),
(121, 'fa-comment', 121),
(122, 'fa-magnet', 122),
(123, 'fa-chevron-up', 123),
(124, 'fa-chevron-down', 124),
(125, 'fa-retweet', 125),
(126, 'fa-shopping-cart', 126),
(127, 'fa-folder', 127),
(128, 'fa-folder-open', 128),
(129, 'fa-arrows-v', 129),
(130, 'fa-arrows-h', 130),
(131, 'fa-bar-chart-o', 131),
(132, 'fa-bar-chart', 132),
(133, 'fa-twitter-square', 133),
(134, 'fa-facebook-square', 134),
(135, 'fa-camera-retro', 135),
(136, 'fa-key', 136),
(137, 'fa-gears', 137),
(138, 'fa-cogs', 138),
(139, 'fa-comments', 139),
(140, 'fa-thumbs-o-up', 140),
(141, 'fa-thumbs-o-down', 141),
(142, 'fa-star-half', 142),
(143, 'fa-heart-o', 143),
(144, 'fa-sign-out', 144),
(145, 'fa-linkedin-square', 145),
(146, 'fa-thumb-tack', 146),
(147, 'fa-external-link', 147),
(148, 'fa-sign-in', 148),
(149, 'fa-trophy', 149),
(150, 'fa-github-square', 150),
(151, 'fa-upload', 151),
(152, 'fa-lemon-o', 152),
(153, 'fa-phone', 153),
(154, 'fa-square-o', 154),
(155, 'fa-bookmark-o', 155),
(156, 'fa-phone-square', 156),
(157, 'fa-twitter', 157),
(158, 'fa-facebook-f', 158),
(159, 'fa-facebook', 159),
(160, 'fa-github', 160),
(161, 'fa-unlock', 161),
(162, 'fa-credit-card', 162),
(163, 'fa-rss', 163),
(164, 'fa-hdd-o', 164),
(165, 'fa-bullhorn', 165),
(166, 'fa-bell', 166),
(167, 'fa-certificate', 167),
(168, 'fa-hand-o-right', 168),
(169, 'fa-hand-o-left', 169),
(170, 'fa-hand-o-up', 170),
(171, 'fa-hand-o-down', 171),
(172, 'fa-arrow-circle-left', 172),
(173, 'fa-arrow-circle-righ', 173),
(174, 'fa-arrow-circle-up', 174),
(175, 'fa-arrow-circle-down', 175),
(176, 'fa-globe', 176),
(177, 'fa-wrench', 177),
(178, 'fa-tasks', 178),
(179, 'fa-filter', 179),
(180, 'fa-briefcase', 180),
(181, 'fa-arrows-alt', 181),
(182, 'fa-group', 182),
(183, 'fa-users', 183),
(184, 'fa-chain', 184),
(185, 'fa-link', 185),
(186, 'fa-cloud', 186),
(187, 'fa-flask', 187),
(188, 'fa-cut', 188),
(189, 'fa-scissors', 189),
(190, 'fa-copy', 190),
(191, 'fa-files-o', 191),
(192, 'fa-paperclip', 192),
(193, 'fa-save', 193),
(194, 'fa-floppy-o', 194),
(195, 'fa-square', 195),
(196, 'fa-navicon', 196),
(197, 'fa-reorder', 197),
(198, 'fa-bars', 198),
(199, 'fa-list-ul', 199),
(200, 'fa-list-ol', 200),
(201, 'fa-strikethrough', 201),
(202, 'fa-underline', 202),
(203, 'fa-table', 203),
(204, 'fa-magic', 204),
(205, 'fa-truck', 205),
(206, 'fa-pinterest', 206),
(207, 'fa-pinterest-square', 207),
(208, 'fa-google-plus-squar', 208),
(209, 'fa-google-plus', 209),
(210, 'fa-money', 210),
(211, 'fa-caret-down', 211),
(212, 'fa-caret-up', 212),
(213, 'fa-caret-left', 213),
(214, 'fa-caret-right', 214),
(215, 'fa-columns', 215),
(216, 'fa-unsorted', 216),
(217, 'fa-sort', 217),
(218, 'fa-sort-down', 218),
(219, 'fa-sort-desc', 219),
(220, 'fa-sort-up', 220),
(221, 'fa-sort-asc', 221),
(222, 'fa-envelope', 222),
(223, 'fa-linkedin', 223),
(224, 'fa-rotate-left', 224),
(225, 'fa-undo', 225),
(226, 'fa-legal', 226),
(227, 'fa-gavel', 227),
(228, 'fa-dashboard', 228),
(229, 'fa-tachometer', 229),
(230, 'fa-comment-o', 230),
(231, 'fa-comments-o', 231),
(232, 'fa-flash', 232),
(233, 'fa-bolt', 233),
(234, 'fa-sitemap', 234),
(235, 'fa-umbrella', 235),
(236, 'fa-paste', 236),
(237, 'fa-clipboard', 237),
(238, 'fa-lightbulb-o', 238),
(239, 'fa-exchange', 239),
(240, 'fa-cloud-download', 240),
(241, 'fa-cloud-upload', 241),
(242, 'fa-user-md', 242),
(243, 'fa-stethoscope', 243),
(244, 'fa-suitcase', 244),
(245, 'fa-bell-o', 245),
(246, 'fa-coffee', 246),
(247, 'fa-cutlery', 247),
(248, 'fa-file-text-o', 248),
(249, 'fa-building-o', 249),
(250, 'fa-hospital-o', 250),
(251, 'fa-ambulance', 251),
(252, 'fa-medkit', 252),
(253, 'fa-fighter-jet', 253),
(254, 'fa-beer', 254),
(255, 'fa-h-square', 255),
(256, 'fa-plus-square', 256),
(257, 'fa-angle-double-left', 257),
(258, 'fa-angle-double-righ', 258),
(259, 'fa-angle-double-up', 259),
(260, 'fa-angle-double-down', 260),
(261, 'fa-angle-left', 261),
(262, 'fa-angle-right', 262),
(263, 'fa-angle-up', 263),
(264, 'fa-angle-down', 264),
(265, 'fa-desktop', 265),
(266, 'fa-laptop', 266),
(267, 'fa-tablet', 267),
(268, 'fa-mobile-phone', 268),
(269, 'fa-mobile', 269),
(270, 'fa-circle-o', 270),
(271, 'fa-quote-left', 271),
(272, 'fa-quote-right', 272),
(273, 'fa-spinner', 273),
(274, 'fa-circle', 274),
(275, 'fa-mail-reply', 275),
(276, 'fa-reply', 276),
(277, 'fa-github-alt', 277),
(278, 'fa-folder-o', 278),
(279, 'fa-folder-open-o', 279),
(280, 'fa-smile-o', 280),
(281, 'fa-frown-o', 281),
(282, 'fa-meh-o', 282),
(283, 'fa-gamepad', 283),
(284, 'fa-keyboard-o', 284),
(285, 'fa-flag-o', 285),
(286, 'fa-flag-checkered', 286),
(287, 'fa-terminal', 287),
(288, 'fa-code', 288),
(289, 'fa-mail-reply-all', 289),
(290, 'fa-reply-all', 290),
(291, 'fa-star-half-empty', 291),
(292, 'fa-star-half-full', 292),
(293, 'fa-star-half-o', 293),
(294, 'fa-location-arrow', 294),
(295, 'fa-crop', 295),
(296, 'fa-code-fork', 296),
(297, 'fa-unlink', 297),
(298, 'fa-chain-broken', 298),
(299, 'fa-question', 299),
(300, 'fa-info', 300),
(301, 'fa-exclamation', 301),
(302, 'fa-superscript', 302),
(303, 'fa-subscript', 303),
(304, 'fa-eraser', 304),
(305, 'fa-puzzle-piece', 305),
(306, 'fa-microphone', 306),
(307, 'fa-microphone-slash', 307),
(308, 'fa-shield', 308),
(309, 'fa-calendar-o', 309),
(310, 'fa-fire-extinguisher', 310),
(311, 'fa-rocket', 311),
(312, 'fa-maxcdn', 312),
(313, 'fa-chevron-circle-le', 313),
(314, 'fa-chevron-circle-ri', 314),
(315, 'fa-chevron-circle-up', 315),
(316, 'fa-chevron-circle-do', 316),
(317, 'fa-html5', 317),
(318, 'fa-css3', 318),
(319, 'fa-anchor', 319),
(320, 'fa-unlock-alt', 320),
(321, 'fa-bullseye', 321),
(322, 'fa-ellipsis-h', 322),
(323, 'fa-ellipsis-v', 323),
(324, 'fa-rss-square', 324),
(325, 'fa-play-circle', 325),
(326, 'fa-ticket', 326),
(327, 'fa-minus-square', 327),
(328, 'fa-minus-square-o', 328),
(329, 'fa-level-up', 329),
(330, 'fa-level-down', 330),
(331, 'fa-check-square', 331),
(332, 'fa-pencil-square', 332),
(333, 'fa-external-link-squ', 333),
(334, 'fa-share-square', 334),
(335, 'fa-compass', 335),
(336, 'fa-toggle-down', 336),
(337, 'fa-caret-square-o-do', 337),
(338, 'fa-toggle-up', 338),
(339, 'fa-caret-square-o-up', 339),
(340, 'fa-toggle-right', 340),
(341, 'fa-caret-square-o-ri', 341),
(342, 'fa-euro', 342),
(343, 'fa-eur', 343),
(344, 'fa-gbp', 344),
(345, 'fa-dollar', 345),
(346, 'fa-usd', 346),
(347, 'fa-rupee', 347),
(348, 'fa-inr', 348),
(349, 'fa-cny', 349),
(350, 'fa-rmb', 350),
(351, 'fa-yen', 351),
(352, 'fa-jpy', 352),
(353, 'fa-ruble', 353),
(354, 'fa-rouble', 354),
(355, 'fa-rub', 355),
(356, 'fa-won', 356),
(357, 'fa-krw', 357),
(358, 'fa-bitcoin', 358),
(359, 'fa-btc', 359),
(360, 'fa-file', 360),
(361, 'fa-file-text', 361),
(362, 'fa-sort-alpha-asc', 362),
(363, 'fa-sort-alpha-desc', 363),
(364, 'fa-sort-amount-asc', 364),
(365, 'fa-sort-amount-desc', 365),
(366, 'fa-sort-numeric-asc', 366),
(367, 'fa-sort-numeric-desc', 367),
(368, 'fa-thumbs-up', 368),
(369, 'fa-thumbs-down', 369),
(370, 'fa-youtube-square', 370),
(371, 'fa-youtube', 371),
(372, 'fa-xing', 372),
(373, 'fa-xing-square', 373),
(374, 'fa-youtube-play', 374),
(375, 'fa-dropbox', 375),
(376, 'fa-stack-overflow', 376),
(377, 'fa-instagram', 377),
(378, 'fa-flickr', 378),
(379, 'fa-adn', 379),
(380, 'fa-bitbucket', 380),
(381, 'fa-bitbucket-square', 381),
(382, 'fa-tumblr', 382),
(383, 'fa-tumblr-square', 383),
(384, 'fa-long-arrow-down', 384),
(385, 'fa-long-arrow-up', 385),
(386, 'fa-long-arrow-left', 386),
(387, 'fa-long-arrow-right', 387),
(388, 'fa-apple', 388),
(389, 'fa-windows', 389),
(390, 'fa-android', 390),
(391, 'fa-linux', 391),
(392, 'fa-dribbble', 392),
(393, 'fa-skype', 393),
(394, 'fa-foursquare', 394),
(395, 'fa-trello', 395),
(396, 'fa-female', 396),
(397, 'fa-male', 397),
(398, 'fa-gittip', 398),
(399, 'fa-gratipay', 399),
(400, 'fa-sun-o', 400),
(401, 'fa-moon-o', 401),
(402, 'fa-archive', 402),
(403, 'fa-bug', 403),
(404, 'fa-vk', 404),
(405, 'fa-weibo', 405),
(406, 'fa-renren', 406),
(407, 'fa-pagelines', 407),
(408, 'fa-stack-exchange', 408),
(409, 'fa-arrow-circle-o-ri', 409),
(410, 'fa-arrow-circle-o-le', 410),
(411, 'fa-toggle-left', 411),
(412, 'fa-caret-square-o-le', 412),
(413, 'fa-dot-circle-o', 413),
(414, 'fa-wheelchair', 414),
(415, 'fa-vimeo-square', 415),
(416, 'fa-turkish-lira', 416),
(417, 'fa-try', 417),
(418, 'fa-plus-square-o', 418),
(419, 'fa-space-shuttle', 419),
(420, 'fa-slack', 420),
(421, 'fa-envelope-square', 421),
(422, 'fa-wordpress', 422),
(423, 'fa-openid', 423),
(424, 'fa-institution', 424),
(425, 'fa-bank', 425),
(426, 'fa-university', 426),
(427, 'fa-mortar-board', 427),
(428, 'fa-graduation-cap', 428),
(429, 'fa-yahoo', 429),
(430, 'fa-google', 430),
(431, 'fa-reddit', 431),
(432, 'fa-reddit-square', 432),
(433, 'fa-stumbleupon-circl', 433),
(434, 'fa-stumbleupon', 434),
(435, 'fa-delicious', 435),
(436, 'fa-digg', 436),
(437, 'fa-pied-piper', 437),
(438, 'fa-pied-piper-alt', 438),
(439, 'fa-drupal', 439),
(440, 'fa-joomla', 440),
(441, 'fa-language', 441),
(442, 'fa-fax', 442),
(443, 'fa-building', 443),
(444, 'fa-child', 444),
(445, 'fa-paw', 445),
(446, 'fa-spoon', 446),
(447, 'fa-cube', 447),
(448, 'fa-cubes', 448),
(449, 'fa-behance', 449),
(450, 'fa-behance-square', 450),
(451, 'fa-steam', 451),
(452, 'fa-steam-square', 452),
(453, 'fa-recycle', 453),
(454, 'fa-automobile', 454),
(455, 'fa-car', 455),
(456, 'fa-cab', 456),
(457, 'fa-taxi', 457),
(458, 'fa-tree', 458),
(459, 'fa-spotify', 459),
(460, 'fa-deviantart', 460),
(461, 'fa-soundcloud', 461),
(462, 'fa-database', 462),
(463, 'fa-file-pdf-o', 463),
(464, 'fa-file-word-o', 464),
(465, 'fa-file-excel-o', 465),
(466, 'fa-file-powerpoint-o', 466),
(467, 'fa-file-photo-o', 467),
(468, 'fa-file-picture-o', 468),
(469, 'fa-file-image-o', 469),
(470, 'fa-file-zip-o', 470),
(471, 'fa-file-archive-o', 471),
(472, 'fa-file-sound-o', 472),
(473, 'fa-file-audio-o', 473),
(474, 'fa-file-movie-o', 474),
(475, 'fa-file-video-o', 475),
(476, 'fa-file-code-o', 476),
(477, 'fa-vine', 477),
(478, 'fa-codepen', 478),
(479, 'fa-jsfiddle', 479),
(480, 'fa-life-bouy', 480),
(481, 'fa-life-buoy', 481),
(482, 'fa-life-saver', 482),
(483, 'fa-support', 483),
(484, 'fa-life-ring', 484),
(485, 'fa-circle-o-notch', 485),
(486, 'fa-ra', 486),
(487, 'fa-rebel', 487),
(488, 'fa-ge', 488),
(489, 'fa-empire', 489),
(490, 'fa-git-square', 490),
(491, 'fa-git', 491),
(492, 'fa-hacker-news', 492),
(493, 'fa-tencent-weibo', 493),
(494, 'fa-qq', 494),
(495, 'fa-wechat', 495),
(496, 'fa-weixin', 496),
(497, 'fa-send', 497),
(498, 'fa-paper-plane', 498),
(499, 'fa-send-o', 499),
(500, 'fa-paper-plane-o', 500),
(501, 'fa-history', 501),
(502, 'fa-genderless', 502),
(503, 'fa-circle-thin', 503),
(504, 'fa-header', 504),
(505, 'fa-paragraph', 505),
(506, 'fa-sliders', 506),
(507, 'fa-share-alt', 507),
(508, 'fa-share-alt-square', 508),
(509, 'fa-bomb', 509),
(510, 'fa-soccer-ball-o', 510),
(511, 'fa-futbol-o', 511),
(512, 'fa-tty', 512),
(513, 'fa-binoculars', 513),
(514, 'fa-plug', 514),
(515, 'fa-slideshare', 515),
(516, 'fa-twitch', 516),
(517, 'fa-yelp', 517),
(518, 'fa-newspaper-o', 518),
(519, 'fa-wifi', 519),
(520, 'fa-calculator', 520),
(521, 'fa-paypal', 521),
(522, 'fa-google-wallet', 522),
(523, 'fa-cc-visa', 523),
(524, 'fa-cc-mastercard', 524),
(525, 'fa-cc-discover', 525),
(526, 'fa-cc-amex', 526),
(527, 'fa-cc-paypal', 527),
(528, 'fa-cc-stripe', 528),
(529, 'fa-bell-slash', 529),
(530, 'fa-bell-slash-o', 530),
(531, 'fa-trash', 531),
(532, 'fa-copyright', 532),
(533, 'fa-at', 533),
(534, 'fa-eyedropper', 534),
(535, 'fa-paint-brush', 535),
(536, 'fa-birthday-cake', 536),
(537, 'fa-area-chart', 537),
(538, 'fa-pie-chart', 538),
(539, 'fa-line-chart', 539),
(540, 'fa-lastfm', 540),
(541, 'fa-lastfm-square', 541),
(542, 'fa-toggle-off', 542),
(543, 'fa-toggle-on', 543),
(544, 'fa-bicycle', 544),
(545, 'fa-bus', 545),
(546, 'fa-ioxhost', 546),
(547, 'fa-angellist', 547),
(548, 'fa-cc za', 548),
(549, 'fa-shekel', 549),
(550, 'fa-sheqel', 550),
(551, 'fa-ils', 551),
(552, 'fa-meanpath', 552),
(553, 'fa-buysellads', 553),
(554, 'fa-connectdevelop', 554),
(555, 'fa-dashcube', 555),
(556, 'fa-forumbee', 556),
(557, 'fa-leanpub', 557),
(558, 'fa-sellsy', 558),
(559, 'fa-shirtsinbulk', 559),
(560, 'fa-simplybuilt', 560),
(561, 'fa-skyatlas', 561),
(562, 'fa-cart-plus', 562),
(563, 'fa-cart-arrow-down', 563),
(564, 'fa-diamond', 564),
(565, 'fa-ship', 565),
(566, 'fa-user-secret', 566),
(567, 'fa-motorcycle', 567),
(568, 'fa-street-view', 568),
(569, 'fa-heartbeat', 569),
(570, 'fa-venus', 570),
(571, 'fa-mars', 571),
(572, 'fa-mercury', 572),
(573, 'fa-transgender', 573),
(574, 'fa-transgender-alt', 574),
(575, 'fa-venus-double', 575),
(576, 'fa-mars-double', 576),
(577, 'fa-venus-mars', 577),
(578, 'fa-mars-stroke', 578),
(579, 'fa-mars-stroke-v', 579),
(580, 'fa-mars-stroke-h', 580),
(581, 'fa-neuter', 581),
(582, 'fa-facebook-official', 582),
(583, 'fa-pinterest-p', 583),
(584, 'fa-whatsapp', 584),
(585, 'fa-server', 585),
(586, 'fa-user-plus', 586),
(587, 'fa-user-times', 587),
(588, 'fa-hotel', 588),
(589, 'fa-bed', 589),
(590, 'fa-viacoin', 590),
(591, 'fa-train', 591),
(592, 'fa-subway', 592),
(593, 'fa-medium', 593),
(594, 'fa-snowflake', 0);

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`id`, `name`, `deleted`) VALUES
(1, 'Pharaoh Azur Beach Resort', 0),
(2, 'SUNRISE Garden Beach Resort -Select-', 0),
(3, 'SENTIDO Mamlouk Palace', 0),
(4, 'SUNRISE Crystal Bay Resort- Grand Select-', 0),
(5, 'SUNRISE Royal Makadi Aqua Resort', 0);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `english` text COLLATE utf8_unicode_ci NOT NULL,
  `german` text COLLATE utf8_unicode_ci,
  `french` text COLLATE utf8_unicode_ci,
  `arabic` text COLLATE utf8_unicode_ci,
  `updated_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `english`, `german`, `french`, `arabic`, `updated_at`, `timestamp`) VALUES
(1, 'insert test phrase', 'test', 'test2', '', '', '2019-11-20 21:11:19'),
(2, '01002741794', NULL, NULL, NULL, '', '2020-01-13 16:48:57'),
(3, 'info@divemore-group.com', NULL, NULL, NULL, '', '2020-01-13 16:48:57'),
(4, '9am-5pm', NULL, NULL, NULL, '', '2020-01-13 16:48:57'),
(5, 'Our purpose is to positively impact lifestyles of the community. We serve through consistent, timely, quality,  efficient and  value  added  delivery of  innovative joiness that  constantly exceed expectations.', NULL, NULL, NULL, '', '2020-01-13 16:48:57'),
(6, 'Don\'t hesitate to contact us with any questions or inquiries.', NULL, NULL, NULL, '', '2020-01-13 16:48:57'),
(7, 'Home', '', '', 'المقدمة', '', '2020-01-13 17:00:35'),
(8, 'Portfolio', '', '', 'سابقة اعمال', '', '2020-01-13 17:01:12'),
(9, 'About Dive Mor', NULL, NULL, NULL, '', '2020-01-26 18:58:40'),
(10, ' ', NULL, NULL, NULL, '', '2020-01-26 18:58:40'),
(11, 'Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\r\nOur company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\r\nOur company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation', NULL, NULL, NULL, '', '2020-01-26 18:58:40'),
(12, 'About D', NULL, NULL, NULL, '', '2020-01-26 18:58:51'),
(13, 'About Dive ', NULL, NULL, NULL, '', '2020-01-26 18:59:01'),
(14, 'About Us', NULL, NULL, NULL, '', '2020-01-26 18:59:44'),
(15, ' WE ARE AWWARED WINNING COMPANY', NULL, NULL, NULL, '', '2020-01-26 18:59:44'),
(16, 'Over the year we have lots of experience in our field. In sit amet massa sapien. Vestibulum diam turpis, gravida in lobortis id, ornare a eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sagittis nulla non elit dignissim suscipit. Duis lorem nulla, eleifend.', NULL, NULL, NULL, '', '2020-01-26 18:59:44'),
(17, 'Who we are', NULL, NULL, NULL, '', '2020-01-26 19:00:11'),
(18, ' WE HAVE WORLDWIDE BUSINESS', NULL, NULL, NULL, '', '2020-01-26 19:00:11'),
(19, 'Helvetica cold-pressed lomo messenger bag ugh. Vinyl jean shorts Austin pork belly PBR retro, Etsy VHS kitsch actually messenger bag pug. Pbrb semiotics try-hard, Schlitz occupy dreamcatcher master cleanse Marfa Vice tofu.', NULL, NULL, NULL, '', '2020-01-26 19:00:11'),
(20, 'What We Do', NULL, NULL, NULL, '', '2020-01-26 19:00:35'),
(21, ' WE BUILD READYMADE APPLICATIONS', NULL, NULL, NULL, '', '2020-01-26 19:00:35'),
(22, 'Achievements', NULL, NULL, NULL, '', '2020-01-26 19:01:32'),
(23, ' CLEAN AND MODERN DESIGN', NULL, NULL, NULL, '', '2020-01-26 19:01:32'),
(24, 'Over', NULL, NULL, NULL, '', '2020-01-26 19:10:49'),
(25, ' the year we have lots of experience in our field. In sit amet massa sapien. Vestibulum diam turpis, gravida in lobortis id, ornare a eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sagittis nulla non elit dignissim suscipit. Duis lorem nulla, eleifend.', NULL, NULL, NULL, '', '2020-01-26 19:11:01'),
(26, 'Hurghada', NULL, NULL, NULL, '', '2020-01-26 19:37:13'),
(27, 'Diving', NULL, NULL, NULL, '', '2020-01-26 19:37:13'),
(28, 'Trips', NULL, NULL, NULL, '', '2020-01-26 19:37:13'),
(29, 'development', NULL, NULL, NULL, '', '2020-01-26 19:37:13'),
(30, 'joomla', NULL, NULL, NULL, '', '2020-01-26 19:37:14'),
(31, 'wordpress', NULL, NULL, NULL, '', '2020-01-26 19:37:14'),
(32, 'web-design', NULL, NULL, NULL, '', '2020-01-26 19:37:14'),
(33, 'new', NULL, NULL, NULL, '', '2020-01-26 19:37:14'),
(34, 'new2', NULL, NULL, NULL, '', '2020-01-26 19:37:14');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `module_id` int(11) NOT NULL,
  `target` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mini_target_id` int(11) NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `new_data` text COLLATE utf8_unicode_ci NOT NULL,
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `new_items` text COLLATE utf8_unicode_ci NOT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `log_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `user_id`, `action`, `module_id`, `target`, `target_id`, `mini_target_id`, `data`, `new_data`, `items`, `new_items`, `comments`, `ip`, `log_time`) VALUES
(1, 1, 'update', 20, 'Footer', '11', 0, '{\"id\":\"11\",\"header\":\"\",\"title\":\"\",\"paragraph\":\"Our purpose is to positively impact lifestyles of the community. We serve through consistent, timely, quality,  efficient and  value  added  delivery of engineered, innovative, and tailor-made technology solutions that  constantly exceed expectations.\",\"points\":\"\",\"meta\":\"footer_about\",\"img\":\"\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"11\",\"meta\":\"footer_about\",\"paragraph\":\"Our purpose is to positively impact lifestyles of the community. We serve through consistent, timely, quality,  efficient and  value  added  delivery of  innovative joiness that  constantly exceed expectations.\"}', '0', '0', 'updated footer data: 11', '::1', '2019-10-28 20:53:14'),
(2, 1, 'Create', 22, 'gallery', '25', 0, '0', '{\"meta\":\"gallery\",\"header\":\"gg\",\"title\":\"gg\",\"points\":\"gggg\",\"rank\":\"5\",\"img\":false}', '0', '0', 'added gallery:', '::1', '2019-11-02 19:22:20'),
(3, 1, 'update', 22, 'gallery', '25', 0, '{\"id\":\"25\",\"header\":\"gg\",\"title\":\"gg\",\"paragraph\":\"\",\"points\":\"gggg\",\"meta\":\"gallery\",\"img\":\"0\",\"link\":\"\",\"rank\":\"5\"}', '{\"id\":\"25\",\"meta\":\"gallery\",\"header\":\"gg\",\"title\":\"gg\",\"points\":\"gggg\",\"rank\":\"5\",\"img\":\"6bc837d5-1df8-4527-8e1a-61ba7a17c5a8.png\"}', '0', '0', 'updated gallery: 25', '::1', '2019-11-02 19:22:35'),
(4, 1, 'update', 22, 'gallery', '25', 0, '{\"id\":\"25\",\"header\":\"gg\",\"title\":\"gg\",\"paragraph\":\"\",\"points\":\"gggg\",\"meta\":\"gallery\",\"img\":\"6bc837d5-1df8-4527-8e1a-61ba7a17c5a8.png\",\"link\":\"\",\"rank\":\"5\"}', '{\"id\":\"25\",\"meta\":\"gallery\",\"header\":\"gg\",\"title\":\"gg\",\"points\":\"gggg\",\"rank\":\"5\",\"img\":\"556a0152-fab8-428f-9010-215a63d9fa00.png\"}', '0', '0', 'updated gallery: 25', '::1', '2019-11-02 19:22:43'),
(5, 1, 'Delete', 22, 'gallery', '25', 0, '{\"id\":\"25\",\"header\":\"gg\",\"title\":\"gg\",\"paragraph\":\"\",\"points\":\"gggg\",\"meta\":\"gallery\",\"img\":\"556a0152-fab8-428f-9010-215a63d9fa00.png\",\"link\":\"\",\"rank\":\"5\"}', '0', '0', '0', 'Deleted gallery:25', '::1', '2019-11-02 19:22:52'),
(6, 1, 'update', 22, 'gallery', '16', 0, '{\"id\":\"16\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"Ducting\",\"meta\":\"gallery\",\"img\":\"IMG_9847_(1).JPG\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"16\",\"meta\":\"gallery\",\"header\":\"Business City\",\"title\":\"Web Development\",\"points\":\"Ducting\",\"rank\":\"1\",\"img\":\"3_archeologygallery_nationalgeographic_17407091.jpg\"}', '0', '0', 'updated gallery: 16', '::1', '2019-11-02 19:47:52'),
(7, 1, 'update', 22, 'gallery', '17', 0, '{\"id\":\"17\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"Ducting\",\"meta\":\"gallery\",\"img\":\"WhatsApp_Image_2019-03-19_at_07.41.35.jpeg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"17\",\"meta\":\"gallery\",\"header\":\"Business City\",\"title\":\"Web Development\",\"points\":\"Ducting\",\"rank\":\"1\",\"img\":\"1295561.jpg\"}', '0', '0', 'updated gallery: 17', '::1', '2019-11-02 19:47:52'),
(8, 1, 'update', 22, 'gallery', '18', 0, '{\"id\":\"18\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"Ducting\",\"meta\":\"gallery\",\"img\":\"ALKAWARY_VILLA_AT_ALDUHAIL_(3).jpeg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"18\",\"meta\":\"gallery\",\"header\":\"Business City\",\"title\":\"Web Development\",\"points\":\"Ducting\",\"rank\":\"1\",\"img\":\"3254291.jpg\"}', '0', '0', 'updated gallery: 18', '::1', '2019-11-02 19:47:52'),
(9, 1, 'update', 22, 'gallery', '19', 0, '{\"id\":\"19\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"Ducting\",\"meta\":\"gallery\",\"img\":\"ALKAWARY_VILLA_AT_ALDUHAIL_(1).jpeg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"19\",\"meta\":\"gallery\",\"header\":\"Business City\",\"title\":\"Web Development\",\"points\":\"Ducting\",\"rank\":\"1\",\"img\":\"3254591.jpg\"}', '0', '0', 'updated gallery: 19', '::1', '2019-11-02 19:47:53'),
(10, 1, 'update', 22, 'gallery', '20', 0, '{\"id\":\"20\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"MCC__Panel\",\"meta\":\"gallery\",\"img\":\"MCC_Panel_Installations_(12).jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"20\",\"meta\":\"gallery\",\"header\":\"Business City\",\"title\":\"Web Development\",\"points\":\"MCC__Panel\",\"rank\":\"1\",\"img\":\"9513261.jpg\"}', '0', '0', 'updated gallery: 20', '::1', '2019-11-02 19:47:53'),
(11, 1, 'update', 22, 'gallery', '21', 0, '{\"id\":\"21\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"ventilation\",\"meta\":\"gallery\",\"img\":\"Ventilation_Fans_Installation_(1).jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"21\",\"meta\":\"gallery\",\"header\":\"Business City\",\"title\":\"Web Development\",\"points\":\"ventilation\",\"rank\":\"1\",\"img\":\"Cenote-Taj-Mahal-3-of-51.jpg\"}', '0', '0', 'updated gallery: 21', '::1', '2019-11-02 19:47:53'),
(12, 1, 'update', 22, 'gallery', '16', 0, '{\"id\":\"16\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"Ducting\",\"meta\":\"gallery\",\"img\":\"3_archeologygallery_nationalgeographic_17407091.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"16\",\"meta\":\"gallery\",\"header\":\"Business City\",\"title\":\"Web Development\",\"points\":\"trip_1\",\"rank\":\"1\"}', '0', '0', 'updated gallery: 16', '::1', '2019-11-02 19:48:54'),
(13, 1, 'update', 22, 'gallery', '17', 0, '{\"id\":\"17\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"Ducting\",\"meta\":\"gallery\",\"img\":\"1295561.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"17\",\"meta\":\"gallery\",\"header\":\"Business City\",\"title\":\"Web Development\",\"points\":\"trip_1\",\"rank\":\"1\"}', '0', '0', 'updated gallery: 17', '::1', '2019-11-02 19:48:54'),
(14, 1, 'update', 22, 'gallery', '18', 0, '{\"id\":\"18\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"Ducting\",\"meta\":\"gallery\",\"img\":\"3254291.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"18\",\"meta\":\"gallery\",\"header\":\"Business City\",\"title\":\"Web Development\",\"points\":\"trip_2\",\"rank\":\"1\"}', '0', '0', 'updated gallery: 18', '::1', '2019-11-02 19:48:54'),
(15, 1, 'update', 22, 'gallery', '19', 0, '{\"id\":\"19\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"Ducting\",\"meta\":\"gallery\",\"img\":\"3254591.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"19\",\"meta\":\"gallery\",\"header\":\"Business City\",\"title\":\"Web Development\",\"points\":\"trip_3\",\"rank\":\"1\"}', '0', '0', 'updated gallery: 19', '::1', '2019-11-02 19:48:55'),
(16, 1, 'update', 22, 'gallery', '20', 0, '{\"id\":\"20\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"MCC__Panel\",\"meta\":\"gallery\",\"img\":\"9513261.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"20\",\"meta\":\"gallery\",\"header\":\"Business City\",\"title\":\"Web Development\",\"points\":\"trip_4\",\"rank\":\"1\"}', '0', '0', 'updated gallery: 20', '::1', '2019-11-02 19:48:55'),
(17, 1, 'update', 22, 'gallery', '21', 0, '{\"id\":\"21\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"ventilation\",\"meta\":\"gallery\",\"img\":\"Cenote-Taj-Mahal-3-of-51.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"21\",\"meta\":\"gallery\",\"header\":\"Business City\",\"title\":\"Web Development\",\"points\":\"trip_5\",\"rank\":\"1\"}', '0', '0', 'updated gallery: 21', '::1', '2019-11-02 19:48:55'),
(18, 1, 'update', 14, 'about', '0', 0, '{\"id\":\"1\",\"header\":\"About Alamtc\",\"title\":\"PROJECT MAINTENANCE\",\"paragraph\":\"Our company Alamtc, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\",\"points\":\"[{\\\"rank\\\":\\\"1\\\",\\\"point\\\":\\\"heating ventilation\\\"},{\\\"rank\\\":\\\"2\\\",\\\"point\\\":\\\"air-conditioning equipment and MEP services\\\"},{\\\"rank\\\":\\\"3\\\",\\\"point\\\":\\\"ducting system\\\"},{\\\"rank\\\":\\\"4\\\",\\\"point\\\":\\\"trading, spare parts\\\"},{\\\"rank\\\":\\\"5\\\",\\\"point\\\":\\\"technical advice and solutions\\\"}]\",\"meta\":\"about\",\"img\":\"1.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"header\":\"About Dive More \",\"title\":\" \",\"paragraph\":\"Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\\r\\nOur company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\\r\\nOur company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\",\"points\":\"{\\\"1\\\":{\\\"point\\\":\\\"diving classes\\\",\\\"rank\\\":\\\"1\\\"},\\\"2\\\":{\\\"point\\\":\\\"snorkling\\\",\\\"rank\\\":\\\"2\\\"},\\\"3\\\":{\\\"point\\\":\\\"water sports\\\",\\\"rank\\\":\\\"3\\\"},\\\"4\\\":{\\\"point\\\":\\\"diving classes\\\",\\\"rank\\\":\\\"4\\\"},\\\"5\\\":{\\\"point\\\":\\\"diving classes\\\",\\\"rank\\\":\\\"5\\\"}}\"}', '0', '0', 'updated about data', '::1', '2019-11-15 19:35:32'),
(19, 1, 'update', 14, 'about', '0', 0, '{\"id\":\"1\",\"header\":\"About Dive More \",\"title\":\" \",\"paragraph\":\"Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\\r\\nOur company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\\r\\nOur company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\",\"points\":\"{\\\"1\\\":{\\\"point\\\":\\\"diving classes\\\",\\\"rank\\\":\\\"1\\\"},\\\"2\\\":{\\\"point\\\":\\\"snorkling\\\",\\\"rank\\\":\\\"2\\\"},\\\"3\\\":{\\\"point\\\":\\\"water sports\\\",\\\"rank\\\":\\\"3\\\"},\\\"4\\\":{\\\"point\\\":\\\"diving classes\\\",\\\"rank\\\":\\\"4\\\"},\\\"5\\\":{\\\"point\\\":\\\"diving classes\\\",\\\"rank\\\":\\\"5\\\"}}\",\"meta\":\"about\",\"img\":\"1.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"header\":\"About Dive More \",\"title\":\" \",\"paragraph\":\"Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\\r\\nOur company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\\r\\nOur company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\",\"points\":\"{\\\"1\\\":{\\\"point\\\":\\\"diving classes\\\",\\\"rank\\\":\\\"1\\\"},\\\"2\\\":{\\\"point\\\":\\\"snorkling\\\",\\\"rank\\\":\\\"2\\\"},\\\"3\\\":{\\\"point\\\":\\\"water sports\\\",\\\"rank\\\":\\\"3\\\"},\\\"4\\\":{\\\"point\\\":\\\"diving classes\\\",\\\"rank\\\":\\\"4\\\"},\\\"5\\\":{\\\"point\\\":\\\"diving classes\\\",\\\"rank\\\":\\\"5\\\"}}\",\"img\":\"325429.jpg\"}', '0', '0', 'updated about data', '::1', '2019-11-15 19:36:37'),
(20, 1, 'Create', 17, 'portfolio', '25', 0, '0', '{\"meta\":\"portfolio\",\"header\":\"Business Cit\",\"title\":\"3.5\",\"paragraph\":\"test test\",\"rank\":\"1\",\"img\":\"129556.jpg\"}', '0', '0', 'added portfolio:', '::1', '2019-11-15 20:04:56'),
(21, 1, 'update', 17, 'portfolio', '25', 0, '{\"id\":\"25\",\"header\":\"Business Cit\",\"title\":\"3.5\",\"paragraph\":\"test test\",\"points\":\"\",\"meta\":\"portfolio\",\"img\":\"129556.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"25\",\"meta\":\"portfolio\",\"header\":\"Business Cit\",\"title\":\"3.5\",\"paragraph\":\"test test  test testtest testtest testtest testtest test\",\"rank\":\"1\"}', '0', '0', 'updated portfolio: 25', '::1', '2019-11-15 20:05:10'),
(22, 1, 'update', 17, 'portfolio', '25', 0, '{\"id\":\"25\",\"header\":\"Business Cit\",\"title\":\"3.5\",\"paragraph\":\"test test  test testtest testtest testtest testtest test\",\"points\":\"\",\"meta\":\"portfolio\",\"img\":\"129556.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"25\",\"meta\":\"portfolio\",\"header\":\"Business City\",\"title\":\"3.5\",\"paragraph\":\"test test  test test\",\"rank\":\"1\"}', '0', '0', 'updated portfolio: 25', '::1', '2019-11-15 20:05:40'),
(23, 1, 'Create', 17, 'portfolio', '26', 0, '0', '{\"meta\":\"portfolio\",\"header\":\"Business City\",\"title\":\"4.5\",\"paragraph\":\"test test  test testtest testtest testtest testtest test\",\"rank\":\"3\",\"img\":\"325429.jpg\"}', '0', '0', 'added portfolio:', '::1', '2019-11-15 20:05:41'),
(24, 1, 'Create', 17, 'portfolio', '27', 0, '0', '{\"meta\":\"portfolio\",\"header\":\"test test  test testtest testtest testtest testtest test\",\"title\":\"1\",\"paragraph\":\"test test  test testtest testtest testtest testtest test\",\"rank\":\"5\",\"img\":false}', '0', '0', 'added portfolio:', '::1', '2019-11-15 20:05:59'),
(25, 1, 'Delete', 17, 'portfolio', '27', 0, '{\"id\":\"27\",\"header\":\"test test  test testtest testtest testtest testtest test\",\"title\":\"1\",\"paragraph\":\"test test  test testtest testtest testtest testtest test\",\"points\":\"\",\"meta\":\"portfolio\",\"img\":\"0\",\"link\":\"\",\"rank\":\"5\"}', '0', '0', '0', 'Deleted portfolio:27', '::1', '2019-11-15 20:06:22'),
(26, 1, 'update', 17, 'portfolio', '25', 0, '{\"id\":\"25\",\"header\":\"Business City\",\"title\":\"3.5\",\"paragraph\":\"test test  test test\",\"points\":\"\",\"meta\":\"portfolio\",\"img\":\"129556.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"25\",\"meta\":\"portfolio\",\"header\":\"Business City\",\"title\":\"3.5\",\"paragraph\":\"test test  test test\",\"rank\":\"1\",\"img\":\"scuba-diving-4000x3000-ocean-underwater-4k-14792.jpg\"}', '0', '0', 'updated portfolio: 25', '::1', '2019-11-15 20:16:22'),
(27, 1, 'update', 17, 'portfolio', '25', 0, '{\"id\":\"25\",\"header\":\"Business City\",\"title\":\"3.5\",\"paragraph\":\"test test  test test\",\"points\":\"\",\"meta\":\"portfolio\",\"img\":\"scuba-diving-4000x3000-ocean-underwater-4k-14792.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"25\",\"meta\":\"portfolio\",\"header\":\"Business City\",\"title\":\"3.5\",\"paragraph\":\"Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\",\"rank\":\"1\"}', '0', '0', 'updated portfolio: 25', '::1', '2019-11-15 20:16:50'),
(28, 1, 'update', 17, 'portfolio', '26', 0, '{\"id\":\"26\",\"header\":\"Business City\",\"title\":\"4.5\",\"paragraph\":\"test test  test testtest testtest testtest testtest test\",\"points\":\"\",\"meta\":\"portfolio\",\"img\":\"325429.jpg\",\"link\":\"\",\"rank\":\"3\"}', '{\"id\":\"26\",\"meta\":\"portfolio\",\"header\":\"Business City\",\"title\":\"4.5\",\"paragraph\":\"Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\",\"rank\":\"3\"}', '0', '0', 'updated portfolio: 26', '::1', '2019-11-15 20:16:51'),
(29, 1, 'update', 17, 'portfolio', '25', 0, '{\"id\":\"25\",\"header\":\"Business City\",\"title\":\"3.5\",\"paragraph\":\"Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\",\"points\":\"\",\"meta\":\"portfolio\",\"img\":\"scuba-diving-4000x3000-ocean-underwater-4k-14792.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"25\",\"meta\":\"portfolio\",\"header\":\"Business City\",\"title\":\"3.5\",\"paragraph\":\"Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\",\"rank\":\"1\",\"img\":\"cabo-pulmo-diving-11.jpg\"}', '0', '0', 'updated portfolio: 25', '::1', '2019-11-15 20:25:17'),
(30, 1, 'update', 17, 'portfolio', '26', 0, '{\"id\":\"26\",\"header\":\"Business City\",\"title\":\"4.5\",\"paragraph\":\"Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\",\"points\":\"\",\"meta\":\"portfolio\",\"img\":\"325429.jpg\",\"link\":\"\",\"rank\":\"3\"}', '{\"id\":\"26\",\"meta\":\"portfolio\",\"header\":\"Business City\",\"title\":\"4.5\",\"paragraph\":\"Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\",\"rank\":\"3\",\"img\":\"Discover-Scuba-Diving1.jpg\"}', '0', '0', 'updated portfolio: 26', '::1', '2019-11-15 20:25:17'),
(31, 1, 'Create', 17, 'portfolio', '28', 0, '0', '{\"meta\":\"portfolio\",\"header\":\"Business City\",\"title\":\"5\",\"paragraph\":\"Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\",\"rank\":\"2\",\"img\":\"scuba-diving2-960x11491.jpg\"}', '0', '0', 'added portfolio:', '::1', '2019-11-15 20:26:02'),
(32, 1, 'Create', 23, 'team', '29', 0, '0', '{\"meta\":\"team\",\"header\":\"Julien Miro\",\"title\":\"Software Developer\",\"rank\":\"1\",\"img\":\"11.jpg\"}', '0', '0', 'added team:', '::1', '2019-11-15 21:31:33'),
(33, 1, 'Create', 23, 'team', '30', 0, '0', '{\"meta\":\"team\",\"header\":\"Joan Williams\",\"title\":\"Support Operator\",\"rank\":\"2\",\"img\":\"21.jpg\"}', '0', '0', 'added team:', '::1', '2019-11-15 21:40:13'),
(34, 1, 'Create', 23, 'team', '31', 0, '0', '{\"meta\":\"team\",\"header\":\"Joan Williams\",\"title\":\"Support Operator\",\"rank\":\"3\",\"img\":false}', '0', '0', 'added team:', '::1', '2019-11-15 21:40:28'),
(35, 1, 'Delete', 23, 'team', '31', 0, '{\"id\":\"31\",\"header\":\"Joan Williams\",\"title\":\"Support Operator\",\"paragraph\":\"\",\"points\":\"\",\"meta\":\"team\",\"img\":\"0\",\"link\":\"\",\"rank\":\"3\"}', '0', '0', '0', 'Deleted team:31', '::1', '2019-11-15 21:40:33'),
(36, 1, 'Create', 23, 'team', '32', 0, '0', '{\"meta\":\"team\",\"header\":\"Benedict Smith\",\"title\":\"Creative Director\",\"rank\":\"3\",\"img\":\"31.jpg\"}', '0', '0', 'added team:', '::1', '2019-11-15 21:41:12'),
(37, 1, 'Create', 23, 'team', '33', 0, '0', '{\"meta\":\"team\",\"header\":\"Madlen Green\",\"title\":\"Sales Manager\",\"rank\":\"4\",\"img\":\"41.jpg\"}', '0', '0', 'added team:', '::1', '2019-11-15 21:41:42'),
(38, 1, 'Create', 23, 'team', '34', 0, '0', '{\"meta\":\"team\",\"header\":\"Julien Miro\",\"title\":\"Julien Miro\",\"rank\":\"5\",\"img\":\"Discover-Scuba-Diving.jpg\"}', '0', '0', 'added team:', '::1', '2019-11-15 21:46:45'),
(39, 1, 'update', 24, 'language', '1', 0, '{\"id\":\"1\",\"english\":\"insert test phrase\",\"german\":null,\"french\":null,\"arabic\":null,\"updated_at\":\"\",\"timestamp\":\"2019-11-20 23:11:19\"}', '{\"english\":\"insert test phrase\",\"german\":\"test\",\"french\":\"\",\"arabic\":\"\"}', '0', '0', 'updated language: 1', '::1', '2019-11-20 21:32:04'),
(40, 1, 'update', 24, 'language', '1', 0, '{\"id\":\"1\",\"english\":\"insert test phrase\",\"german\":\"test\",\"french\":\"\",\"arabic\":\"\",\"updated_at\":\"\",\"timestamp\":\"2019-11-20 23:11:19\"}', '{\"english\":\"insert test phrase\",\"german\":\"test\",\"french\":\"test2\",\"arabic\":\"\"}', '0', '0', 'updated language: 1', '::1', '2019-11-20 21:33:05'),
(41, 1, 'update', 24, 'Offers', '23', 0, '{\"id\":\"23\",\"activity_id\":\"2\",\"hotel_id\":\"5\",\"sub_id\":\"5\",\"name\":\"Test 22\",\"data\":\"Test Test Test Test Test\",\"image\":\"\",\"price\":\"2000\",\"special\":\"1\",\"deleted\":\"0\",\"timestamp\":\"2019-11-02 04:30:21\",\"activity_name\":\"Diving\",\"hotel_name\":\"SUNRISE Royal Makadi Aqua Resort\",\"sub_name\":\"Speciality\"}', '{\"id\":\"23\",\"activity_id\":\"2\",\"hotel_id\":\"5\",\"sub_id\":\"5\",\"name\":\"Test 22\",\"data\":\"Test Test Test Test Test44\",\"price\":\"2000\",\"special\":\"1\"}', '0', '0', 'updated offer: Test 22', '::1', '2020-01-02 14:00:47'),
(42, 1, 'Create', 24, 'Offers', '24', 0, '0', '{\"activity_id\":\"1\",\"hotel_id\":\"4\",\"sub_id\":\"6\",\"name\":\"test 23\",\"data\":\"Test Test Test Test Test55\",\"price\":\"10000\",\"special\":\"1\"}', '0', '0', 'added offer:test 23', '::1', '2020-01-02 14:01:53'),
(43, 1, 'Delete', 24, 'Offers', '23', 0, '{\"id\":\"23\",\"activity_id\":\"2\",\"hotel_id\":\"5\",\"sub_id\":\"5\",\"name\":\"Test 22\",\"data\":\"Test Test Test Test Test44\",\"image\":\"\",\"price\":\"2000\",\"special\":\"1\",\"deleted\":\"0\",\"timestamp\":\"2019-11-02 04:30:21\",\"activity_name\":\"Diving\",\"hotel_name\":\"SUNRISE Royal Makadi Aqua Resort\",\"sub_name\":\"Speciality\"}', '0', '0', '0', 'Deleted offer:Test 22', '::1', '2020-01-02 14:02:04'),
(44, 1, 'update', 24, 'Offers', '1', 0, '{\"id\":\"1\",\"activity_id\":\"1\",\"hotel_id\":\"1\",\"sub_id\":\"6\",\"name\":\"Test\",\"data\":\"Test Test Test Test Test\",\"image\":\"104x84.png\",\"price\":\"2000\",\"special\":\"0\",\"deleted\":\"0\",\"timestamp\":\"2019-11-01 19:30:21\",\"activity_name\":\"Aqua Center\",\"hotel_name\":\"Pharaoh Azur Beach Resort\",\"sub_name\":\"Aqua Center\"}', '{\"id\":\"1\",\"activity_id\":\"1\",\"hotel_id\":\"1\",\"sub_id\":\"6\",\"name\":\"Test\",\"data\":\"Test Test Test Test Test\",\"price\":\"2000\",\"special\":\"0\",\"image\":\"21.jpg\"}', '0', '0', 'updated offer: Test', '41.65.208.30', '2020-01-03 15:45:44'),
(45, 1, 'update', 24, 'Offers', '3', 0, '{\"id\":\"3\",\"activity_id\":\"2\",\"hotel_id\":\"1\",\"sub_id\":\"1\",\"name\":\"Test 2\",\"data\":\"Test Test Test Test Test\",\"image\":\"104x84.png\",\"price\":\"2000\",\"special\":\"0\",\"deleted\":\"0\",\"timestamp\":\"2019-11-01 19:30:21\",\"activity_name\":\"Diving\",\"hotel_name\":\"Pharaoh Azur Beach Resort\",\"sub_name\":\"Diving\"}', '{\"id\":\"3\",\"activity_id\":\"2\",\"hotel_id\":\"1\",\"sub_id\":\"1\",\"name\":\"Test 2\",\"data\":\"Test Test Test Test Test\",\"price\":\"2000\",\"special\":\"0\",\"image\":\"13731617_980013822111647_805224201259891546_n.jpg\"}', '0', '0', 'updated offer: Test 2', '41.65.208.30', '2020-01-03 15:46:00'),
(46, 1, 'update', 24, 'Offers', '4', 0, '{\"id\":\"4\",\"activity_id\":\"2\",\"hotel_id\":\"2\",\"sub_id\":\"1\",\"name\":\"Test 3\",\"data\":\"Test Test Test Test Test\",\"image\":\"104x84.png\",\"price\":\"2000\",\"special\":\"0\",\"deleted\":\"0\",\"timestamp\":\"2019-11-01 19:30:21\",\"activity_name\":\"Diving\",\"hotel_name\":\"SUNRISE Garden Beach Resort -Select-\",\"sub_name\":\"Diving\"}', '{\"id\":\"4\",\"activity_id\":\"2\",\"hotel_id\":\"2\",\"sub_id\":\"1\",\"name\":\"Test 3\",\"data\":\"Test Test Test Test Test\",\"price\":\"2000\",\"special\":\"0\",\"image\":\"IMG_3782.JPG\"}', '0', '0', 'updated offer: Test 3', '41.65.208.30', '2020-01-03 15:46:04'),
(47, 1, 'Status Change', 26, 'Bookings', '0', 0, '0', '0', '0', '0', 'Changed booking status:', '41.65.208.30', '2020-01-10 09:09:02'),
(48, 1, 'Status Change', 26, 'Bookings', '0', 0, '0', '0', '0', '0', 'Changed booking status:', '41.65.208.30', '2020-01-10 09:09:07'),
(49, 1, 'Status Change', 26, 'Bookings', '0', 0, '0', '0', '0', '0', 'Changed booking status:', '41.65.208.30', '2020-01-10 09:09:12'),
(50, 1, 'Status Change', 26, 'Bookings', '0', 0, '0', '0', '0', '0', 'Changed booking status:', '41.65.208.30', '2020-01-10 09:09:47'),
(51, 1, 'Status Change', 26, 'Bookings', '0', 0, '0', '0', '0', '0', 'Changed booking status:', '41.65.208.30', '2020-01-10 09:10:16'),
(52, 1, 'Status Change', 26, 'Bookings', '0', 0, '0', '0', '0', '0', 'Changed booking status:', '41.65.208.30', '2020-01-10 09:17:35'),
(53, 1, 'Status Change', 26, 'Bookings', '0', 0, '0', '0', '0', '0', 'Changed booking status:', '41.65.208.30', '2020-01-10 09:17:44'),
(54, 1, 'Status Change', 26, 'Bookings', '0', 0, '0', '0', '0', '0', 'Changed booking status:0', '41.65.208.30', '2020-01-10 09:20:20'),
(55, 1, 'Status Change', 26, 'Bookings', '3', 0, '0', '0', '0', '0', 'Changed booking status:3', '41.65.208.30', '2020-01-10 09:26:24'),
(56, 1, 'Status Change', 26, 'Bookings', '3', 0, '0', '0', '0', '0', 'Changed booking status:3', '41.65.208.30', '2020-01-10 09:26:27'),
(57, 1, 'Status Change', 26, 'Bookings', '1', 0, '0', '0', '0', '0', 'Changed booking status:1', '41.65.208.30', '2020-01-10 09:26:32'),
(58, 1, 'update', 22, 'gallery', '16', 0, '{\"id\":\"16\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"trip_1\",\"meta\":\"gallery\",\"img\":\"3_archeologygallery_nationalgeographic_17407091.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"16\",\"meta\":\"gallery\",\"header\":\"Business City\",\"title\":\"Web Development\",\"points\":\"trip_1\",\"rank\":\"1\",\"img\":\"21.jpg\"}', '0', '0', 'updated gallery: 16', '41.65.208.30', '2020-01-12 15:12:48'),
(59, 1, 'update', 22, 'gallery', '17', 0, '{\"id\":\"17\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"trip_1\",\"meta\":\"gallery\",\"img\":\"1295561.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"17\",\"meta\":\"gallery\",\"header\":\"Business City\",\"title\":\"Web Development\",\"points\":\"trip_1\",\"rank\":\"1\",\"img\":\"13620906_976161869163509_1200008273719622505_n.jpg\"}', '0', '0', 'updated gallery: 17', '41.65.208.30', '2020-01-12 15:12:48'),
(60, 1, 'update', 22, 'gallery', '18', 0, '{\"id\":\"18\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"trip_2\",\"meta\":\"gallery\",\"img\":\"3254291.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"18\",\"meta\":\"gallery\",\"header\":\"Business City\",\"title\":\"Web Development\",\"points\":\"trip_2\",\"rank\":\"1\",\"img\":\"13731617_980013822111647_805224201259891546_n.jpg\"}', '0', '0', 'updated gallery: 18', '41.65.208.30', '2020-01-12 15:12:48'),
(61, 1, 'update', 22, 'gallery', '19', 0, '{\"id\":\"19\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"trip_3\",\"meta\":\"gallery\",\"img\":\"3254591.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"19\",\"meta\":\"gallery\",\"header\":\"Business City\",\"title\":\"Web Development\",\"points\":\"trip_3\",\"rank\":\"1\",\"img\":\"14102607_1012532422193120_5040987907399577544_n.jpg\"}', '0', '0', 'updated gallery: 19', '41.65.208.30', '2020-01-12 15:12:48'),
(62, 1, 'update', 22, 'gallery', '20', 0, '{\"id\":\"20\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"trip_4\",\"meta\":\"gallery\",\"img\":\"9513261.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"20\",\"meta\":\"gallery\",\"header\":\"Business City\",\"title\":\"Web Development\",\"points\":\"trip_4\",\"rank\":\"1\",\"img\":\"IMG_0027.JPG\"}', '0', '0', 'updated gallery: 20', '41.65.208.30', '2020-01-12 15:12:48'),
(63, 1, 'update', 22, 'gallery', '21', 0, '{\"id\":\"21\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"trip_5\",\"meta\":\"gallery\",\"img\":\"Cenote-Taj-Mahal-3-of-51.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"21\",\"meta\":\"gallery\",\"header\":\"Business City\",\"title\":\"Web Development\",\"points\":\"trip_5\",\"rank\":\"1\",\"img\":\"IMG_0174.JPG\"}', '0', '0', 'updated gallery: 21', '41.65.208.30', '2020-01-12 15:12:51'),
(64, 1, 'update', 22, 'gallery', '16', 0, '{\"id\":\"16\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"trip_1\",\"meta\":\"gallery\",\"img\":\"21.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"16\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Trip\",\"rank\":\"1\"}', '0', '0', 'updated gallery: 16', '41.65.208.30', '2020-01-12 15:15:58'),
(65, 1, 'update', 22, 'gallery', '17', 0, '{\"id\":\"17\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"trip_1\",\"meta\":\"gallery\",\"img\":\"13620906_976161869163509_1200008273719622505_n.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"17\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Trip\",\"rank\":\"2\"}', '0', '0', 'updated gallery: 17', '41.65.208.30', '2020-01-12 15:15:58'),
(66, 1, 'update', 22, 'gallery', '18', 0, '{\"id\":\"18\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"trip_2\",\"meta\":\"gallery\",\"img\":\"13731617_980013822111647_805224201259891546_n.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"18\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Diving_Trip\",\"rank\":\"1\"}', '0', '0', 'updated gallery: 18', '41.65.208.30', '2020-01-12 15:15:58'),
(67, 1, 'update', 22, 'gallery', '19', 0, '{\"id\":\"19\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"trip_3\",\"meta\":\"gallery\",\"img\":\"14102607_1012532422193120_5040987907399577544_n.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"19\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Diving_Trip\",\"rank\":\"2\"}', '0', '0', 'updated gallery: 19', '41.65.208.30', '2020-01-12 15:15:58'),
(68, 1, 'update', 22, 'gallery', '20', 0, '{\"id\":\"20\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"trip_4\",\"meta\":\"gallery\",\"img\":\"IMG_0027.JPG\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"20\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Diving_Trip\",\"rank\":\"3\"}', '0', '0', 'updated gallery: 20', '41.65.208.30', '2020-01-12 15:15:58'),
(69, 1, 'update', 22, 'gallery', '21', 0, '{\"id\":\"21\",\"header\":\"Business City\",\"title\":\"Web Development\",\"paragraph\":\"\",\"points\":\"trip_5\",\"meta\":\"gallery\",\"img\":\"IMG_0174.JPG\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"21\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Diving_Trip\",\"rank\":\"4\"}', '0', '0', 'updated gallery: 21', '41.65.208.30', '2020-01-12 15:15:58'),
(70, 1, 'Create', 22, 'gallery', '38', 0, '0', '{\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Diving Trip\",\"rank\":\"5\",\"img\":\"IMG_0179.JPG\"}', '0', '0', 'added gallery:', '41.65.208.30', '2020-01-12 15:15:58'),
(71, 1, 'Create', 22, 'gallery', '39', 0, '0', '{\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Diving Trip\",\"rank\":\"6\",\"img\":\"IMG_0216.JPG\"}', '0', '0', 'added gallery:', '41.65.208.30', '2020-01-12 15:15:58'),
(72, 1, 'Create', 22, 'gallery', '40', 0, '0', '{\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Sea Trip\",\"rank\":\"1\",\"img\":\"IMG_1324.JPG\"}', '0', '0', 'added gallery:', '41.65.208.30', '2020-01-12 15:16:02'),
(73, 1, 'Create', 22, 'gallery', '41', 0, '0', '{\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Sea Trip\",\"rank\":\"2\",\"img\":\"IMG_1417.JPG\"}', '0', '0', 'added gallery:', '41.65.208.30', '2020-01-12 15:16:02'),
(74, 1, 'Create', 22, 'gallery', '42', 0, '0', '{\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Sea Trip\",\"rank\":\"3\",\"img\":\"IMG_1624.JPG\"}', '0', '0', 'added gallery:', '41.65.208.30', '2020-01-12 15:16:06'),
(75, 1, 'update', 22, 'gallery', '17', 0, '{\"id\":\"17\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Trip\",\"meta\":\"gallery\",\"img\":\"13620906_976161869163509_1200008273719622505_n.jpg\",\"link\":\"\",\"rank\":\"2\"}', '{\"id\":\"17\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Sea_Trip\",\"rank\":\"1\"}', '0', '0', 'updated gallery: 17', '41.65.208.30', '2020-01-12 15:17:55'),
(76, 1, 'update', 22, 'gallery', '38', 0, '{\"id\":\"38\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Diving Trip\",\"meta\":\"gallery\",\"img\":\"IMG_0179.JPG\",\"link\":\"\",\"rank\":\"5\"}', '{\"id\":\"38\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Diving_Trip\",\"rank\":\"5\"}', '0', '0', 'updated gallery: 38', '41.65.208.30', '2020-01-12 15:17:55'),
(77, 1, 'update', 22, 'gallery', '39', 0, '{\"id\":\"39\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Diving Trip\",\"meta\":\"gallery\",\"img\":\"IMG_0216.JPG\",\"link\":\"\",\"rank\":\"6\"}', '{\"id\":\"39\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Diving_Trip\",\"rank\":\"6\"}', '0', '0', 'updated gallery: 39', '41.65.208.30', '2020-01-12 15:17:55'),
(78, 1, 'update', 22, 'gallery', '40', 0, '{\"id\":\"40\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Sea Trip\",\"meta\":\"gallery\",\"img\":\"IMG_1324.JPG\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"40\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Diving_Trip\",\"rank\":\"7\"}', '0', '0', 'updated gallery: 40', '41.65.208.30', '2020-01-12 15:17:55'),
(79, 1, 'update', 22, 'gallery', '41', 0, '{\"id\":\"41\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Sea Trip\",\"meta\":\"gallery\",\"img\":\"IMG_1417.JPG\",\"link\":\"\",\"rank\":\"2\"}', '{\"id\":\"41\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Diving_Trip\",\"rank\":\"8\"}', '0', '0', 'updated gallery: 41', '41.65.208.30', '2020-01-12 15:17:55'),
(80, 1, 'update', 22, 'gallery', '42', 0, '{\"id\":\"42\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Sea Trip\",\"meta\":\"gallery\",\"img\":\"IMG_1624.JPG\",\"link\":\"\",\"rank\":\"3\"}', '{\"id\":\"42\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Sea_Trip\",\"rank\":\"2\"}', '0', '0', 'updated gallery: 42', '41.65.208.30', '2020-01-12 15:17:55'),
(81, 1, 'Create', 22, 'gallery', '43', 0, '0', '{\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Sea Trip\",\"rank\":\"3\",\"img\":\"IMG_1629.JPG\"}', '0', '0', 'added gallery:', '41.65.208.30', '2020-01-13 08:47:50'),
(82, 1, 'update', 22, 'gallery', '43', 0, '{\"id\":\"43\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Sea Trip\",\"meta\":\"gallery\",\"img\":\"IMG_1629.JPG\",\"link\":\"\",\"rank\":\"3\"}', '{\"id\":\"43\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Sea_Trip\",\"rank\":\"3\"}', '0', '0', 'updated gallery: 43', '41.65.208.30', '2020-01-13 08:49:14'),
(83, 1, 'Create', 22, 'gallery', '44', 0, '0', '{\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Sea Trip\",\"rank\":\"4\",\"img\":\"IMG_1632.JPG\"}', '0', '0', 'added gallery:', '41.65.208.30', '2020-01-13 08:49:17'),
(84, 1, 'Create', 22, 'gallery', '45', 0, '0', '{\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Diving Trip\",\"rank\":\"9\",\"img\":\"IMG_1924.JPG\"}', '0', '0', 'added gallery:', '41.65.208.30', '2020-01-13 08:49:20'),
(85, 1, 'update', 22, 'gallery', '16', 0, '{\"id\":\"16\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Trip\",\"meta\":\"gallery\",\"img\":\"21.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"16\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Trips\",\"rank\":\"1\"}', '0', '0', 'updated gallery: 16', '41.65.208.30', '2020-01-13 08:49:26'),
(86, 1, 'update', 22, 'gallery', '44', 0, '{\"id\":\"44\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Sea Trip\",\"meta\":\"gallery\",\"img\":\"IMG_1632.JPG\",\"link\":\"\",\"rank\":\"4\"}', '{\"id\":\"44\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Sea_Trip\",\"rank\":\"4\"}', '0', '0', 'updated gallery: 44', '41.65.208.30', '2020-01-13 08:49:26'),
(87, 1, 'update', 22, 'gallery', '45', 0, '{\"id\":\"45\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Diving Trip\",\"meta\":\"gallery\",\"img\":\"IMG_1924.JPG\",\"link\":\"\",\"rank\":\"9\"}', '{\"id\":\"45\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Diving_Trip\",\"rank\":\"9\"}', '0', '0', 'updated gallery: 45', '41.65.208.30', '2020-01-13 08:49:26'),
(88, 1, 'update', 19, 'contact', '8', 0, '{\"id\":\"8\",\"header\":\"Call\",\"title\":\"+1 5589 55488 55\",\"paragraph\":\"Monday-Friday (9am-5pm)\",\"points\":\"\",\"meta\":\"contact_us\",\"img\":\"fa fa-mobile\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"8\",\"meta\":\"contact_us\",\"header\":\"Call\",\"title\":\"01002741794\",\"paragraph\":\"9am-5pm\"}', '0', '0', 'updated contact: Call', '41.65.208.30', '2020-01-13 16:29:42'),
(89, 1, 'update', 19, 'contact', '9', 0, '{\"id\":\"9\",\"header\":\"Email\",\"title\":\"info@example.com\",\"paragraph\":\"Web: www.example.com\",\"points\":\"\",\"meta\":\"contact_us\",\"img\":\"fa fa-envelope-o\",\"link\":\"\",\"rank\":\"2\"}', '{\"id\":\"9\",\"meta\":\"contact_us\",\"header\":\"Email\",\"title\":\"info@divemore-group.com\",\"paragraph\":\"Web: divemore-group.com\"}', '0', '0', 'updated contact: Email', '41.65.208.30', '2020-01-13 16:29:42'),
(90, 1, 'update', 19, 'contact', '10', 0, '{\"id\":\"10\",\"header\":\"Location\",\"title\":\"A108 Adam Street\",\"paragraph\":\"NY 535022, USA\",\"points\":\"\",\"meta\":\"contact_us\",\"img\":\"fa fa-map-marker\",\"link\":\"\",\"rank\":\"3\"}', '{\"id\":\"10\",\"meta\":\"contact_us\",\"header\":\"Location\",\"title\":\"Sheraton Street, Hurghada, Egypt\",\"paragraph\":\"Sheraton Street, Hurghada, Egypt\"}', '0', '0', 'updated contact: Location', '41.65.208.30', '2020-01-13 16:29:42'),
(91, 1, 'update', 20, 'Footer', '13', 0, '{\"id\":\"13\",\"header\":\"Tel\",\"title\":\"+123 456 789\",\"paragraph\":\"\",\"points\":\"\",\"meta\":\"footer_tel\",\"img\":\"\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"13\",\"meta\":\"footer_tel\",\"title\":\"01002741794\"}', '0', '0', 'updated footer data: 13', '41.65.208.30', '2020-01-13 16:48:57'),
(92, 1, 'update', 20, 'Footer', '14', 0, '{\"id\":\"14\",\"header\":\"Email\",\"title\":\"contact@example.com\",\"paragraph\":\"\",\"points\":\"\",\"meta\":\"footer_email\",\"img\":\"\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"14\",\"meta\":\"footer_email\",\"title\":\"info@divemore-group.com\"}', '0', '0', 'updated footer data: 14', '41.65.208.30', '2020-01-13 16:48:57'),
(93, 1, 'update', 25, 'language', '7', 0, '{\"id\":\"7\",\"english\":\" \",\"german\":null,\"french\":null,\"arabic\":null,\"updated_at\":\"\",\"timestamp\":\"2020-01-13 10:00:35\"}', '{\"english\":\" Home\",\"german\":\"\",\"french\":\"\",\"arabic\":\"المقدمة\"}', '0', '0', 'updated language: 7', '41.65.208.30', '2020-01-13 17:01:07'),
(94, 1, 'update', 25, 'language', '8', 0, '{\"id\":\"8\",\"english\":\" \",\"german\":null,\"french\":null,\"arabic\":null,\"updated_at\":\"\",\"timestamp\":\"2020-01-13 10:01:12\"}', '{\"english\":\" Portfolio\",\"german\":\"\",\"french\":\"\",\"arabic\":\"سابقة اعمال\"}', '0', '0', 'updated language: 8', '41.65.208.30', '2020-01-13 17:01:37'),
(95, 1, 'update', 13, 'slider', '7', 0, '{\"id\":\"7\",\"img\":\"slider7.jpg\",\"first_title\":\"The Best Business Information\",\"second_title\":\"We\'re In The Business Of Get Quality Business Service\",\"dir\":\"one\",\"rank\":\"1\"}', '{\"id\":\"7\",\"first_title\":\"The Best Business Information\",\"second_title\":\"We\'re In The Business Of Get Quality Business Service\",\"rank\":\"1\",\"img\":\"3_archeologygallery_nationalgeographic_1740709.jpg\"}', '0', '0', 'updated slider: 7', '::1', '2020-01-15 07:55:52'),
(96, 1, 'update', 13, 'slider', '5', 0, '{\"id\":\"5\",\"img\":\"slider5.jpg\",\"first_title\":\"The Best Business Information\",\"second_title\":\"We\'re In The Business Of Get Quality Business Service\",\"dir\":\"two\",\"rank\":\"2\"}', '{\"id\":\"5\",\"first_title\":\"The Best Business Information\",\"second_title\":\"We\'re In The Business Of Get Quality Business Service\",\"rank\":\"2\",\"img\":\"129556.jpg\"}', '0', '0', 'updated slider: 5', '::1', '2020-01-15 07:55:52'),
(97, 1, 'update', 13, 'slider', '1', 0, '{\"id\":\"1\",\"img\":\"slider1.jpg\",\"first_title\":\"The Best Business Information \",\"second_title\":\"We\'re In The Business Of Helping You Start Your Business\",\"dir\":\"two\",\"rank\":\"3\"}', '{\"id\":\"1\",\"first_title\":\"The Best Business Information \",\"second_title\":\"We\'re In The Business Of Helping You Start Your Business\",\"rank\":\"3\",\"img\":\"325429.jpg\"}', '0', '0', 'updated slider: 1', '::1', '2020-01-15 07:55:52'),
(98, 1, 'update', 13, 'slider', '7', 0, '{\"id\":\"7\",\"img\":\"3_archeologygallery_nationalgeographic_1740709.jpg\",\"first_title\":\"The Best Business Information\",\"second_title\":\"We\'re In The Business Of Get Quality Business Service\",\"dir\":\"one\",\"rank\":\"1\"}', '{\"id\":\"7\",\"first_title\":\"The Best Business Information\",\"second_title\":\"We\'re In The Business Of Get Quality Business Service\",\"rank\":\"1\",\"img\":\"3_archeologygallery_nationalgeographic_17407092.jpg\"}', '0', '0', 'updated slider: 7', '::1', '2020-01-15 08:01:45'),
(99, 1, 'Delete', 13, 'slider', '9', 0, '{\"id\":\"9\",\"img\":\"slider21.jpg\",\"first_title\":\"The Best Business Information\",\"second_title\":\"We\'re In The Business Of Get Quality Business Service\",\"dir\":\"two\",\"rank\":\"4\"}', '0', '0', '0', 'Deleted slider:9', '::1', '2020-01-15 08:07:26'),
(100, 1, 'Delete', 13, 'slider', '10', 0, '{\"id\":\"10\",\"img\":\"slider22.jpg\",\"first_title\":\"The Best Business Information\",\"second_title\":\"We\'re In The Business Of Get Quality Business Service\",\"dir\":\"two\",\"rank\":\"4\"}', '0', '0', '0', 'Deleted slider:10', '::1', '2020-01-15 08:07:28'),
(101, 1, 'Delete', 13, 'slider', '11', 0, '{\"id\":\"11\",\"img\":\"slider23.jpg\",\"first_title\":\"The Best Business Information\",\"second_title\":\"We\'re In The Business Of Get Quality Business Service\",\"dir\":\"two\",\"rank\":\"4\"}', '0', '0', '0', 'Deleted slider:11', '::1', '2020-01-15 08:07:32'),
(102, 1, 'update', 13, 'slider', '7', 0, '{\"id\":\"7\",\"img\":\"3_archeologygallery_nationalgeographic_17407092.jpg\",\"first_title\":\"The Best Business Information\",\"second_title\":\"We\'re In The Business Of Get Quality Business Service\",\"dir\":\"one\",\"rank\":\"1\"}', '{\"id\":\"7\",\"first_title\":\"The Best Business Information\",\"second_title\":\"We\'re In The Business Of Get Quality Business Service\",\"rank\":\"1\",\"img\":\"3_archeologygallery_nationalgeographic_1740709.jpg\"}', '0', '0', 'updated slider: 7', '::1', '2020-01-15 08:07:33'),
(103, 1, 'update', 22, 'gallery', '16', 0, '{\"id\":\"16\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Trips\",\"meta\":\"gallery\",\"img\":\"21.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"16\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Trips\",\"rank\":\"1\",\"img\":\"3254592.jpg\"}', '0', '0', 'updated gallery: 16', '::1', '2020-01-15 09:00:33'),
(104, 1, 'update', 22, 'gallery', '17', 0, '{\"id\":\"17\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Sea_Trip\",\"meta\":\"gallery\",\"img\":\"13620906_976161869163509_1200008273719622505_n.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"17\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Sea_Trip\",\"rank\":\"1\",\"img\":\"3254292.jpg\"}', '0', '0', 'updated gallery: 17', '::1', '2020-01-16 08:05:43'),
(105, 1, 'update', 22, 'gallery', '18', 0, '{\"id\":\"18\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Diving_Trip\",\"meta\":\"gallery\",\"img\":\"13731617_980013822111647_805224201259891546_n.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"18\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Diving_Trip\",\"rank\":\"1\",\"img\":\"1295562.jpg\"}', '0', '0', 'updated gallery: 18', '::1', '2020-01-16 08:05:43'),
(106, 1, 'update', 22, 'gallery', '19', 0, '{\"id\":\"19\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Diving_Trip\",\"meta\":\"gallery\",\"img\":\"14102607_1012532422193120_5040987907399577544_n.jpg\",\"link\":\"\",\"rank\":\"2\"}', '{\"id\":\"19\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Diving_Trip\",\"rank\":\"2\",\"img\":\"9513262.jpg\"}', '0', '0', 'updated gallery: 19', '::1', '2020-01-16 08:05:45'),
(107, 1, 'update', 22, 'gallery', '20', 0, '{\"id\":\"20\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Diving_Trip\",\"meta\":\"gallery\",\"img\":\"IMG_0027.JPG\",\"link\":\"\",\"rank\":\"3\"}', '{\"id\":\"20\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Diving_Trip\",\"rank\":\"3\",\"img\":\"Cenote-Taj-Mahal-3-of-52.jpg\"}', '0', '0', 'updated gallery: 20', '::1', '2020-01-16 08:05:46'),
(108, 1, 'Delete', 22, 'gallery', '21', 0, '{\"id\":\"21\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Diving_Trip\",\"meta\":\"gallery\",\"img\":\"IMG_0174.JPG\",\"link\":\"\",\"rank\":\"4\"}', '0', '0', '0', 'Deleted gallery:21', '::1', '2020-01-16 08:05:56'),
(109, 1, 'Delete', 22, 'gallery', '38', 0, '{\"id\":\"38\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Diving_Trip\",\"meta\":\"gallery\",\"img\":\"IMG_0179.JPG\",\"link\":\"\",\"rank\":\"5\"}', '0', '0', '0', 'Deleted gallery:38', '::1', '2020-01-16 08:06:01'),
(110, 1, 'Delete', 22, 'gallery', '39', 0, '{\"id\":\"39\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Diving_Trip\",\"meta\":\"gallery\",\"img\":\"IMG_0216.JPG\",\"link\":\"\",\"rank\":\"6\"}', '0', '0', '0', 'Deleted gallery:39', '::1', '2020-01-16 08:06:05'),
(111, 1, 'Delete', 22, 'gallery', '40', 0, '{\"id\":\"40\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Diving_Trip\",\"meta\":\"gallery\",\"img\":\"IMG_1324.JPG\",\"link\":\"\",\"rank\":\"7\"}', '0', '0', '0', 'Deleted gallery:40', '::1', '2020-01-16 08:06:07'),
(112, 1, 'Delete', 22, 'gallery', '41', 0, '{\"id\":\"41\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Diving_Trip\",\"meta\":\"gallery\",\"img\":\"IMG_1417.JPG\",\"link\":\"\",\"rank\":\"8\"}', '0', '0', '0', 'Deleted gallery:41', '::1', '2020-01-16 08:06:10'),
(113, 1, 'Delete', 22, 'gallery', '42', 0, '{\"id\":\"42\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Sea_Trip\",\"meta\":\"gallery\",\"img\":\"IMG_1624.JPG\",\"link\":\"\",\"rank\":\"2\"}', '0', '0', '0', 'Deleted gallery:42', '::1', '2020-01-16 08:06:12'),
(114, 1, 'Delete', 22, 'gallery', '43', 0, '{\"id\":\"43\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Sea_Trip\",\"meta\":\"gallery\",\"img\":\"IMG_1629.JPG\",\"link\":\"\",\"rank\":\"3\"}', '0', '0', '0', 'Deleted gallery:43', '::1', '2020-01-16 08:06:17'),
(115, 1, 'update', 13, 'slider', '7', 0, '{\"id\":\"7\",\"img\":\"3_archeologygallery_nationalgeographic_1740709.jpg\",\"first_title\":\"The Best Business Information\",\"second_title\":\"We\'re In The Business Of Get Quality Business Service\",\"dir\":\"one\",\"rank\":\"1\"}', '{\"id\":\"7\",\"first_title\":\"The Best Business Information\",\"second_title\":\"We\'re In The Business Of Get Quality Business Service\",\"rank\":\"1\",\"img\":\"bg11.jpg\"}', '0', '0', 'updated slider: 7', '::1', '2020-01-26 17:54:59'),
(116, 1, 'update', 13, 'slider', '5', 0, '{\"id\":\"5\",\"img\":\"129556.jpg\",\"first_title\":\"The Best Business Information\",\"second_title\":\"We\'re In The Business Of Get Quality Business Service\",\"dir\":\"two\",\"rank\":\"2\"}', '{\"id\":\"5\",\"first_title\":\"The Best Business Information\",\"second_title\":\"We\'re In The Business Of Get Quality Business Service\",\"rank\":\"2\",\"img\":\"bg21.jpg\"}', '0', '0', 'updated slider: 5', '::1', '2020-01-26 17:54:59'),
(117, 1, 'update', 13, 'slider', '1', 0, '{\"id\":\"1\",\"img\":\"325429.jpg\",\"first_title\":\"The Best Business Information \",\"second_title\":\"We\'re In The Business Of Helping You Start Your Business\",\"dir\":\"two\",\"rank\":\"3\"}', '{\"id\":\"1\",\"first_title\":\"The Best Business Information \",\"second_title\":\"We\'re In The Business Of Helping You Start Your Business\",\"rank\":\"3\",\"img\":\"bg31.jpg\"}', '0', '0', 'updated slider: 1', '::1', '2020-01-26 17:55:00'),
(118, 1, 'update', 14, 'about', '0', 0, 'null', '{\"header\":\"About Dive Mor\",\"title\":\" \",\"paragraph\":\"Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\\r\\nOur company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\\r\\nOur company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\"}', '0', '0', 'updated about data', '::1', '2020-01-26 18:58:41'),
(119, 1, 'update', 14, 'about', '0', 0, 'null', '{\"header\":\"About D\",\"title\":\" \",\"paragraph\":\"Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\\r\\nOur company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\\r\\nOur company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\"}', '0', '0', 'updated about data', '::1', '2020-01-26 18:58:52'),
(120, 1, 'update', 14, 'about', '0', 0, 'null', '{\"header\":\"About Dive \",\"title\":\" \",\"paragraph\":\"Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\\r\\nOur company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\\r\\nOur company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation\"}', '0', '0', 'updated about data', '::1', '2020-01-26 18:59:01'),
(121, 1, 'update', 14, 'about', '0', 0, 'null', '{\"header\":\"About Us\",\"title\":\" WE ARE AWWARED WINNING COMPANY\",\"paragraph\":\"Over the year we have lots of experience in our field. In sit amet massa sapien. Vestibulum diam turpis, gravida in lobortis id, ornare a eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sagittis nulla non elit dignissim suscipit. Duis lorem nulla, eleifend.\"}', '0', '0', 'updated about data', '::1', '2020-01-26 18:59:44'),
(122, 1, 'update', 14, 'about', '0', 0, 'null', '{\"header\":\"Who we are\",\"title\":\" WE HAVE WORLDWIDE BUSINESS\",\"paragraph\":\"Helvetica cold-pressed lomo messenger bag ugh. Vinyl jean shorts Austin pork belly PBR retro, Etsy VHS kitsch actually messenger bag pug. Pbrb semiotics try-hard, Schlitz occupy dreamcatcher master cleanse Marfa Vice tofu.\"}', '0', '0', 'updated about data', '::1', '2020-01-26 19:00:11'),
(123, 1, 'update', 14, 'about', '0', 0, 'null', '{\"header\":\"What We Do\",\"title\":\" WE BUILD READYMADE APPLICATIONS\",\"paragraph\":\"Over the year we have lots of experience in our field. In sit amet massa sapien. Vestibulum diam turpis, gravida in lobortis id, ornare a eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sagittis nulla non elit dignissim suscipit. Duis lorem nulla, eleifend.\"}', '0', '0', 'updated about data', '::1', '2020-01-26 19:00:35'),
(124, 1, 'update', 14, 'about', '0', 0, 'null', '{\"header\":\"Achievements\",\"title\":\" CLEAN AND MODERN DESIGN\",\"paragraph\":\"Over the year we have lots of experience in our field. In sit amet massa sapien. Vestibulum diam turpis, gravida in lobortis id, ornare a eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sagittis nulla non elit dignissim suscipit. Duis lorem nulla, eleifend.\"}', '0', '0', 'updated about data', '::1', '2020-01-26 19:01:32'),
(125, 1, 'update', 14, 'about', '0', 0, 'null', '{\"header\":\"Achievements\",\"title\":\" CLEAN AND MODERN DESIGN\",\"paragraph\":\"Over\"}', '0', '0', 'updated about data', '::1', '2020-01-26 19:10:49'),
(126, 1, 'update', 14, 'about', '0', 0, 'null', '{\"header\":\"Achievements\",\"title\":\" CLEAN AND MODERN DESIGN\",\"paragraph\":\" the year we have lots of experience in our field. In sit amet massa sapien. Vestibulum diam turpis, gravida in lobortis id, ornare a eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sagittis nulla non elit dignissim suscipit. Duis lorem nulla, eleifend.\"}', '0', '0', 'updated about data', '::1', '2020-01-26 19:11:02'),
(127, 1, 'update', 22, 'gallery', '16', 0, '{\"id\":\"16\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"Trips\",\"meta\":\"gallery\",\"img\":\"3254592.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"16\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"Trips\",\"rank\":\"1\",\"img\":\"portfolio11.jpg\"}', '0', '0', 'updated gallery: 16', '::1', '2020-01-26 19:31:54'),
(128, 1, 'update', 22, 'gallery', '17', 0, '{\"id\":\"17\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"development\",\"meta\":\"gallery\",\"img\":\"3254292.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"17\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"development\",\"rank\":\"1\",\"img\":\"portfolio21.jpg\"}', '0', '0', 'updated gallery: 17', '::1', '2020-01-26 19:31:54'),
(129, 1, 'update', 22, 'gallery', '18', 0, '{\"id\":\"18\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"joomla\",\"meta\":\"gallery\",\"img\":\"1295562.jpg\",\"link\":\"\",\"rank\":\"1\"}', '{\"id\":\"18\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"joomla\",\"rank\":\"1\",\"img\":\"portfolio31.jpg\"}', '0', '0', 'updated gallery: 18', '::1', '2020-01-26 19:31:54'),
(130, 1, 'update', 22, 'gallery', '19', 0, '{\"id\":\"19\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"wordpress\",\"meta\":\"gallery\",\"img\":\"9513262.jpg\",\"link\":\"\",\"rank\":\"2\"}', '{\"id\":\"19\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"wordpress\",\"rank\":\"2\",\"img\":\"portfolio41.jpg\"}', '0', '0', 'updated gallery: 19', '::1', '2020-01-26 19:31:54'),
(131, 1, 'update', 22, 'gallery', '20', 0, '{\"id\":\"20\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"wordpress\",\"meta\":\"gallery\",\"img\":\"Cenote-Taj-Mahal-3-of-52.jpg\",\"link\":\"\",\"rank\":\"3\"}', '{\"id\":\"20\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"wordpress\",\"rank\":\"3\",\"img\":\"portfolio51.jpg\"}', '0', '0', 'updated gallery: 20', '::1', '2020-01-26 19:31:54'),
(132, 1, 'update', 22, 'gallery', '44', 0, '{\"id\":\"44\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"web-design\",\"meta\":\"gallery\",\"img\":\"IMG_1632.JPG\",\"link\":\"\",\"rank\":\"4\"}', '{\"id\":\"44\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"web-design\",\"rank\":\"4\",\"img\":\"portfolio61.jpg\"}', '0', '0', 'updated gallery: 44', '::1', '2020-01-26 19:31:55'),
(133, 1, 'update', 22, 'gallery', '45', 0, '{\"id\":\"45\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"wordpress\",\"meta\":\"gallery\",\"img\":\"IMG_1924.JPG\",\"link\":\"\",\"rank\":\"9\"}', '{\"id\":\"45\",\"meta\":\"gallery\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"points\":\"wordpress\",\"rank\":\"9\",\"img\":\"portfolio71.jpg\"}', '0', '0', 'updated gallery: 45', '::1', '2020-01-26 19:31:55'),
(134, 1, 'Create', 22, 'gallery', '51', 0, '0', '{\"meta\":\"gallery\",\"header\":\"new\",\"title\":\"new\",\"points\":\"wordpress\",\"rank\":\"10\",\"img\":\"portfolio72.jpg\"}', '0', '0', 'added gallery:', '::1', '2020-01-26 19:31:55'),
(135, 1, 'Create', 22, 'gallery', '52', 0, '0', '{\"meta\":\"gallery\",\"header\":\"new2\",\"title\":\"new2\",\"points\":\"new2\",\"rank\":\"11\",\"img\":\"portfolio-bg11.jpg\"}', '0', '0', 'added gallery:', '::1', '2020-01-26 19:33:14'),
(136, 1, 'Delete', 22, 'gallery', '45', 0, '{\"id\":\"45\",\"header\":\"Hurghada\",\"title\":\"Diving\",\"paragraph\":\"\",\"points\":\"wordpress\",\"meta\":\"gallery\",\"img\":\"portfolio71.jpg\",\"link\":\"\",\"rank\":\"9\"}', '0', '0', '0', 'Deleted gallery:45', '::1', '2020-01-26 19:33:21');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(5) NOT NULL,
  `activity_id` int(5) NOT NULL,
  `hotel_id` int(5) NOT NULL,
  `sub_id` int(5) NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `activity_id`, `hotel_id`, `sub_id`, `rank`, `deleted`) VALUES
(1, 1, 1, 6, 1, 0),
(2, 3, 1, 7, 2, 0),
(3, 2, 1, 1, 3, 0),
(4, 2, 2, 1, 4, 0),
(5, 2, 2, 2, 5, 0),
(6, 2, 2, 3, 6, 0),
(7, 2, 2, 4, 7, 0),
(8, 2, 2, 5, 8, 0),
(9, 2, 3, 1, 9, 0),
(10, 2, 3, 2, 10, 0),
(11, 2, 3, 3, 11, 0),
(12, 2, 3, 4, 12, 0),
(13, 2, 3, 5, 13, 0),
(14, 2, 4, 1, 14, 0),
(15, 2, 4, 2, 15, 0),
(16, 2, 4, 3, 16, 0),
(17, 2, 4, 4, 17, 0),
(18, 2, 4, 5, 18, 0),
(19, 2, 5, 1, 19, 0),
(20, 2, 5, 2, 20, 0),
(21, 2, 5, 3, 21, 0),
(22, 2, 5, 4, 22, 0),
(23, 2, 5, 5, 23, 0);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creat` tinyint(1) NOT NULL,
  `edit` tinyint(1) NOT NULL,
  `view` tinyint(1) NOT NULL,
  `remove` tinyint(1) NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `creat`, `edit`, `view`, `remove`, `link`, `type`, `rank`, `deleted`) VALUES
(1, 'الإعدادات', 1, 1, 1, 0, 'admin/users', 'store', 1, 0),
(2, 'المبيعات', 1, 1, 1, 1, 'pos', 'store', 2, 0),
(3, 'المشتريات', 1, 1, 1, 1, 'prs', 'store', 3, 0),
(4, 'المعاملات', 1, 1, 1, 1, 'transactions', 'store', 3, 0),
(5, 'الموردين', 1, 1, 1, 1, 'admin/suppliers', 'store', 5, 0),
(6, 'العملاء', 1, 1, 1, 1, 'admin/clients', 'store', 5, 0),
(7, 'المخزن', 1, 1, 1, 0, 'admin/items/groups', 'store', 4, 0),
(8, 'التقارير', 1, 1, 1, 1, 'reports', 'store', 6, 0),
(11, 'التسجيل', 1, 1, 1, 1, 'admin/log_activity', 'store', 6, 0),
(12, 'Site manager', 1, 1, 1, 1, 'admin/site_manager', 'store', 7, 0),
(13, 'slider', 1, 1, 1, 0, '', 'site', 8, 0),
(14, 'about', 1, 1, 1, 1, '', 'site', 9, 0),
(15, 'services', 1, 1, 1, 1, '', 'site', 10, 0),
(16, 'products', 1, 1, 1, 1, '', 'site', 11, 0),
(17, 'portfolio', 1, 1, 1, 1, '', 'site', 12, 0),
(18, 'blog', 1, 1, 1, 1, '', 'site', 13, 0),
(19, 'contact', 1, 1, 1, 1, '', 'site', 14, 0),
(20, 'Footer', 1, 1, 1, 1, '', 'site', 14, 0),
(21, 'Client Requests', 1, 1, 1, 1, '', 'site', 15, 0),
(22, 'gallery', 1, 1, 1, 1, '', 'site', 12, 0),
(23, 'team', 1, 1, 1, 1, '', 'site', 13, 0),
(24, 'Offers', 1, 1, 1, 1, '', 'site', 14, 0),
(25, 'language', 1, 1, 1, 1, '', 'site', 15, 0),
(26, 'Bookings', 1, 1, 1, 1, '', 'site', 16, 0);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `to_uid` int(11) DEFAULT NULL,
  `to_branch_id` int(11) DEFAULT NULL,
  `to_dep_id` int(11) DEFAULT NULL,
  `readedby` int(11) DEFAULT NULL,
  `head` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(3) NOT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(5) NOT NULL,
  `activity_id` int(5) NOT NULL,
  `hotel_id` int(5) NOT NULL,
  `sub_id` int(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `data` text NOT NULL,
  `image` varchar(150) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `currency` varchar(50) NOT NULL,
  `special` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `activity_id`, `hotel_id`, `sub_id`, `name`, `data`, `image`, `price`, `currency`, `special`, `deleted`, `timestamp`) VALUES
(1, 1, 1, 6, 'Test', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(2, 3, 1, 7, 'Test 1', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 1, 0, '2019-11-02 02:30:21'),
(3, 2, 1, 1, 'Test 2', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(4, 2, 2, 1, 'Test 3', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(5, 2, 2, 2, 'Test 4', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(6, 2, 2, 3, 'Test 5', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(7, 2, 2, 4, 'Test 6', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 1, 0, '2019-11-02 02:30:21'),
(8, 2, 2, 5, 'Test 7', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(9, 2, 3, 1, 'Test 8', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(10, 2, 3, 2, 'Test 9', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(11, 2, 3, 3, 'Test 10', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 1, 0, '2019-11-02 02:30:21'),
(12, 2, 3, 4, 'Test 11', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(13, 2, 3, 5, 'Test 12', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(14, 2, 4, 1, 'Test 13', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 1, 0, '2019-11-02 02:30:21'),
(15, 2, 4, 2, 'Test 14', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(16, 2, 4, 3, 'Test 15', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(17, 2, 4, 4, 'Test 16', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(18, 2, 4, 5, 'Test 17', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(19, 2, 5, 1, 'Test 18', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(20, 2, 5, 2, 'Test 19', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 1, 0, '2019-11-02 02:30:21'),
(21, 2, 5, 3, 'Test 20', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(22, 2, 5, 4, 'Test 21', 'Test Test Test Test Test', 'IMG_3782.JPG', '2000', '$', 0, 0, '2019-11-02 02:30:21'),
(24, 1, 4, 6, 'test 23', 'Test Test Test Test Test55', 'IMG_3782.JPG', '10000', '$', 1, 0, '2020-01-02 14:01:53');

-- --------------------------------------------------------

--
-- Table structure for table `site_files`
--

CREATE TABLE `site_files` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `form_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `site_groups`
--

CREATE TABLE `site_groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_groups`
--

INSERT INTO `site_groups` (`id`, `group_name`, `serial`, `img`, `rank`, `deleted`) VALUES
(1, 'HVAC ACCESSORIES', 'hva', 'hvac_accessories.jpg', 1, 0),
(2, 'VENTILATION FANS', 'ven', 'ventilation fans.jpg', 2, 0),
(3, 'ELECTRICAL MATERIALS', 'elec', 'ELECTRICAL MATERIALS.jpg', 3, 0),
(4, 'GRILLES & DIFFUSERS', 'gril', 'grilles diffusers.jpg', 4, 0),
(5, 'G.I. & ACCESSORIES', 'g.i.', 'G.I. & ACCESSORIES.jpg', 5, 0),
(6, 'P.I. & ACCESSORIES', 'p.i.', 'P.I. & ACCESSORIES.jpg', 6, 0),
(7, 'AIR CONDITION UNITS', 'air', 'AIR CONDITION UNITS.jpg', 7, 0),
(8, 'THERMOSTAT & CONTROL VALVES', 'ther', 'THERMOSTAT & CONTROL VALVES.jpg', 8, 0),
(9, 'AC FILTERS', 'ac', 'AC FILTERS.jpg', 9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `site_group_items`
--

CREATE TABLE `site_group_items` (
  `id` int(20) NOT NULL,
  `group_id` int(11) NOT NULL,
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_group_items`
--

INSERT INTO `site_group_items` (`id`, `group_id`, `item_name`, `serial`, `company`, `description`, `img`, `file`, `rank`, `timestamp`, `deleted`) VALUES
(1, 1, 'وش مبنى 100 سم', '00133E8', '1', 'Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit.', '1.jpg', '1.pdf', 1, '2019-04-18 14:46:10', 0),
(2, 1, 'حديد 4 لنية ', '002225B', '4', 'حديد 4 لنية ', '', '', 2, '2019-04-24 14:50:10', 0),
(3, 1, 'test', '009B11E', '4', 'حديد 3 لنية', '', '', 3, '2019-04-24 15:22:01', 0),
(4, 3, 'حديد 4 لنية', '00269BB', '1', 'حديد 4 لنية', '', '', 0, '2019-04-24 15:23:21', 0),
(5, 3, 'حديد 4 لنية', '00F436C', '6', 'حديد 4 لنية', '', '', 0, '2019-04-24 15:24:17', 0),
(6, 3, 'حديد 2.5 لنية أطوال', '00A6820', '7', 'حديد 2.5 لنية', '', '', 0, '2019-04-24 15:26:18', 0),
(7, 3, 'حديد 2 لنية لفف', '003988F', '1', 'حديد 2 لنية لفف', '', '', 0, '2019-04-24 15:29:51', 0),
(8, 7, 'أسمنت الجيش رتبة 52.5 بورتلاندى', '0091BED', '13', 'أسمنت الجيش رتبة 52.5 بورتلاندى', '', '', 0, '2019-04-24 15:36:18', 0),
(9, 7, 'أسمنت السويدى', '0009D57', '12', 'أسمنت السويدى', '', '', 0, '2019-04-24 16:10:52', 0),
(10, 7, 'أسمنت المصرية', '00C27C9', '11', 'أسمنت المصرية', '', '', 0, '2019-04-24 16:11:38', 0),
(11, 7, 'أسمنت سويتر', '00D982E', '25', 'أسمنت سويتر', '', '', 0, '2019-04-24 16:15:22', 0),
(12, 7, 'أسمنت الجيش رتبة 42.5 بورتلاندى العريش', '00B3C18', '14', 'أسمنت الجيش رتبة 42.5 بورتلاندى العريش', '', '', 0, '2019-04-24 16:18:53', 0),
(14, 1, 'test2', '008C60E', 'test2', 'test2 test2 test2 test2', '556a0152-fab8-428f-9010-215a63d9fa00.png', '710b7b44-f1e3-4ecf-bf81-37ca8927664c1.png', 0, '2019-10-04 20:23:52', 0),
(15, 1, 'test2w', '00A31BA', 'test2w', 'test2wtest2wtest2w test2wtest2w', '556a0152-fab8-428f-9010-215a63d9fa00.png', 'Book121.csv', 1, '2019-10-04 20:27:44', 0),
(16, 2, 'test2ww', '00F9BB8', 'test2w', 'test 2uhsd', '6bc837d5-1df8-4527-8e1a-61ba7a17c5a8.png', '710b7b44-f1e3-4ecf-bf81-37ca8927664c.png', 5, '2019-10-04 22:38:53', 0);

-- --------------------------------------------------------

--
-- Table structure for table `site_meta_data`
--

CREATE TABLE `site_meta_data` (
  `id` int(11) NOT NULL,
  `header` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paragraph` text COLLATE utf8_unicode_ci NOT NULL,
  `points` text COLLATE utf8_unicode_ci NOT NULL,
  `meta` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_meta_data`
--

INSERT INTO `site_meta_data` (`id`, `header`, `title`, `paragraph`, `points`, `meta`, `img`, `link`, `rank`) VALUES
(2, 'Our Services', 'Central Ventilation System', 'will have to make sure the prototype looks finished by inserting text or photo.make sure the prototype looks finished by.', '', 'services', 'fa fa-sun-o', '', 1),
(3, 'Our Services', 'HVAC Contractor', 'will have to make sure the prototype looks finished by inserting text or photo.make sure the prototype looks finished by.', '', 'services', 'fa fa-adjust', '', 2),
(4, 'Our Services', 'HVAC Maintenance', 'will have to make sure the prototype looks finished by inserting text or photo.make sure the prototype looks finished by.', '', 'services', 'fa fa-fire', '', 3),
(5, 'Our Services', 'HVAC Technical Support', 'will have to make sure the prototype looks finished by inserting text or photo.make sure the prototype looks finished by.', '', 'services', 'fa fa-cogs', '', 4),
(8, 'Call', '01002741794', '9am-5pm', '', 'contact_us', 'fa fa-mobile', '', 1),
(9, 'Email', 'info@divemore-group.com', 'Web: divemore-group.com', '', 'contact_us', 'fa fa-envelope-o', '', 2),
(10, 'Location', 'Sheraton Street, Hurghada, Egypt', 'Sheraton Street, Hurghada, Egypt', '', 'contact_us', 'fa fa-map-marker', '', 3),
(11, '', '', 'Our purpose is to positively impact lifestyles of the community. We serve through consistent, timely, quality,  efficient and  value  added  delivery of  innovative joiness that  constantly exceed expectations.', '', 'footer_about', '', '', 1),
(12, '', '', 'Don\'t hesitate to contact us with any questions or inquiries.', '', 'footer_info', '', '', 1),
(13, 'Tel', '01002741794', '', '', 'footer_tel', '', '', 1),
(14, 'Email', 'info@divemore-group.com', '', '', 'footer_email', '', '', 1),
(15, 'Working Hours', '9am-5pm', '', '', 'footer_working', '', '', 1),
(16, 'Hurghada', 'Diving', '', 'Trips', 'gallery', 'portfolio11.jpg', '', 1),
(17, 'Hurghada', 'Diving', '', 'development', 'gallery', 'portfolio21.jpg', '', 1),
(18, 'Hurghada', 'Diving', '', 'joomla', 'gallery', 'portfolio31.jpg', '', 1),
(19, 'Hurghada', 'Diving', '', 'wordpress', 'gallery', 'portfolio41.jpg', '', 2),
(20, 'Hurghada', 'Diving', '', 'wordpress', 'gallery', 'portfolio51.jpg', '', 3),
(22, 'Sent Successfully', 'Sent', '#000000', '', 'clients_req_status', '', '', 1),
(23, 'Failed to sent', 'Failed', '#bd1212', '', 'clients_req_status', '', '', 1),
(24, 'Waiting to send', 'Waiting', '#467b2b', '', 'clients_req_status', '', '', 1),
(25, 'Business City', '3.5', 'Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation', '', 'portfolio', 'cabo-pulmo-diving-11.jpg', '', 1),
(26, 'Business City', '4.5', 'Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation', '', 'portfolio', 'Discover-Scuba-Diving1.jpg', '', 3),
(28, 'Business City', '5', 'Our company dive more, we are one of the major dealer and supplier of HVAC (Heating Ventilation and Air Conditioning) and Switchgear Materials . We are a leading company in supply and installation', '', 'portfolio', 'scuba-diving2-960x11491.jpg', '', 2),
(29, 'Julien Miro', 'Software Developer', '', '', 'team', '11.jpg', '', 1),
(30, 'Joan Williams', 'Support Operator', '', '', 'team', '21.jpg', '', 2),
(32, 'Benedict Smith', 'Creative Director', '', '', 'team', '31.jpg', '', 3),
(33, 'Madlen Green', 'Sales Manager', '', '', 'team', '41.jpg', '', 4),
(34, 'Julien Miro', 'Julien Miro', '', '', 'team', 'Discover-Scuba-Diving.jpg', '', 5),
(35, 'Waiting to send', 'Waiting', '#bd1212', '', 'booking_status', '', '', 1),
(36, 'Pending', 'Pending', '#ffa922', '', 'booking_status', '', '', 1),
(37, 'Confirmed', 'Confirmed', '#1f8d5d', '', 'booking_status', '', '', 1),
(44, 'Hurghada', 'Diving', '', 'web-design', 'gallery', 'portfolio61.jpg', '', 4),
(46, 'About Us', ' WE ARE AWWARED WINNING COMPANY', 'Over the year we have lots of experience in our field. In sit amet massa sapien. Vestibulum diam turpis, gravida in lobortis id, ornare a eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sagittis nulla non elit dignissim suscipit. Duis lorem nulla, eleifend.', '', 'about_us', '325429.jpg', '', 1),
(47, 'Who we are', ' WE HAVE WORLDWIDE BUSINESS', 'Helvetica cold-pressed lomo messenger bag ugh. Vinyl jean shorts Austin pork belly PBR retro, Etsy VHS kitsch actually messenger bag pug. Pbrb semiotics try-hard, Schlitz occupy dreamcatcher master cleanse Marfa Vice tofu.', '', 'who_we_are', '325429.jpg', '', 1),
(48, 'What We Do', ' WE BUILD READYMADE APPLICATIONS', 'Over the year we have lots of experience in our field. In sit amet massa sapien. Vestibulum diam turpis, gravida in lobortis id, ornare a eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sagittis nulla non elit dignissim suscipit. Duis lorem nulla, eleifend.', '', 'what_we_do', '325429.jpg', '', 1),
(49, 'Achievements', ' CLEAN AND MODERN DESIGN', ' the year we have lots of experience in our field. In sit amet massa sapien. Vestibulum diam turpis, gravida in lobortis id, ornare a eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sagittis nulla non elit dignissim suscipit. Duis lorem nulla, eleifend.', '', 'achievements', '325429.jpg', '', 1),
(51, 'new', 'new', '', 'wordpress', 'gallery', 'portfolio72.jpg', '', 10),
(52, 'new2', 'new2', '', 'new2', 'gallery', 'portfolio-bg11.jpg', '', 11);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_title` text COLLATE utf8_unicode_ci NOT NULL,
  `second_title` text COLLATE utf8_unicode_ci NOT NULL,
  `dir` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `img`, `first_title`, `second_title`, `dir`, `rank`) VALUES
(1, 'bg31.jpg', 'The Best Business Information ', 'We\'re In The Business Of Helping You Start Your Business', 'two', 3),
(5, 'bg21.jpg', 'The Best Business Information', 'We\'re In The Business Of Get Quality Business Service', 'two', 2),
(7, 'bg11.jpg', 'The Best Business Information', 'We\'re In The Business Of Get Quality Business Service', 'one', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sub_activity`
--

CREATE TABLE `sub_activity` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_activity`
--

INSERT INTO `sub_activity` (`id`, `name`, `deleted`) VALUES
(1, 'Diving', 0),
(2, 'Courses', 0),
(3, 'Intro', 0),
(4, 'Daily Dive', 0),
(5, 'Speciality', 0),
(6, 'Aqua Center', 0),
(7, 'SPA', 0);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` varchar(150) NOT NULL,
  `possion` varchar(100) NOT NULL,
  `info` text NOT NULL,
  `contact` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `fullname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `role_id` int(5) NOT NULL,
  `department` int(11) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `permission` tinyint(1) NOT NULL,
  `disabled` tinyint(1) NOT NULL,
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `fullname`, `email`, `role_id`, `department`, `mobile_no`, `password`, `is_admin`, `permission`, `disabled`, `last_ip`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'admin', 'admin', 'info@alamtc.com', 1, 1, '12345', '$2y$10$2trbqRBFfl/HfbGqpx/7Vu/sJGoZhVI1Mg3A.6ylgvpOff4UyFxRC', 1, 1, 0, '', '2019-03-13 00:00:00', '2019-03-13 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_branches`
--

CREATE TABLE `user_branches` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch_id` int(6) NOT NULL,
  `role_id` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_branches`
--

INSERT INTO `user_branches` (`id`, `user_id`, `branch_id`, `role_id`) VALUES
(316, 1, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `name`, `permission`) VALUES
(1, 'Admin', 1),
(2, 'IT', 1),
(3, 'GMs', 0),
(4, 'Staff', 0),
(5, 'Managers', 0),
(6, 'test', 1),
(7, 'test2', 0),
(8, 'test22', 0),
(9, 'test221', 0),
(10, 'test22', 0),
(11, 'test23', 0),
(12, 'test22', 0),
(13, 'test23', 0),
(14, 'test23', 0),
(15, 'test221', 0),
(16, 'test221', 0),
(17, 'test221', 0),
(18, 'test22', 0),
(19, 'test', 0),
(20, 'test22', 0),
(21, 'test', 0),
(22, 'test2', 0),
(23, 'test22', 0),
(24, 'test2', 0),
(25, 'cost', 1),
(26, 'recieving', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_groups_permission`
--

CREATE TABLE `user_groups_permission` (
  `id` int(11) NOT NULL,
  `role_id` int(6) NOT NULL,
  `module_id` int(11) NOT NULL,
  `g_view` tinyint(1) NOT NULL,
  `creat` tinyint(2) NOT NULL,
  `edit` tinyint(2) NOT NULL,
  `view` tinyint(2) NOT NULL,
  `remove` tinyint(2) NOT NULL,
  `pr_edit` tinyint(1) NOT NULL,
  `pr_approve` tinyint(1) NOT NULL,
  `recieve` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_groups_permission`
--

INSERT INTO `user_groups_permission` (`id`, `role_id`, `module_id`, `g_view`, `creat`, `edit`, `view`, `remove`, `pr_edit`, `pr_approve`, `recieve`) VALUES
(1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0),
(2, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0),
(3, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0),
(7, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0),
(8, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0),
(9, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0),
(10, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0),
(11, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0),
(12, 0, 2, 1, 0, 0, 0, 1, 1, 0, 0),
(13, 0, 3, 1, 1, 1, 1, 1, 0, 0, 0),
(14, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0),
(15, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0),
(16, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0),
(17, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0),
(18, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0),
(19, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0),
(20, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0),
(21, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0),
(22, 1, 2, 1, 1, 1, 1, 1, 1, 0, 0),
(23, 1, 3, 1, 0, 0, 0, 1, 0, 0, 0),
(24, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0),
(25, 1, 5, 0, 0, 0, 0, 0, 0, 0, 0),
(26, 1, 6, 0, 0, 0, 0, 0, 0, 0, 0),
(27, 1, 7, 0, 0, 0, 0, 0, 0, 0, 0),
(28, 1, 8, 0, 0, 0, 0, 0, 0, 0, 0),
(29, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0),
(30, 1, 10, 0, 0, 0, 0, 0, 0, 0, 0),
(31, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0),
(32, 2, 2, 1, 0, 0, 0, 1, 1, 0, 0),
(33, 2, 3, 1, 0, 0, 0, 1, 0, 0, 0),
(34, 2, 4, 1, 0, 0, 0, 0, 0, 0, 0),
(35, 2, 5, 0, 0, 0, 0, 0, 0, 0, 0),
(36, 2, 6, 0, 0, 0, 0, 0, 0, 0, 0),
(37, 2, 7, 0, 0, 0, 0, 0, 0, 0, 0),
(38, 2, 8, 0, 0, 0, 0, 0, 0, 0, 0),
(39, 2, 9, 0, 0, 0, 0, 0, 0, 0, 0),
(40, 2, 10, 1, 1, 1, 1, 0, 0, 0, 0),
(41, 6, 1, 1, 0, 0, 0, 0, 0, 0, 0),
(42, 6, 2, 0, 0, 0, 1, 0, 0, 0, 0),
(43, 6, 3, 0, 1, 0, 0, 0, 0, 0, 0),
(44, 6, 4, 0, 0, 1, 0, 0, 0, 0, 0),
(45, 6, 5, 0, 1, 0, 0, 0, 0, 0, 0),
(46, 6, 6, 0, 1, 0, 1, 0, 0, 0, 0),
(47, 6, 7, 1, 1, 0, 0, 0, 0, 0, 0),
(48, 6, 8, 0, 1, 0, 1, 0, 0, 0, 0),
(49, 6, 9, 0, 1, 1, 0, 0, 0, 0, 0),
(50, 6, 10, 0, 0, 0, 0, 0, 0, 0, 0),
(51, 25, 1, 1, 0, 0, 1, 0, 0, 0, 0),
(52, 25, 2, 1, 0, 0, 1, 0, 0, 0, 0),
(53, 25, 3, 1, 1, 0, 0, 0, 0, 0, 0),
(54, 25, 4, 1, 0, 1, 0, 0, 0, 0, 0),
(55, 26, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(56, 26, 2, 1, 1, 1, 1, 0, 1, 0, 1),
(57, 26, 3, 0, 0, 0, 0, 0, 0, 0, 0),
(58, 26, 4, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_permissions`
--

CREATE TABLE `user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `creat` tinyint(4) NOT NULL,
  `edit` tinyint(4) NOT NULL,
  `view` tinyint(4) NOT NULL,
  `remove` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_permissions`
--

INSERT INTO `user_permissions` (`id`, `user_id`, `module_id`, `creat`, `edit`, `view`, `remove`, `timestamp`) VALUES
(1, 1, 1, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(2, 1, 2, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(3, 1, 3, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(4, 1, 4, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(5, 1, 5, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(6, 1, 6, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(7, 1, 7, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(8, 1, 8, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(9, 1, 9, 0, 0, 0, 0, '2019-01-15 13:55:20'),
(10, 1, 11, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(11, 1, 12, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(12, 1, 13, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(13, 1, 14, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(14, 1, 15, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(15, 1, 16, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(16, 1, 17, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(17, 1, 18, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(18, 1, 19, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(19, 1, 20, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(20, 1, 21, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(21, 1, 22, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(22, 1, 23, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(23, 1, 24, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(24, 1, 25, 1, 1, 1, 1, '2019-01-15 13:55:20'),
(25, 1, 26, 1, 1, 1, 1, '2019-01-15 13:55:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `supp_name` (`client_name`);

--
-- Indexes for table `clients_mail_queue`
--
ALTER TABLE `clients_mail_queue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `font_icons`
--
ALTER TABLE `font_icons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_files`
--
ALTER TABLE `site_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_groups`
--
ALTER TABLE `site_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `group_name` (`group_name`),
  ADD UNIQUE KEY `serial` (`serial`);

--
-- Indexes for table `site_group_items`
--
ALTER TABLE `site_group_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `serial` (`serial`);

--
-- Indexes for table `site_meta_data`
--
ALTER TABLE `site_meta_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_activity`
--
ALTER TABLE `sub_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_branches`
--
ALTER TABLE `user_branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_groups_permission`
--
ALTER TABLE `user_groups_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_permissions`
--
ALTER TABLE `user_permissions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity`
--
ALTER TABLE `activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clients_mail_queue`
--
ALTER TABLE `clients_mail_queue`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `font_icons`
--
ALTER TABLE `font_icons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=595;

--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `site_files`
--
ALTER TABLE `site_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `site_groups`
--
ALTER TABLE `site_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `site_group_items`
--
ALTER TABLE `site_group_items`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `site_meta_data`
--
ALTER TABLE `site_meta_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sub_activity`
--
ALTER TABLE `sub_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT for table `user_branches`
--
ALTER TABLE `user_branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=317;

--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user_groups_permission`
--
ALTER TABLE `user_groups_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `user_permissions`
--
ALTER TABLE `user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
